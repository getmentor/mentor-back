const express = require("express");
const bodyParser = require("body-parser");
const graphqlHttp = require("express-graphql");
const mongoose = require("mongoose");

const graphQlSchema = require("./graphql/schema/index");
const graphQlResolvers = require("./graphql/resolvers/index");
const isAuth = require("./middleware/is-auth");
const nodeMailer = require("nodemailer");
const SMTPConnection = require("nodemailer/lib/smtp-connection");
var handlebars = require("handlebars");
var fs = require("fs");

const usersRouter = require("./routes/users");
const stripeRouter = require("./routes/stripe");

const app = express();
const port = process.env.PORT || 5000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(require("body-parser").text());

app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Methods", "POST,GET,OPTIONS");
  res.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization");
  if (req.method === "OPTIONS") {
    return res.sendStatus(200);
  }
  next();
});

app.use(isAuth);

app.use(
  "/graphql",
  graphqlHttp({
    schema: graphQlSchema,
    rootValue: graphQlResolvers,
    graphiql: true
  })
);

var status;
var readHTMLFile = function(path, callback) {
  fs.readFile(path, { encoding: "utf-8" }, function(err, html) {
    if (err) {
      throw err;
      callback(err);
    } else {
      callback(null, html);
    }
  });
};
app.post("/sendmail", function(req, res, next) {
  const body = req.body;

  const email_user = body.email;

  var mailConfig;

  if (process.env.NODE_ENV === "production") {
    // all emails are delivered to destination
    mailConfig = {
      host: "smtp.sendgrid.net",
      port: 587,
      auth: {
        user: "app141342349@heroku.com",
        pass: "b7baphmr4004"
      }
    };
  } else {
    // all emails are catched by ethereal.email
    mailConfig = {
      host: "smtp.ethereal.email",
      port: 587,
      auth: {
        user: "rex.kovacek39@ethereal.email",
        pass: "7wwvw1KjgeJc65gnVg"
      }
    };
  }

  let transporter = nodeMailer.createTransport(mailConfig);

  readHTMLFile(
    __dirname + "/email_templates/reset-pasword-template.html",
    function(err, html) {
      let confirm = "https://react-mentor.herokuapp.com/reset-password";

      var template = handlebars.compile(html);
      var replacements = {
        email: email_user,
        confirm: confirm
      };
      var htmlToSend = template(replacements);
      let mailOptions = {
        from: '"Mentor" <app141342349@heroku.com>', // sender address
        to: email_user, // list of receivers
        subject: "Reset Password", // Subject line
        html: htmlToSend
      };
      transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          status = 1;
          console.log(error);
        } else {
          status = 0;
          console.log("Message %s sent: %s", info.messageId, info.response);
        }
      });
    }
  );

  console.log(status);
  res.send(status);
  return status;
});

app.use("/users", usersRouter);
app.use("/stripe", stripeRouter);

mongoose
  .connect(
    `mongodb+srv://${process.env.MONGO_USER}:${
      process.env.MONGO_PASSWORD
    }@cluster0-dmni0.mongodb.net/${
      process.env.MONGO_DB
    }?retryWrites=true&w=majority`
  )
  .then(() => {
    app.listen(port, () => {
      console.log(`Server is running on port: ${port}`);
    });
  })
  .catch(err => {
    console.log(err);
  });
