const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const User = require("../../models/user");

const { listUser } = require("./merge");

const { listMentors } = require("./merge");
const { dateToString } = require("../../helpers/date");

const nodeMailer = require("nodemailer");
const SMTPConnection = require("nodemailer/lib/smtp-connection");
var handlebars = require("handlebars");
var fs = require("fs");
var paths = require("path");
const stripe = require("stripe")("sk_test_LBzReqBt1p9MltgkkVKntIn100p8wS1dhx");

module.exports = {
  users: async () => {
    try {
      const users = await User.find();
      return users.map(user => {
        return listUser(user);
      });
    } catch (err) {
      throw err;
    }
  },
  mentors: async ({ first, last }) => {
    try {
      const mentors = await User.find({ role: 2 })
        .skip(first)
        .limit(last);
      return mentors.map(user => {
        return listMentors(user);
      });
    } catch (err) {
      throw err;
    }
  },
  totalmentors: async () => {
    try {
      const mentors = await User.find({ role: 2 });
      return mentors.map(user => {
        return listMentors(user);
      });
    } catch (err) {
      throw err;
    }
  },
  mentorDetails: async ({ userId }) => {
    const user = await User.findOne({ _id: userId, status: 1 });

    if (!user) {
      throw new Error("User does not exist!");
    }
    return {
      userId: user.id,
      name: user.name,
      last_name: user.last_name,
      email: user.email,
      role: user.role,
      main_desc: user.main_desc,
      short_desc: user.short_desc,
      profile_pic: user.profile_pic,
      resume_link: user.resume_link,
      linkedin_url: user.linkedin_url,
      twitte_url: user.twitte_url,
      medium_url: user.medium_url
    };
  },
  createUser: async args => {
    try {
      const existingUser = await User.findOne({ email: args.userInput.email });
      if (existingUser) {
        throw new Error("User exists already.");
      }
      const hashedPassword = await bcrypt.hash(args.userInput.password, 12);

      const user = new User({
        name: args.userInput.name,
        last_name: args.userInput.last_name,
        email: args.userInput.email,
        password: hashedPassword,
        skills: args.userInput.skills,
        resume_link: args.userInput.resume_link,
        profile_pic: args.userInput.profile_pic,
        short_desc: args.userInput.short_desc,
        main_desc: args.userInput.main_desc,
        language: args.userInput.language,
        location: args.userInput.location,
        // expertise: args.userInput.expertise,
        // toolkit: args.userInput.toolkit,
        // experience: args.userInput.experience,
        // education: args.userInput.education,
        // certificates: args.userInput.certificates,
        // social_media_links: args.userInput.social_media_links,
        linkedin_url: args.userInput.linkedin_url,
        twitte_url: args.userInput.twitte_url,
        medium_url: args.userInput.medium_url,
        role: args.userInput.role,
        status: 0
      });

      const result = await user.save();

      /*  -------Email Functionality  : Start--------  */

      var status;
      var readHTMLFile = function(path, callback) {
        fs.readFile(path, { encoding: "utf-8" }, function(err, html) {
          if (err) {
            throw err;
            callback(err);
          } else {
            callback(null, html);
          }
        });
      };

      var mailConfig;

      const email_user = args.userInput.email;
      const user_role = args.userInput.role;

      if (process.env.NODE_ENV === "production") {
        // all emails are delivered to destination
        mailConfig = {
          host: "smtp.sendgrid.net",
          port: 587,
          auth: {
            user: "app141342349@heroku.com",
            pass: "b7baphmr4004"
          }
        };
      } else {
        // all emails are catched by ethereal.email
        mailConfig = {
          host: "smtp.ethereal.email",
          port: 587,
          auth: {
            user: "rex.kovacek39@ethereal.email",
            pass: "7wwvw1KjgeJc65gnVg"
          }
        };
      }

      let transporter = nodeMailer.createTransport(mailConfig);

      let dirpath = paths.resolve(
        __dirname,
        "../../email_templates/confirm-email-address-template.html"
      );
      let href;
      if (user_role == 1) {
        href =
          "https://react-node-mentor.herokuapp.com/users/updateconfirmstatus/" +
          email_user +
          "/1";
      } else if (user_role == 2) {
        href =
          "https://react-node-mentor.herokuapp.com/users/updateconfirmstatus/" +
          email_user +
          "/2";
      }

      console.log(dirpath);

      readHTMLFile(dirpath, function(err, html) {
        var template = handlebars.compile(html);
        var replacements = {
          email: email_user,
          role: user_role,
          href: href
        };
        var htmlToSend = template(replacements);
        let mailOptions = {
          from: '"Mentor" <app141342349@heroku.com>', // sender address
          to: email_user, // list of receivers
          subject: "Confirm your account on Mentor", // Subject line
          html: htmlToSend
        };
        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            status = 1;
            console.log(error);
          } else {
            status = 0;
            console.log("Message %s sent: %s", info.messageId, info.response);
          }
        });
      });

      /*  ---------Email Functionality-----------  : End  */

      stripe.customers.create(
        {
          email: result.email,
          name: result.name,
          description: "Customer :" + result.email
          //  source: "tok_amex" // obtained with Stripe.js
        },
        function(err, stripeCustomer) {
          // asynchronously called

          if (err) {
            console.log("Stripe Customer Creation failed");
          } else {
            console.log("Stripe Customer Creation Succeeded");
            console.log(stripeCustomer);

            User.findOne({ email: args.userInput.email }).then(user => {
              user.stripe_id = stripeCustomer.id;
              user.save();
            });
          }
        }
      );

      return { ...result._doc, password: null, _id: result.id };
    } catch (err) {
      throw err;
    }
  },
  login: async ({ email, password }) => {
    const user = await User.findOne({ email: email, status: 1 });

    if (!user) {
      throw new Error("User does not exist!");
    }
    const isEqual = await bcrypt.compare(password, user.password);
    if (!isEqual) {
      throw new Error("Password is incorrect!");
    }
    const token = jwt.sign(
      { userId: user.id, email: user.email },
      "somesupersecretkey",
      {
        expiresIn: "1h"
      }
    );
    return {
      userId: user.id,
      token: token,
      tokenExpiration: 1,
      role: user.role,
      email: user.email
    };
  },
  resetPassword: async args => {
    try {
      const user = await User.findOne({
        email: args.resetpassword.email,
        status: 1
      });

      if (!user) {
        throw new Error("User does not exist!");
      }

      if (args.resetpassword.password != args.resetpassword.confirmpassword) {
        throw new Error("Passwords don't match!");
      }

      const hashedPassword = await bcrypt.hash(args.resetpassword.password, 12);

      User.findOne({ email: args.resetpassword.email }).then(user => {
        user.password = hashedPassword;
        user.save();
      });

      return { ...user._doc, password: null, _id: user.id };
    } catch (err) {
      throw err;
    }
  },
  editMentorProfile: async args => {
    try {
      const existingUser = await User.findOne({
        email: args.mentorInput.email
      });
      if (!existingUser) {
        throw new Error("Invalid!");
      }
      const result = await User.findOne({ email: args.mentorInput.email }).then(
        user => {
          (user.name = args.mentorInput.name),
            (user.last_name = args.mentorInput.last_name),
            (user.email = args.mentorInput.email),
            (user.resume_link = args.mentorInput.resume_link),
            (user.profile_pic = args.mentorInput.profile_pic),
            (user.short_desc = args.mentorInput.short_desc),
            (user.main_desc = args.mentorInput.main_desc),
            (user.linkedin_url = args.mentorInput.linkedin_url),
            (user.twitte_url = args.mentorInput.twitte_url),
            (user.medium_url = args.mentorInput.medium_url);
          user.save();
        }
      );

      return { _id: existingUser.id };
    } catch (err) {
      throw err;
    }
  },
  userDetails: async ({ userId }) => {
    const user = await User.findOne({ _id: userId, status: 1 });

    if (!user) {
      throw new Error("User does not exist!");
    }
    return {
      userId: user.id,
      name: user.name,
      last_name: user.last_name,
      email: user.email,
      role: user.role,
      main_desc: user.main_desc,
      short_desc: user.short_desc,
      profile_pic: user.profile_pic,
      resume_link: user.resume_link,
      linkedin_url: user.linkedin_url,
      twitte_url: user.twitte_url,
      medium_url: user.medium_url
    };
  }
};
