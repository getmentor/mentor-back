const DataLoader = require("dataloader");

const Certificate = require("../../models/certificates");
const User = require("../../models/user");
const { dateToString } = require("../../helpers/date");

const certificateLoader = new DataLoader(certificate => {
  return certificates(certificate);
});

const userLoader = new DataLoader(user => {
  return User.find({ _id: { $in: user } });
});

const certificates = async certificate_id => {
  try {
    const certificates = await Certificate.find({
      _id: { $in: certificate_id }
    });
    certificates.sort((a, b) => {
      return (
        certificate_id.indexOf(a._id.toString()) -
        certificate_id.indexOf(b._id.toString())
      );
    });
    return certificates.map(certificate => {
      return transformCertificate(certificate);
    });
  } catch (err) {
    throw err;
  }
};

const singleCertificate = async certificate_id => {
  try {
    const certificate = await certificateLoader.load(certificate_id.toString());
    return certificate;
  } catch (err) {
    throw err;
  }
};

const user = async user_id => {
  try {
    const user = await userLoader.load(user_id.toString());
    return {
      ...user._doc,
      _id: user.id
    };
  } catch (err) {
    throw err;
  }
};

const transformCertificate = certificate => {
  return {
    ...certificate._doc,
    _id: certificate.id
  };
};

const transformUserCertificate = usercertificate => {
  return {
    ...usercertificate._doc,
    _id: usercertificate.id,
    user: user.bind(this, usercertificate._doc.user_id),
    certificate: singleCertificate.bind(
      this,
      usercertificate._doc.certificate_id
    ),
    createdAt: dateToString(usercertificate._doc.createdAt),
    updatedAt: dateToString(usercertificate._doc.updatedAt)
  };
};

exports.transformCertificate = transformCertificate;
exports.transformUserCertificate = transformUserCertificate;
