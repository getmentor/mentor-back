const Certificate = require("../../models/certificates");
const userCertificates = require("../../models/user_certificates");
const { listCertificates } = require("./merge");
const { transformUserCertificate } = require("./certificatemerge");
const User = require("../../models/user");

module.exports = {
  createCertificate: async args => {
    try {
      const existingCertificate = await Certificate.findOne({
        certificate: args.certificateInput.certificate
      });
      if (existingCertificate) {
        throw new Error("Certificate exists already.");
      }

      const certificate = new Certificate({
        certificate: args.certificateInput.certificate,
        status: args.certificateInput.status
      });
      const result = await certificate.save();

      return {
        _id: result.id,
        certificate: result.certificate,
        status: result.status
      };
    } catch (err) {
      throw err;
    }
  },
  certificatelist: async args => {
    try {
      const certificate = await Certificate.find({ status: 1 });
      return certificate.map(certificate => {
        return listCertificates(certificate);
      });
    } catch (err) {
      throw err;
    }
  },
  createUserCertificate: async (args, req) => {
    const existingCertificate = await userCertificates.findOne({
      certificate_id: args.certificate,
      user_id: args.user
    });
    if (existingCertificate) {
      throw new Error("Certificate exists already.");
    }

    const fetchedCertificate = await Certificate.findOne({
      _id: args.certificate
    });
    const user = await User.findOne({ _id: args.user });
    const certificate = new userCertificates({
      certificate_id: fetchedCertificate,
      user_id: user,
      description: args.description
    });
    const result = await certificate.save();
    return transformUserCertificate(result);
  },
  listUserCertificate: async (args, req) => {
    try {
      const certificate = await userCertificates.find({ user_id: args.user });
      if (!certificate) {
        throw new Error("No data found");
      }

      return certificate.map(certificate => {
        return transformUserCertificate(certificate);
      });
    } catch (err) {
      throw err;
    }
  }
};
