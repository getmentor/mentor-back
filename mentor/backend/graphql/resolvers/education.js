const Education = require("../../models/education");
const { listEducation } = require("./merge");

module.exports = {
  createUserEducation: async args => {
    try {
      const education = new Education({
        qualification: args.education.qualification,
        field: args.education.field,
        from_year: args.education.from_year,
        from_month: args.education.from_year,
        to_year: args.education.to_year,
        to_month: args.education.to_month,
        user_id: args.education.user_id
      });
      const result = await education.save();
      return { ...result._doc, _id: result.id };
    } catch (err) {
      throw err;
    }
  },
  usereducation: async args => {
    try {
      const education = await Education.find({ user_id: args.userId });
      if (!education) {
        throw new Error("No data found");
      }
      return education.map(education => {
        return listEducation(education);
      });
    } catch (err) {
      throw err;
    }
  }
};
