const Expertise = require("../../models/expertise");
const userExpertises = require("../../models/user_expertise");
const { listExpertises } = require("./merge");
const { transformUserExpertise } = require("./expertisemerge");
const User = require("../../models/user");

module.exports = {
  createExpertise: async args => {
    try {
      const existingExpertise = await Expertise.findOne({
        expertise: args.expertiseInput.expertise
      });
      if (existingExpertise) {
        throw new Error("Expertise exists already.");
      }

      const expertise = new Expertise({
        expertise: args.expertiseInput.expertise,
        status: args.expertiseInput.status
      });
      const result = await expertise.save();

      return {
        _id: result.id,
        expertise: result.expertise,
        status: result.status
      };
    } catch (err) {
      throw err;
    }
  },
  expertiselist: async args => {
    try {
      const expertise = await Expertise.find({ status: 1 });
      return expertise.map(expertise => {
        return listExpertises(expertise);
      });
    } catch (err) {
      throw err;
    }
  },
  createUserExpertise: async (args, req) => {
    const existingExpertise = await userExpertises.findOne({
      expertise_id: args.expertise,
      user_id: args.user
    });
    if (existingExpertise) {
      throw new Error("Expertise exists already.");
    }

    const fetchedExpertise = await Expertise.findOne({ _id: args.expertise });
    const user = await User.findOne({ _id: args.user });
    const expertise = new userExpertises({
      expertise_id: fetchedExpertise,
      user_id: user,
      description: args.description
    });
    const result = await expertise.save();
    return transformUserExpertise(result);
  },
  listUserExpertise: async (args, req) => {
    
    try {
      const expertise = await userExpertises.find({ user_id: args.user });
      if (!expertise) {
        throw new Error("No data found");
      }

      return expertise.map(expertise => {
        return transformUserExpertise(expertise);
      });
    } catch (err) {
      throw err;
    }
  }
};
