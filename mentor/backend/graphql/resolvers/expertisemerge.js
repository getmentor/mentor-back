const DataLoader = require("dataloader");

const Expertise = require("../../models/expertise");
const User = require("../../models/user");
const { dateToString } = require("../../helpers/date");

const expertiseLoader = new DataLoader(expertise => {
  return expertises(expertise);
});

const userLoader = new DataLoader(user => {
  return User.find({ _id: { $in: user } });
});

const expertises = async expertise_id => {
  try {
    const expertises = await Expertise.find({ _id: { $in: expertise_id } });
    expertises.sort((a, b) => {
      return (
        expertise_id.indexOf(a._id.toString()) - expertise_id.indexOf(b._id.toString())
      );
    });
    return expertises.map(expertise => {
      return transformExpertise(expertise);
    });
  } catch (err) {
    throw err;
  }
};

const singleExpertise = async expertise_id => {
  try {
    const expertise = await expertiseLoader.load(expertise_id.toString());
    return expertise;
  } catch (err) {
    throw err;
  }
};

const user = async user_id => {
  try {
    const user = await userLoader.load(user_id.toString());
    return {
      ...user._doc,
      _id: user.id
    };
  } catch (err) {
    throw err;
  }
};

const transformExpertise = expertise => {
  return {
    ...expertise._doc,
    _id: expertise.id
  };
};

const transformUserExpertise = userexpertise => {
  return {
    ...userexpertise._doc,
    _id: userexpertise.id,
    user: user.bind(this, userexpertise._doc.user_id),
    expertise: singleExpertise.bind(this, userexpertise._doc.expertise_id),
    createdAt: dateToString(userexpertise._doc.createdAt),
    updatedAt: dateToString(userexpertise._doc.updatedAt)
  };
};

exports.transformExpertise = transformExpertise;
exports.transformUserExpertise = transformUserExpertise;

// exports.user = user;
// exports.events = events;
// exports.singleEvent = singleEvent;
