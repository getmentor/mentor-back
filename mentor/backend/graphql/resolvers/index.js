const authResolver = require("./auth");
const skillResolver = require("./skill");
const languageResolver = require("./language");
const expertiseResolver = require("./expertise");
const workexperienceResolver = require("./workexperience");
const usereducationResolver = require("./education");
const toolkitResolver = require("./toolkit");
const certificateResolver = require("./certificates");

const rootResolver = {
  ...authResolver,
  ...skillResolver,
  ...languageResolver,
  ...expertiseResolver,
  ...workexperienceResolver,
  ...usereducationResolver,
  ...toolkitResolver,
  ...certificateResolver
};

module.exports = rootResolver;
