const Language = require("../../models/languages");
const { listLanguages } = require("./merge");

module.exports = {
  createLanguage: async args => {
    try {
      const existingLang = await Language.findOne({
        language: args.langInput.language
      });
      if (existingLang) {
        throw new Error("Language exists already.");
      }

      const language = new Language({
        language: args.langInput.language,
        status: args.langInput.status
      });
      const result = await language.save();

      return {
        _id: result.id,
        language: result.language,
        status: result.status
      };
    } catch (err) {
      throw err;
    }
  },
  language: async args => {
    try {
      const language = await Language.find({ status: 1 });
      return language.map(language => {
        return listLanguages(language);
      });
    } catch (err) {
      throw err;
    }
  }
};
