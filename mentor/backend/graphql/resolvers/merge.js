const User = require("../../models/user");
const Skills = require("../../models/skills");
const { dateToString } = require("../../helpers/date");
const DataLoader = require("dataloader");

const user = async userId => {
  try {
    const user = await User.findById(userId);
    return {
      ...user._doc,
      _id: user.id
    };
  } catch (err) {
    throw err;
  }
};

const users = async userIds => {
  try {
    const users = await User.find({ _id: { $in: userIds } });
    return users.map(user => {
      return listUser(user);
    });
  } catch (err) {
    throw err;
  }
};

const listUser = user => {
  return {
    ...user._doc,
    _id: user.id,
    password: null
  };
};
const listskills = skill => {
  return {
    ...skill._doc,
    _id: skill.id
  };
};
const listMentors = user => {
  return {
    ...user._doc,
    _id: user.id,
    password: null
  };
};
const listLanguages = langauges => {
  return {
    ...langauges._doc,
    _id: langauges.id
  };
};
const listExpertises = expertise => {
  return {
    ...expertise._doc,
    _id: expertise.id
  };
};
const listToolkit = toolkit => {
  return {
    ...toolkit._doc,
    _id: toolkit.id
  };
};
const listCertificates = certificate => {
  return {
    ...certificate._doc,
    _id: certificate.id
  };
};

const userLoader = new DataLoader(user => {
  return User.find({ _id: { $in: user } });
});

const userexp = async user_id => {
  try {
    const user = await userLoader.load(user_id.toString());
    return {
      ...user._doc,
      _id: user.id
    };
  } catch (err) {
    throw err;
  }
};

const listExperience = experience => {
  return {
    ...experience._doc,
    _id: experience.id,
    user_id: userexp.bind(this, experience._doc.user_id)
  };
};

const listEducation = education => {
  return {
    ...education._doc,
    _id: education.id,
    user_id: userexp.bind(this, education._doc.user_id)
  };
};

exports.listMentors = listMentors;
exports.listUser = listUser;
exports.user = user;
exports.listSkills = listskills;
exports.listLanguages = listLanguages;
exports.listExpertises = listExpertises;
exports.listExperience = listExperience;
exports.listEducation = listEducation;
exports.listToolkit = listToolkit;
exports.listCertificates = listCertificates;
