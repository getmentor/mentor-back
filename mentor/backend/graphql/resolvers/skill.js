const Skills = require("../../models/skills");
const { listSkills } = require("./merge");

module.exports = {
  createSkill: async args => {
    try {
      const existingSkill = await Skills.findOne({
        skill: args.skillInput.skill
      });
      if (existingSkill) {
        throw new Error("Skill exists already.");
      }

      const skill = new Skills({
        skill: args.skillInput.skill,
        status: args.skillInput.status
      });
      const result = await skill.save();

      return { _id: result.id, skill: result.skill, status: result.status };
    } catch (err) {
      throw err;
    }
  },
  skills: async args => {
    try {
      const skills = await Skills.find({ status: 1 });
      return skills.map(skill => {
        return listSkills(skill);
      });
    } catch (err) {
      throw err;
    }
  }
};
