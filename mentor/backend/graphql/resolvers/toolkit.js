const Toolkit = require("../../models/toolkit");
const userToolkit = require("../../models/user_toolkit");
const { listToolkit } = require("./merge");
const { transformUserToolkit } = require("./toolkitmerge");
const User = require("../../models/user");

module.exports = {
  createToolkit: async args => {
    try {
      const existingToolkit = await Toolkit.findOne({
        toolkit: args.toolkitInput.toolkit
      });
      if (existingToolkit) {
        throw new Error("Toolkit exists already.");
      }

      const toolkit = new Toolkit({
        toolkit: args.toolkitInput.toolkit,
        status: args.toolkitInput.status
      });
      const result = await toolkit.save();

      return {
        _id: result.id,
        toolkit: result.toolkit,
        status: result.status
      };
    } catch (err) {
      throw err;
    }
  },
  toolkitlist: async args => {
    try {
      const toolkit = await Toolkit.find({ status: 1 });
      return toolkit.map(toolkit => {
        return listToolkit(toolkit);
      });
    } catch (err) {
      throw err;
    }
  },
  createUserToolkit: async (args, req) => {
    const existingToolkit = await userToolkit.findOne({
      toolkit_id: args.toolkit,
      user_id: args.user
    });
    if (existingToolkit) {
      throw new Error("Toolkit exists already.");
    }

    const fetchedToolkit = await Toolkit.findOne({ _id: args.toolkit });
    const user = await User.findOne({ _id: args.user });
    const toolkit = new userToolkit({
      toolkit_id: fetchedToolkit,
      user_id: user,
      description: args.description
    });
    const result = await toolkit.save();
    return transformUserToolkit(result);
  },
  listUserToolkit: async (args, req) => {
    try {
      const toolkit = await userToolkit.find({ user_id: args.user });
      if (!toolkit) {
        throw new Error("No data found");
      }

      return toolkit.map(toolkit => {
        return transformUserToolkit(toolkit);
      });
    } catch (err) {
      throw err;
    }
  }
};
