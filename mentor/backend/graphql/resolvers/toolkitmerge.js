const DataLoader = require("dataloader");

const Toolkit = require("../../models/toolkit");
const User = require("../../models/user");
const { dateToString } = require("../../helpers/date");

const toolkitLoader = new DataLoader(toolkit => {
  return toolkits(toolkit);
});

const userLoader = new DataLoader(user => {
  return User.find({ _id: { $in: user } });
});

const toolkits = async toolkit_id => {
  try {
    const toolkits = await Toolkit.find({ _id: { $in: toolkit_id } });
    toolkits.sort((a, b) => {
      return (
        toolkit_id.indexOf(a._id.toString()) -
        toolkit_id.indexOf(b._id.toString())
      );
    });
    return toolkits.map(toolkit => {
      return transformToolkit(toolkit);
    });
  } catch (err) {
    throw err;
  }
};

const singleToolkit = async toolkit_id => {
  try {
    const toolkit = await toolkitLoader.load(toolkit_id.toString());
    return toolkit;
  } catch (err) {
    throw err;
  }
};

const user = async user_id => {
  try {
    const user = await userLoader.load(user_id.toString());
    return {
      ...user._doc,
      _id: user.id
    };
  } catch (err) {
    throw err;
  }
};

const transformToolkit = toolkit => {
  return {
    ...toolkit._doc,
    _id: toolkit.id
  };
};

const transformUserToolkit = usertoolkit => {
  return {
    ...usertoolkit._doc,
    _id: usertoolkit.id,
    user: user.bind(this, usertoolkit._doc.user_id),
    toolkit: singleToolkit.bind(this, usertoolkit._doc.toolkit_id),
    createdAt: dateToString(usertoolkit._doc.createdAt),
    updatedAt: dateToString(usertoolkit._doc.updatedAt)
  };
};

exports.transformToolkit = transformToolkit;
exports.transformUserToolkit = transformUserToolkit;

// exports.user = user;
// exports.events = events;
// exports.singleEvent = singleEvent;
