const Experience = require("../../models/work_experience");
const { listExperience } = require("./merge");

module.exports = {
  createWorkExperience: async args => {
    try {
      const experience = new Experience({
        company_name: args.workExp.company_name,
        job_title: args.workExp.job_title,
        location: args.workExp.location,
        from_year: args.workExp.from_year,
        from_month: args.workExp.from_year,
        to_year: args.workExp.to_year,
        to_month: args.workExp.to_month,
        present: args.workExp.present,
        description: args.workExp.description,
        user_id: args.workExp.user_id
      });
      const result = await experience.save();
      return { ...result._doc, _id: result.id };
    } catch (err) {
      throw err;
    }
  },
  experiences: async args => {
    try {
      const experiences = await Experience.find({ user_id: args.userId });
      if (!experiences) {
        throw new Error("No data found");
      }
      return experiences.map(experience => {
        return listExperience(experience);
      });
    } catch (err) {
      throw err;
    }
  }
};
