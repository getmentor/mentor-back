const { buildSchema } = require("graphql");

module.exports = buildSchema(`

type User {
  _id: ID!
  name:String!
  email: String!
  password: String
  skills:String
  resume_link:String
  role:Int!
  status : Int
  createdAt: String!
  updatedAt: String!
  
}
type Mentors {
  _id: ID!
  name:String!
  last_name:String
  email: String!
  password: String
  skills:String!
  resume_link:String
  profile_pic :String
  short_desc :String
  main_desc :String
  language :String
  location :String
  linkedin_url: String
  twitte_url: String
  medium_url: String
  role:Int!
  status : Int
  createdAt: String
  updatedAt: String
  
}
type MentorEdit {
  _id: ID!  
}

type AuthData {
  userId: ID!
  token: String!
  tokenExpiration: Int!
  email:String!
  role:String!
}
type MentorDetails {
  userId: ID!
  name:String!
  last_name:String
  email:String!
  role:String!
  main_desc: String
  short_desc: String
  profile_pic: String
  resume_link: String
  linkedin_url: String
  twitte_url: String
  medium_url: String
}
type Skill {
  _id: ID!
  skill:String!
  status : Int!
}
type SkillList{
  _id: ID!
  skill:String!
  status : Int!
}
type Language {
  _id: ID!
  language:String!
  status : Int!
}
type LanguageList{
  _id: ID!
  language:String!
  status : Int!
}
type Expertise{
  _id: ID!
  expertise:String!
  status : Int!
}
type ExpertiseList{
  _id: ID!
  expertise:String!
  status : Int!
}
type UserExpertise{
  _id: ID!
  expertise_id:Expertise!
  user_id : User!
  description :String!
}
type UserExpertiseList{
  _id: ID!
  expertise:Expertise!
  user: User!
  description :String
}
type ListUserExpertise {
  _id: ID!
  expertise:Expertise!
  user: User!
  description :String!
}
type Experience {
  _id: ID!
  company_name:String!
  job_title:String!
  location: String!
  from_year: String
  from_month:String
  to_year:String
  to_month :String
  present :Int
  description :String  
  
}
type ResetStatus{
  _id:ID!
  email: String!
  password: String
}
type Education{
  _id:ID!
  qualification: String!
  field: String!
  from_year: String
  from_month: String
  to_year: String
  to_month: String

}
type Toolkit{
  _id: ID!
  toolkit:String!
  status : Int!
}
type Certificate{
  _id: ID!
  certificate:String!
  status : Int!
}

type UserToolkit{
  _id: ID!
  toolkit:Toolkit!
  user: User!
  description :String
}

type UserCertificate{
  _id: ID!
  certificate:Certificate!
  user: User!
  description :String
}

type ToolkitList{
  _id: ID!
  toolkit:String!
  status : Int!
}

type ListUserToolkit {
  _id: ID!
  toolkit:Toolkit!
  user: User!
  description :String!
}

type CertificateList{
  _id: ID!
  certificate:String!
  status : Int!
}

type ListUserCertificate {
  _id: ID!
  certificate:Certificate!
  user: User!
  description :String!
}

input UserInput {
  name:String!
  last_name:String!
  email: String!
  password: String!
  skills:String!
  resume_link:String!
  profile_pic :String!
  short_desc :String!
  main_desc :String!
  language :String!
  location :String!
  linkedin_url: String!
  twitte_url: String!
  medium_url: String!
  role:Int!
  status : Int
}
input SkillInput {
  skill:String!
  status:Int!
}
input LanuageInput {
  language:String!
  status:Int!
}
input expertiseInput{
  expertise:String!
  status:Int!
}

input WorkExp {
  company_name:String!
  job_title:String!
  location: String!
  from_year: String
  from_month:String
  to_year:String
  to_month :String
  present :Int!
  description :String  
  user_id:ID!
}

input ResetPassword { 
  email: String!
  password: String!
  confirmpassword:String!
}

input UserEducation{
  qualification: String!
  field: String!
  from_year: String
  from_month: String
  to_year: String
  to_month: String
  user_id:ID!
}

input ToolkitInput{
  toolkit:String!
  status:Int!
}
input CertificateInput{
  certificate:String!
  status:Int!
}
input MentorInput {
  name:String!
  last_name:String
  email: String!
  resume_link:String
  profile_pic :String
  short_desc :String
  main_desc :String
  linkedin_url: String
  twitte_url: String
  medium_url: String  
}


type RootQuery {
    users: [User!]!
    mentors(first: Int!, last: Int!):[Mentors!]!
    totalmentors:[Mentors!]!
    mentorDetails(userId: String!): MentorDetails
    login(email: String!, password: String!): AuthData!
    skills:[SkillList!]!
    language:[LanguageList!]!
    expertiselist:[ExpertiseList]!
    userexpertise:UserExpertise!
    listUserExpertise(user:ID!): [ListUserExpertise!]!
    experiences(userId: ID!):[Experience!]!
    usereducation(userId: ID!):[Education!]!
    toolkitlist:[ToolkitList]!
    listUserToolkit(user:ID!): [ListUserToolkit!]!
    certificatelist:[CertificateList]!
    listUserCertificate(user:ID!): [ListUserCertificate!]!
    userDetails(userId: String!): MentorDetails

}

type RootMutation {
    createUser(userInput: UserInput): User
    createSkill(skillInput:SkillInput):Skill
    createLanguage(langInput:LanuageInput):Language
    createExpertise(expertiseInput:expertiseInput):Expertise
    createUserExpertise(expertise: ID!,user: ID!, description: String!): UserExpertiseList!
    createWorkExperience(workExp:WorkExp):Experience
    resetPassword(resetpassword:ResetPassword):ResetStatus
    createUserEducation(education:UserEducation):Education
    createToolkit(toolkitInput:ToolkitInput):Toolkit
    createUserToolkit(toolkit: ID!,user: ID!, description: String!): UserToolkit!
    createCertificate(certificateInput:CertificateInput):Certificate
    createUserCertificate(certificate: ID!,user: ID!, description: String!): UserCertificate!
    editMentorProfile(mentorInput:MentorInput):MentorEdit
}

schema {
    query: RootQuery
    mutation: RootMutation
}
`);
