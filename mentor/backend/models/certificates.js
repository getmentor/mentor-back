const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const certificateSchema = new Schema(
  {
    certificate: {
      type: String,
      required: true
    },
    status: {
      type: Number,
      required: false
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Certificate", certificateSchema);
