const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const educationSchema = new Schema(
  {
    qualification: {
      type: String,
      required: true
    },
    field: {
      type: String,
      required: true
    },
    from_year: {
      type: String,
      required: false
    },
    from_month: {
      type: String,
      required: false
    },
    to_year: {
      type: String,
      required: false
    },
    to_month: {
      type: String,
      required: false
    },
    user_id: {
      type: Schema.Types.ObjectId,
      ref: "User"
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Education", educationSchema);
