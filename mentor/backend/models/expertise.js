const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const expertiseSchema = new Schema(
  {
    expertise: {
      type: String,
      required: true
    },
    status: {
      type: Number,
      required: false
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Expertise", expertiseSchema);
