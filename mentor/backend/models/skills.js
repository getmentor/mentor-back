const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const skillsSchema = new Schema(
  {
    skill: {
      type: String,
      required: true
    },
    status: {
      type: Number,
      required: false
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Skills", skillsSchema);
