const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const toolKitSchema = new Schema(
  {
    toolkit: {
      type: String,
      required: true
    },
    status: {
      type: Number,
      required: false
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Toolkit", toolKitSchema);
