const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    name: {
      type: String,
      required: true
    },
    last_name: {
      type: String,
      required: false
    },
    email: {
      type: String,
      required: true
    },
    password: {
      type: String,
      required: true
    },
    skills: {
      type: String,
      required: false
    },
    resume_link: {
      type: String,
      required: false
    },
    profile_pic: {
      type: String,
      required: false
    },
    short_desc: {
      type: String,
      required: false
    },
    main_desc: {
      type: String,
      required: false
    },
    language: {
      type: String,
      required: false
    },
    location: {
      type: String,
      required: false
    },
    linkedin_url: {
      type: String,
      required: false
    },
    twitte_url: {
      type: String,
      required: false
    },
    medium_url: {
      type: String,
      required: false
    },
    role: {
      type: Number,
      required: true
    },
    stripe_id: {
      type: String,
      required: false
    },
    status: {
      type: Number,
      required: false
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("User", userSchema);
