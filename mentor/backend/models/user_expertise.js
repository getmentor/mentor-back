const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const userExpertiseSchema = new Schema(
  {
    expertise_id: {
      type: Schema.Types.ObjectId,
      ref: "Expertise"
    },
    user_id: {
      type: Schema.Types.ObjectId,
      ref: "User"
    },
    description: {
      type: String,
      required: false
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("UserExpertise", userExpertiseSchema);
