const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const workExperienceSchema = new Schema(
  {
    company_name: {
      type: String,
      required: true
    },
    job_title: {
      type: String,
      required: true
    },
    location: {
      type: String,
      required: true
    },
    from_year: {
      type: String,
      required: false
    },
    from_month: {
      type: String,
      required: false
    },
    to_year: {
      type: String,
      required: false
    },
    to_month: {
      type: String,
      required: false
    },
    present: {
      type: Number,
      required: true
    },
    description: {
      type: String,
      required: false
    },
    user_id: {
      type: Schema.Types.ObjectId,
      ref: "User"
    }
  },
  { timestamps: true }
);

module.exports = mongoose.model("WorkExperience", workExperienceSchema);
