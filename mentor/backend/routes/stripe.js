const router = require("express").Router();
const stripe = require("stripe")("sk_test_LBzReqBt1p9MltgkkVKntIn100p8wS1dhx");
let User = require("../models/user");

router.route("/retrievecustomer").post((req, res) => {
  console.log(req.body.userId);

  User.findOne({ _id: req.body.userId })
    .then(result => {
      stripe.customers.retrieve(result.stripe_id, function(err, customer) {
        res.send(customer);
      });
    })
    .catch(err => {});
});
router.route("/retrievecard").post((req, res) => {
  console.log(req.body.userId);

  User.findOne({ _id: req.body.userId })
    .then(result => {
      stripe.customers.listSources(
        result.stripe_id,
        {
          object: "card"
        },
        function(err, cards) {
          if (err) {
            console.log("Retrievecard: Stripe card retrieval failed");
            res.send(err);
          } else {
            if (cards.data.length > 0) {
              console.log("Retrievecard: Stripe card retrieval Succeeded");
              res.send(cards);
            } else {
              console.log("Retrievecard: Stripe card retrieval failed");
              res.send(cards);
            }
          }
        }
      );
    })
    .catch(err => {
      res.send(err);
    });
});
router.route("/deletecard").post((req, res) => {
  User.findOne({ _id: req.body.userId })
    .then(result => {
      stripe.customers.deleteSource(result.stripe_id, req.body.id, function(
        err,
        cards
      ) {
        if (err) {
          console.log("deletecard failed");
          res.send(err);
        } else {
          if (cards) {
            console.log("deletecard Succeeded");
            res.send(cards);
          } else {
            console.log("deletecard failed");
            res.send(cards);
          }
        }
      });
    })
    .catch(err => {
      res.send(err);
    });
});
router.route("/retrievesinglecard").post((req, res) => {
  // console.log(req.body.userId);

  User.findOne({ _id: req.body.userId })
    .then(result => {
      stripe.customers.retrieveSource(result.stripe_id, req.body.id, function(
        err,
        cards
      ) {
        if (err) {
          console.log("Retrievesinglecard: Stripe card retrieval failed");
          res.send(err);
        } else {
          if (cards) {
            console.log("Retrievesinglecard:Stripe card retrieval Succeeded");
            res.send(cards);
          } else {
            console.log("Retrievesinglecard:Stripe card retrieval failed");
            res.send(cards);
          }
        }
      });
    })
    .catch(err => {
      res.send(err);
    });
});
router.route("/updatecard").post((req, res) => {
  // console.log(req.body.userId);

  User.findOne({ _id: req.body.userId })
    .then(result => {
      stripe.customers.updateSource(
        result.stripe_id,
        req.body.id,
        {
          name: req.body.name,
          exp_month: req.body.exp_month,
          exp_year: req.body.exp_year
        },
        function(err, cards) {
          if (err) {
            console.log("Updatecard: Stripe card retrieval failed");
            res.send(err);
          } else {
            if (cards) {
              console.log("Updatecard:Stripe card retrieval Succeeded");
              res.send(cards);
            } else {
              console.log("Updatecard:Stripe card retrieval failed");
              res.send(cards);
            }
          }
        }
      );
    })
    .catch(err => {
      res.send(err);
    });
});
router.route("/createcard").post((req, res) => {
  // console.log(req.body.token);

  User.findOne({ _id: req.body.userId })
    .then(result => {
      stripe.customers.createSource(
        result.stripe_id,
        {
          source: req.body.token
        },
        function(err, card) {
          if (err) {
            console.log("Createcard:Stripe card add failed");
            res.send(err);
          } else {
            console.log("Createcard:Stripe card add Succeeded");

            res.send(card);
          }
        }
      );
    })
    .catch(err => {});
});

module.exports = router;
