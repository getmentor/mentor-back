const router = require("express").Router();
let User = require("../models/user");

const nodeMailer = require("nodemailer");
const SMTPConnection = require("nodemailer/lib/smtp-connection");
var handlebars = require("handlebars");
var fs = require("fs");
var paths = require("path");

var readHTMLFile = function(path, callback) {
  fs.readFile(path, { encoding: "utf-8" }, function(err, html) {
    if (err) {
      throw err;
      callback(err);
    } else {
      callback(null, html);
    }
  });
};

router.route("/updateconfirmstatus/:email/:status").get((req, res) => {
  let status = req.params.status;
  let email = req.params.email;
  //console.log(req.params.email);

  User.findOne({ email: req.params.email }).then(user => {
    user.status = Number(status);
    user.save();
  });

  User.findOne({ email: req.params.email })
    .then(result => {
      if (status == 1) {
        return res.redirect(
          "https://react-mentor.herokuapp.com/registration-success"
        );
      } else if (status == 2) {
        /*  -------Email Functionality  : Start--------  */

        var mailConfig;
        const name = result.name;
        const skills = result.skills;
        let email_user = result.email;
        // const email_admin = "mentor.wds@gmail.com";
        const email_admin = "msafari@outlook.it";

        const uid = result.id;
        console.log(name);
        if (process.env.NODE_ENV === "production") {
          // all emails are delivered to destination

          mailConfig = {
            host: "smtp.sendgrid.net",
            port: 587,
            auth: {
              user: "app141342349@heroku.com",
              pass: "b7baphmr4004"
            }
          };
        } else {
          // all emails are catched by ethereal.email
          mailConfig = {
            host: "smtp.ethereal.email",
            port: 587,
            auth: {
              user: "rex.kovacek39@ethereal.email",
              pass: "7wwvw1KjgeJc65gnVg"
            }
          };
        }

        let transporter = nodeMailer.createTransport(mailConfig);

        let dirpath = paths.resolve(
          __dirname,
          "../email_templates/approve-teacher.html"
        );

        let approve =
          "https://react-node-mentor.herokuapp.com/users/userapprove/" +
          email_user;

        let reject =
          "https://react-node-mentor.herokuapp.com/users/userdecline/" +
          email_user +
          "/" +
          uid;

        readHTMLFile(dirpath, function(err, html) {
          var template = handlebars.compile(html);
          var replacements = {
            email_admin: email_admin,
            email_user: email_user,
            name: name,
            skills: skills,
            approve: approve,
            reject: reject
          };
          var htmlToSend = template(replacements);
          let mailOptions = {
            from: '"Mentor" <app141342349@heroku.com>', // sender address
            to: email_admin, // list of receivers
            subject: "Mentor", // Subject line
            html: htmlToSend
          };
          transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
              status = 1;
              console.log(error);
            } else {
              status = 0;
              console.log("Message %s sent: %s", info.messageId, info.response);
            }
          });
        });

        /*  -------Email Functionality  : End--------  */
        return res.redirect(
          "https://react-mentor.herokuapp.com/registration-success-teach"
        );
      } else {
        return res.redirect("https://react-mentor.herokuapp.com/");
      }
    })
    .catch(err => {
      return res.redirect("https://react-mentor.herokuapp.com/");
    });
});

router.route("/userapprove/:email").get((req, res) => {
  let status = 1;
  User.findOne({ email: req.params.email }).then(user => {
    user.status = Number(status);
    user.save();
  });

  /*  -------Email Functionality  : Start--------  */

  var mailConfig;
  let email_user = req.params.email;

  if (process.env.NODE_ENV === "production") {
    // all emails are delivered to destination

    mailConfig = {
      host: "smtp.sendgrid.net",
      port: 587,
      auth: {
        user: "app141342349@heroku.com",
        pass: "b7baphmr4004"
      }
    };
  } else {
    // all emails are catched by ethereal.email
    mailConfig = {
      host: "smtp.ethereal.email",
      port: 587,
      auth: {
        user: "rex.kovacek39@ethereal.email",
        pass: "7wwvw1KjgeJc65gnVg"
      }
    };
  }

  let transporter = nodeMailer.createTransport(mailConfig);

  let dirpath = paths.resolve(
    __dirname,
    "../email_templates/admin-approved-account.html"
  );

  readHTMLFile(dirpath, function(err, html) {
    var template = handlebars.compile(html);
    var replacements = {
      email_user: email_user
    };
    var htmlToSend = template(replacements);
    let mailOptions = {
      from: '"Mentor" <app141342349@heroku.com>', // sender address
      to: email_user, // list of receivers
      subject: "Mentor", // Subject line
      html: htmlToSend
    };
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        status = 1;
        console.log(error);
      } else {
        status = 0;
        console.log("Message %s sent: %s", info.messageId, info.response);
      }
    });
  });

  return res.redirect(
    "https://react-mentor.herokuapp.com/account-approved-successfully"
  );

  /*  -------Email Functionality  : End--------  */
});

router.route("/userdecline/:email/:id").get((req, res) => {
  let email_user = req.params.email;
  User.findByIdAndDelete(req.params.id)
    .then(() => res.json("User deleted."))
    .catch(err => res.status(400).json("Error: " + err));

  /*  -------Email Functionality  : Start--------  */

  var mailConfig;

  if (process.env.NODE_ENV === "production") {
    // all emails are delivered to destination

    mailConfig = {
      host: "smtp.sendgrid.net",
      port: 587,
      auth: {
        user: "app141342349@heroku.com",
        pass: "b7baphmr4004"
      }
    };
  } else {
    // all emails are catched by ethereal.email
    mailConfig = {
      host: "smtp.ethereal.email",
      port: 587,
      auth: {
        user: "rex.kovacek39@ethereal.email",
        pass: "7wwvw1KjgeJc65gnVg"
      }
    };
  }

  let transporter = nodeMailer.createTransport(mailConfig);

  let dirpath = paths.resolve(
    __dirname,
    "../email_templates/admin-rejected-your-account.html"
  );

  readHTMLFile(dirpath, function(err, html) {
    var template = handlebars.compile(html);
    var replacements = {
      email_user: email_user
    };
    var htmlToSend = template(replacements);
    let mailOptions = {
      from: '"Mentor" <app141342349@heroku.com>', // sender address
      to: email_user, // list of receivers
      subject: "Mentor", // Subject line
      html: htmlToSend
    };
    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        status = 1;
        console.log(error);
      } else {
        status = 0;
        console.log("Message %s sent: %s", info.messageId, info.response);
      }
    });
  });

  return res.redirect(
    "https://react-mentor.herokuapp.com/account-rejected-successfully"
  );

  /*  -------Email Functionality  : End--------  */
});
module.exports = router;
