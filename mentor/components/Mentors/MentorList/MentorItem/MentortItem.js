import React from "react";

import "./MentorItem.css";
import Link from "next/link";

const mentorItem = props => (
  <div className="container teacher-info">
    <div className="columns inner">
      <div className="left">
        <figure className="image">
          <img
            className="is-rounded"
            src={"../static/uploads/users/" + props.profile_pic}
          />
        </figure>
        <ul className="is-inline-block">
          <li className="is-inline-block">
            <span className="spec">Price</span>
            <span className="value free">Free</span>
          </li>
          <li className="is-inline-block">
            <span className="spec">Reviews</span>
            <span className="value">99</span>
          </li>
        </ul>
      </div>
      <div className="right">
        <div className="spec-wrap is-inline-block">
          <div className="spec-inner is-inline-block">
            <div className="left-blk">
              <h1 className="title is-4 teacher-name">{props.name}</h1>
              <h1 className="subtitle is-6 teacher-name">{props.short_desc}</h1>
              <div className="columns">
                <div className="column is-two-fifths location-wrap">
                  <p>
                    <img src="../static/location.png" alt="location-icon" />
                    <span className="subtitle">
                      <span className="current-city">{props.location}</span>
                      {/* <span className="current-country">Spain</span> */}
                    </span>
                  </p>
                  <p>
                    <span className="subtitle">(+01:00 UTC)</span>
                  </p>
                </div>
                <div className="column is-one-fifth language-wrap">
                  <p>
                    <span className="subtitle">Spanish</span>
                  </p>
                  <p>
                    <span className="subtitle">English</span>
                  </p>
                </div>
                <div className="column is-two-fifths native-wrap">
                  <p>
                    <span className="subtitle">
                      <span className="current-city">Christchurch</span>
                      <span className="current-country">Newzealand</span>
                    </span>
                  </p>
                </div>
              </div>
            </div>
            <div className="right-blk">
              <p className="is-inline-block">
                <img src="../static/star.png" alt="star-rating" />
                <p className="title is-5 is-inline-block rating">4.5</p>
                <p className="subtitle is-6 is-inline-block">99 sessions</p>
              </p>
              <div className="button is-success is-outlined view-profile">
                <Link
                  href={{
                    pathname: "/teacher-details",
                    query: { mentorId: props.mentorId }
                  }}
                  as="/teacher-details"
                >
                  <a>View profile</a>
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className="column spec-wrap">
          <div className="container">
            <p className="subtitle is-6 des">{props.main_desc}</p>
            <div class="tags">
              <span class="tag">{props.skills}</span>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default mentorItem;
