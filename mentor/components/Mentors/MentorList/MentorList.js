import React from "react";

import MentorItem from "../MentorList/MentorItem/MentortItem";
import "./MentorList.css";

const mentorList = props => {
  const mentors = props.mentors.map(mentor => {
    return (
      <MentorItem
        key={mentor._id}
        mentorId={mentor._id}
        name={mentor.name}
        email={mentor.email}
        skills={mentor.skills}
        profile_pic={mentor.profile_pic}
        short_desc={mentor.short_desc}
        main_desc={mentor.main_desc}
        language={mentor.language}
        location={mentor.location}
      />
    );
  });

  return <ul className="mentor__list">{mentors}</ul>;
};

export default mentorList;
