import React, { Component } from 'react';
import './backDrop.scss';

class BackDrop extends Component {
  handleOutsideClick = () => {
    document.getElementById('mobile-menu-wrap').classList.remove('show-menu');
    document.getElementById('backdrop').classList.remove('show');
  };
  render() {
    return (
      <>
        <div
          className="backdrop"
          id="backdrop"
          onClick={this.handleOutsideClick}
        />
      </>
    );
  }
}
export default BackDrop;
