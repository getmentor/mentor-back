import React, { Component } from "react";
import "./chat-footer.scss";
import QiscusSDKCore from "qiscus-sdk-core";
const qiscus = new QiscusSDKCore();

export default class ChatFooter extends Component {
  render() {
    return (
      <>
        <div className="level chat-footer">
          <div className="input-wrap">
            <textarea col="1" rows="1" placeholder="Your Message" />
          </div>
          <div className="btn-wrap">
            <button className="button">Send</button>
          </div>
        </div>
      </>
    );
  }
}
