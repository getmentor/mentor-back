import React, { Component } from "react";
import "./chat-header.scss";

export default class ChatHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: ""
    };
  }
  componentDidMount() {
    this.setState({ username: sessionStorage.getItem("mentor_name") });
  }
  render() {
    return (
      <>
        <div className="chat-header">
          <span className="recipient-name title is-6 is-inline-block">
            {this.state.username}
            <span className="chat-header-typing">
              <i>typing....</i>
            </span>
          </span>
          <a href="/" className="media">
            <img src="../static/desk-microphone.png" alt="microphone-icon" />
          </a>
          <a href="/" className="media">
            <img src="../static/camera-video.png" alt="microphone-icon" />
          </a>

          <a href="/" className="clear-chat subtitle is-6 is-inline-block">
            <img src="../static/delete.png" alt="delete-icon" />
          </a>
        </div>
      </>
    );
  }
}
