import React, { Component } from "react";
import "./chat-window.scss";
import QiscusSDKCore from "qiscus-sdk-core";
const qiscus = new QiscusSDKCore();
import Router, { withRouter } from "next/router";
import Moment from "react-moment";
import env from "../../constant.json";

class ChatWindow extends Component {
  constructor(props) {
    super(props);
    this.state = {
      roomid: "",
      username: "",
      useremail: "",
      userimage: "",
      messages: [],
      mentor_email: "",
      mentor_username: "",
      typingstatus: ""
    };

    this.message = React.createRef();
  }
  componentWillMount() {
    // if (this.props.router.query) {
    //   sessionStorage.setItem(
    //     "mentor_name",
    //     this.props.router.query.mentor_name
    //   );
    //   sessionStorage.setItem(
    //     "mentor_email",
    //     this.props.router.query.mentor_email
    //   );
    //   sessionStorage.setItem(
    //     "mentor_image",
    //     this.props.router.query.mentor_image
    //   );
    // }
  }

  componentDidMount() {
    this.fetchUser();
    this.setState({
      mentor_email: sessionStorage.getItem("mentor_email")
    });

    //this.CreateChatRoom();
  }

  //  Initialization Qiscus Chat SDK
  Initialization() {
    qiscus.init({
      AppId: "mentor-r6ledbypleisco", //"mentorcha-ehkoh6xenpl", //new : mentor-r6ledbypleisco
      options: {
        commentDeletedCallback: function(data) {},
        commentDeliveredCallback: function(data) {},
        commentReadCallback: function(data) {},
        presenceCallback: function(data) {},
        typingCallback: data => {
          console.log("Typing.........");
          this.setState({ typingstatus: true });
        },
        onReconnectCallback: function(data) {},
        newMessagesCallback: messages => {
          messages.forEach(message => {
            console.log(message.message);
            this.loadComments();
          });
        },
        roomClearedCallback: function(data) {}
      }
    });
    // console.log(qiscus);
  }

  //  AuthenticateUser to Qiscus

  AuthenticateUser = event => {
    const user_name = this.state.username;
    const user_email = this.state.useremail;
    const user_image = this.state.userimage;

    qiscus
      .setUser(user_email, user_email, user_name, user_image, {})
      .then(response => {
        this.CreateChatRoom();
      })
      .catch(function(error) {});

    // console.log(qiscus);
  };

  //Create Chat Room

  CreateChatRoom = event => {
    let mentor_name = sessionStorage.getItem("mentor_name");
    let mentor_email = sessionStorage.getItem("mentor_email");
    let mentor_image = sessionStorage.getItem("mentor_image");

    this.setState({ mentor_username: sessionStorage.getItem("mentor_name") });

    qiscus
      .chatTarget(mentor_email)
      .then(room => {
        this.setState({ roomid: room.id });
        this.loadComments();
        // this.deleteComments();
      })
      .catch(function(error) {});
  };

  //Send Message

  SendMessage = event => {
    const message = this.message.current.value;
    const room_id = this.state.roomid;

    qiscus
      .sendComment(room_id, message)
      .then(comment => {
        this.loadComments();
        this.message.current.value = "";
        // On success
      })
      .catch(function(error) {
        // On error
      });
  };

  onSend = event => {
    this.SendMessage();
  };

  //loadComments

  loadComments() {
    const roomId = this.state.roomid;

    const options = {
      limit: 10
    };

    qiscus
      .loadComments(roomId, options)
      .then(async comments => {
        // console.log("comments", comments);
        await this.setState({ messages: comments });
        // console.log("comments", this.state.messages);
        // On success
      })
      .catch(function(error) {
        // On error
      });
  }

  //delete comment
  deleteComments = event => {
    // const roomId = this.state.roomid;
    const roomId = "3687094";
    const commentUniqueIds = "e8a262741f28c7b19c542000bebddb2d";
    qiscus
      .deleteComment(roomId, commentUniqueIds)
      .then(function(comment) {
        // On success
      })
      .catch(function(error) {
        // On error
      });
  };
  //clear chat

  clearChat = event => {
    const roomIds = "3687094";
    qiscus
      .clearRoomMessages([roomIds])
      .then(function(rooms) {
        // On success
      })
      .catch(function(error) {
        // On error
      });
  };

  async fetchUser() {
    const userId = localStorage.getItem("userId");

    let requestBody = {
      query: `
        query userDetails($userId: String!) {
          userDetails(userId: $userId) {
            userId    
            name   
            last_name     
            email
            role
            main_desc
            short_desc
            profile_pic
            resume_link
            linkedin_url
            twitte_url
            medium_url
          }
        }
      `,
      variables: {
        userId: userId
      }
    };

    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(async resData => {
        if (resData.data.userDetails) {
          await this.setState({
            username: resData.data.userDetails.name
          });
          await this.setState({
            userimage: resData.data.userDetails.profile_pic
          });
          await this.setState({ useremail: resData.data.userDetails.email });

          window.scrollTo(0, 0);

          this.Initialization();
          this.AuthenticateUser();
          // this.CreateChatRoom();
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    return (
      <>
        <div className="chat-header">
          <span className="recipient-name title is-6 is-inline-block">
            {this.state.mentor_username}
            <span className="chat-header-typing">
              {this.state.typingstatus ? <i>typing....</i> : null}
            </span>
          </span>
          <a href="/" className="media">
            <img src="../static/desk-microphone.png" alt="microphone-icon" />
          </a>
          <a href="/" className="media">
            <img src="../static/camera-video.png" alt="microphone-icon" />
          </a>

          <a href="/" className="clear-chat subtitle is-6 is-inline-block">
            <img src="../static/delete.png" alt="delete-icon" />
          </a>
        </div>
        {this.state.mentor_email ? (
          <>
            <div className="chat-window has-text-centered is-inline-block">
              <p className="date subtitle">Today</p>
              {this.state.messages.map(
                function(item) {
                  return item.email !=
                    sessionStorage.getItem("mentor_email") ? (
                    <div className="message-bundle is-inline-block me">
                      <div className="user-avatarr">
                        <img
                          src={
                            "../static/uploads/users/" + item.user_avatar_url
                          }
                          alt="teacher-photo"
                        />
                      </div>
                      <div className="message-body">
                        <div className="info">
                          <span className="time">
                            <Moment format="HH:SS">{item.timestamp}</Moment>
                          </span>
                        </div>
                        <p className="message">{item.message}</p>
                      </div>
                    </div>
                  ) : (
                    <div className="message-bundle is-inline-block">
                      <div className="user-avatarr">
                        <img
                          src={
                            "../static/uploads/users/" + item.user_avatar_url
                          }
                          alt="teacher-photo"
                        />
                      </div>
                      <div className="message-body">
                        <div className="info">
                          <span className="receiver-name">{item.username}</span>
                          <span className="time">
                            <Moment format="HH:SS">{item.timestamp}</Moment>
                          </span>
                        </div>
                        <p className="message">{item.message}</p>
                      </div>
                    </div>
                  );
                }.bind(this)
              )}
            </div>
            <div className="level chat-footer">
              <div className="input-wrap">
                <textarea
                  onKeyPress={this.CreateChatRoom}
                  col="1"
                  rows="1"
                  placeholder="Your Message"
                  ref={this.message}
                />
              </div>
              <div className="btn-wrap">
                <button onClick={this.onSend} className="button">
                  Send
                </button>

                {/* <button onClick={this.loadComments} className="button">
              Load messages
            </button> */}
              </div>
            </div>
          </>
        ) : (
          <div className="chat-window has-text-centered is-inline-block">
            <div className="empty-content-container">
              <img src="../static/bell.png" className="empty-logo" />

              <p className="empty-description">
                Lets send a message to your contact
              </p>
            </div>
          </div>
        )}
      </>
    );
  }
}
// export default withRouter(ChatWindow);
export default ChatWindow;
