import React, { Component } from "react";
import "./mobile-chatbox.scss";
import ChatBox from "../chatbox/chatbox";

export default class MobileChatBox extends Component {
  render() {
    return (
      <>
        <div className="container">
          <div className="chat-details-wrap">
            <div className="chat-details">
              <a className="button is-primary back-to-chatlist">
                <span>Back to chat list</span>
              </a>
              <ChatBox />
            </div>
          </div>
        </div>
      </>
    );
  }
}
