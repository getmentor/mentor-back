import React, { Component } from "react";
import "./chat-header.scss";

export default class ChatHeader extends Component {
  render() {
    return (
      <>
        <div className="chat-header">
          <span className="recipient-name title is-6 is-inline-block">
            Tony Luke
          </span>
          <a href="/" className="media">
            <img src="../static/desk-microphone.png" alt="microphone-icon" />
          </a>
          <a href="/" className="media">
            <img src="../static/camera-video.png" alt="microphone-icon" />
          </a>

          <a href="/" className="clear-chat subtitle is-6 is-inline-block">
            <img src="../static/delete.png" alt="delete-icon" />
          </a>
        </div>
      </>
    );
  }
}
