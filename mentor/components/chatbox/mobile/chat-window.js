import React, { Component } from "react";
import "./chat-window.scss";

export default class ChatWindow extends Component {
  render() {
    return (
      <>
        <div className="chat-window has-text-centered is-inline-block">
          <p className="date subtitle">Today</p>
          <div className="message-bundle is-inline-block me">
            <div className="user-avatarr">
              <img src="../static/p_003.jpg" alt="teacher-photo" />
            </div>
            <div className="message-body">
              <div className="info">
                <span className="time">14:45</span>
              </div>
              <p className="message">Hi Tony !</p>
            </div>
          </div>
          <div className="message-bundle is-inline-block">
            <div className="user-avatarr">
              <img src="../static/t_001.jpg" alt="teacher-photo" />
            </div>
            <div className="message-body">
              <div className="info">
                <span className="receiver-name">Tony</span>
                <span className="time">14:45</span>
              </div>
              <p className="message">Hello Alexa !</p>
            </div>
          </div>
          <div className="message-bundle is-inline-block me">
            <div className="user-avatarr">
              <img src="../static/p_003.jpg" alt="teacher-photo" />
            </div>
            <div className="message-body">
              <div className="info">
                <span className="time">14:45</span>
              </div>
              <p className="message">
                I wish to master MERN stack web development. Iam looking for a
                good mentor
              </p>
            </div>
          </div>
          <div className="message-bundle is-inline-block">
            <div className="user-avatarr">
              <img src="../static/t_001.jpg" alt="teacher-photo" />
            </div>
            <div className="message-body">
              <div className="info">
                <span className="receiver-name">Tony</span>
                <span className="time">14:45</span>
              </div>
              <p className="message">
                Stay cool Alexa, I can provide an in depth understanding of MERN
                stack and its industry best coding standards
              </p>
            </div>
          </div>
          <div className="message-bundle is-inline-block me">
            <div className="user-avatarr">
              <img src="../static/p_003.jpg" alt="teacher-photo" />
            </div>
            <div className="message-body">
              <p className="message">Sounds great ! Thank you</p>
              <div className="info">
                <span className="time">14:45</span>
              </div>
              <div className="status">
                <img src="../static/t_001.jpg" alt="receiver-profile-photo" />
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
