import React, { Component } from "react";
import "./chatbox.scss";
import ChatHeader from "../mobile/chat-header";
import ChatWindow from "../mobile/chat-window";
import ChatFooter from "../mobile/chat-footer";

export default class ChatBox extends Component {
  render() {
    return (
      <>
        <div className="container">
          <div className="chat-container">
            <ChatHeader />
            <ChatWindow />
            <ChatFooter />
          </div>
        </div>
      </>
    );
  }
}
