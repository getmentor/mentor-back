import React, { Component } from "react";
import "./mobile-chatbox.scss";
import ChatBox from "../mobile/chatbox";

export default class MobileChatBox extends Component {
  render() {
    return (
      <>
        <div className="container layout-container">
          <div className="chat-details-wrap is-inline-block">
            <div className="chat-details is-inline-block">
              <a className="button is-primary back-to-chatlist">
                <span>Back to chat list</span>
              </a>
              <ChatBox />
            </div>
          </div>
        </div>
      </>
    );
  }
}
