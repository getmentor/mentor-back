import React from "react";

import Grid from "../layout/Grid";

const style = {
  wrapper: {
    width: "auto",
    margin: "0 auto"
  },
  button: {
    fontSize: "22px",
    fontWeight: "bold",
    marginTop: "5px",
    color: "#fff",
    padding: "10px",
    backgroundColor: "#157FB8",
    border: "none",
    borderRadius: "5px",
    cursor: "pointer"
  }
};

function Button({ children }) {
  return (
    <Grid style={style.wrapper}>
      <button style={style.button} className="confirm-email-btn">
        {children}
      </button>
    </Grid>
  );
}

export default Button;
