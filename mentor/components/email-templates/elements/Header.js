import React from "react";

import Grid from "../layout/Grid";
import Img from "./Img";

const logoSrc = "http://www.wds03.talanz.com/assets/img/logo.png";

const style = {
  header: {
    margin: "10px auto 20px auto",
    width: "auto"
  },

  img: {
    height: "60px"
  }
};

function Header() {
  return (
    <Grid style={style.header}>
      <Img style={style.img} src={logoSrc} alt="logo" />
    </Grid>
  );
}

export default Header;
