import React from "react";

import Grid from "../layout/Grid";

const style = {
  wrapper: {
    width: "auto",
    margin: "0 auto"
  },
  subtitle: {
    fontSize: "18px",
    fontWeight: "bold",
    marginTop: "5px",
    marginBottom: "10px",
    textAlign: "center"
  }
};

function Subtitle({ children }) {
  return (
    <Grid style={style.wrapper}>
      <h2 style={style.subtitle} className="title-heading">
        {children}
      </h2>
    </Grid>
  );
}

export default Subtitle;
