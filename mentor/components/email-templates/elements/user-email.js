import React from "react";

import Grid from "../layout/Grid";

const style = {
  wrapper: {
    width: "auto",
    margin: "0 auto"
  },

  useremail: {
    fontSize: "20px",
    fontWeight: "bold",
    marginTop: "5px",
    color: "green",
    marginBottom: "20px",
    textAlign: "center",
    color: "#000"
  }
};

function UserEmail({ children }) {
  return (
    <Grid style={style.wrapper}>
      <h3 style={style.useremail} className="title-heading">
        {children}
      </h3>
    </Grid>
  );
}

export default UserEmail;
