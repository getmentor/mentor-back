import React from "react";

import Grid from "../email-templates/layout/Grid";
import Header from "../email-templates/elements/Header";
import Title from "../email-templates/elements/Title";
import Subtitle from "../email-templates/elements/Subtitle";
import Body from "../email-templates/elements/Body";
import Button from "../email-templates/elements/Button";

const style = {
  container: {
    backgroundColor: "#efefef",
    padding: "20px 0",
    fontFamily: "sans-serif",
    minHeight: "550px"
  },

  main: {
    maxWidth: "90%",
    width: "100%"
  }
};

function ConfirmEmailTemplate({ data }) {
  return (
    <center style={style.container}>
      <Grid style={style.main}>
        <Header />
        <Body>
          <Title>Hi, user</Title>
          <Subtitle>Thanks for registering</Subtitle>
          <Subtitle>Please confirm your email id</Subtitle>
          <Button>Confirm</Button>
        </Body>
      </Grid>
    </center>
  );
}

export default ConfirmEmailTemplate;
