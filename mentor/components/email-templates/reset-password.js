import React from "react";

import Grid from "../email-templates/layout/Grid";
import Header from "../email-templates/elements/Header";
import Title from "../email-templates/elements/Title";
import Subtitle from "../email-templates/elements/Subtitle";
import Body from "../email-templates/elements/Body";
import Button from "../email-templates/elements/Button";
import UserEmail from "../email-templates/elements/user-email";

const style = {
  container: {
    backgroundColor: "#efefef",
    padding: "20px 0",
    fontFamily: "sans-serif",
    minHeight: "550px"
  },

  main: {
    maxWidth: "90%",
    width: "100%"
  }
};

function ResetPasswordTemplate({ data }) {
  return (
    <center style={style.container}>
      <Grid style={style.main}>
        <Header />
        <Body>
          <Title>Hi, user</Title>
          <Subtitle>You've recently asked to reset the password</Subtitle>
          <Subtitle>for this Mentor account</Subtitle>
          <Subtitle>To update your password, click the button below</Subtitle>
          <UserEmail>Email id</UserEmail>
          <Button>Confirm</Button>
        </Body>
      </Grid>
    </center>
  );
}

export default ResetPasswordTemplate;
