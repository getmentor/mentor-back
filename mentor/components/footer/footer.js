import React, { Component } from "react";
import "./footer.scss";
import counterpart from "counterpart";
import Translate from "react-translate-component";

import en from "../../lang/en";
import de from "../../lang/de";
import ar from "../../lang/ar";
counterpart.registerTranslations("en", en);
counterpart.registerTranslations("de", de);
counterpart.registerTranslations("ar", ar);

counterpart.setLocale("en");
// eslint-disable-next-line react/prefer-stateless-function
class Footer extends Component {
  state = {
    lang: "en"
  };

  onLangChange = e => {
    this.setState({ lang: e.target.value });
    counterpart.setLocale(e.target.value);
    localStorage.setItem("language", e.target.value);
  };

  componentDidMount() {
    const language = localStorage.getItem("language");
    console.log(language);
    if (language) {
      this.setState({
        lang: localStorage.getItem("language")
      });
      counterpart.setLocale(language);
    }
  }

  render() {
    return (
      <footer className="footer has-text-centered">
        <ul className="primary-list">
          <li>
            <Translate
              content="footerpara.p1"
              component="p"
              className="label is-inline-block headings"
              unsafe={true}
            />
            {/* <p>
              Copyright 2019 Mentor.<span>|</span>
            </p> */}
          </li>
          <li>
            <a href="/">
              <Translate
                content="footerspan.s1"
                component="span"
                className="label is-inline-block headings"
                unsafe={true}
              />
              {/* <span>Privacy Policy|</span> */}
            </a>
          </li>
          <li>
            <a href="/">
              <Translate
                content="footerspan.s2"
                component="span"
                className="label is-inline-block headings"
                unsafe={true}
              />
              {/* <span>Terms of Service</span> */}
            </a>
          </li>
          <li>
            <div class="field lan-switch is-inline-block">
              <div class="control">
                <div class="select">
                  <select value={this.state.lang} onChange={this.onLangChange}>
                    <option value="en">English</option>
                    <option value="de">Danish</option>
                    <option value="ar">عربى</option>
                  </select>
                </div>
              </div>
            </div>
          </li>
        </ul>
        {/* <div className="field lan-switch is-inline-block">
          <div className="control">
            <div className="select">
              <select value={this.state.lang} onChange={this.onLangChange}>
                <option value="en">English</option>
                <option value="de">Danish</option>
                <option value="ar">عربى</option>
              </select>
            </div>
          </div>
        </div> */}
      </footer>
    );
  }
}
export default Footer;
