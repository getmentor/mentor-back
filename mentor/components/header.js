import React, { Component } from "react";
import Router from "next/router";
import "./header.scss";
import Link from "next/link";
import LoggedOutUser from "../components/logged-out/logged-out";
import LoggedInuser from "../components/logged-in/logged-in";

import MobileMenu from "../components/mobile-menu/mobile-menu";
import "../components/mobile-menu/mobile-menu.scss";
import AuthContext from "../context/auth-context";

import counterpart from "counterpart";
import Translate from "react-translate-component";

export default class header extends Component {
  state = {
    token: null,
    userId: null,
    role: null
  };
  componentDidMount() {
    window.scrollTo(0, 0);
    const token = localStorage.getItem("token");
    if (token) {
      this.setState({
        token: localStorage.getItem("token")
      });
    }
    const userId = localStorage.getItem("userId");
    if (userId) {
      this.setState({
        userId: localStorage.getItem("userId")
      });
    }
    const role = localStorage.getItem("role");
    if (role) {
      this.setState({
        role: localStorage.getItem("role")
      });
    }
  }

  handleMobileMenu = () => {
    let mobileMenuElement = document.getElementById("mobile-menu-wrap");
    mobileMenuElement.classList.toggle("show-menu");
    // document.getElementById("backdrop").classList.toggle("show");
  };
  handleToggleMenuClick = () => {
    let mobileMenuElement = document.getElementById("mobile-menu-wrap");
    mobileMenuElement.classList.toggle("show-menu");
  };
  handleToggleMenuClose = () => {
    let mobileMenuElement = document.getElementById("mobile-menu-wrap");
    mobileMenuElement.classList.remove("show-menu");
  };
  render() {
    return (
      <>
        <header className="Header">
          <div className="header-inner">
            <MobileMenu />
            <div
              role="button"
              className="navbar-burger burger"
              aria-label="menu"
              aria-expanded="false"
              data-target="navbarBasicExample"
              onClick={this.handleMobileMenu}
            >
              <span aria-hidden="true" />
              <span aria-hidden="true" />
              <span aria-hidden="true" />
            </div>
            <div className="left-side">
              <nav
                className="navbar navbar-custom"
                role="navigation"
                aria-label="main navigation"
              >
                <div className="navbar-brand">
                  <div className="logo">
                    {/* <a href="/" className="logo-link"> */}
                    <Link href="/">
                      <a onClick={this.handleToggleMenuClose}>
                        <img
                          src="../static/logo.png"
                          className="logo-icon"
                          alt="logo"
                        />
                      </a>
                    </Link>

                    {/* </a> */}
                  </div>
                </div>

                <div className="navbar-menu navbar-custom">
                  <div className="navbar-start">
                    <div className="navbar-item has-dropdown is-hoverable without-dropdown">
                      {/* {this.state.token && (
                        <Link href="/about" className="navbar-link" to="/about">
                          About
                        </Link>
                      )} */}
                      {/* {this.state.token && ( */}
                      <Translate
                        content="link.about"
                        component="a"
                        unsafe={true}
                        href="/about"
                        to="/about"
                        className="navbar-link"
                      />
                      {/* )} */}
                    </div>

                    <div className="navbar-item has-dropdown is-hoverable">
                      {/* <Link className="navbar-link" to="/">
                        Search
                      </Link> */}
                      <Translate
                        content="link.header_search"
                        component="a"
                        unsafe={true}
                        className="navbar-link"
                      />

                      <div className="navbar-dropdown">
                        <Link>
                          <a className="navbar-item">Link 1</a>
                        </Link>
                        <Link>
                          <a className="navbar-item">Link 2</a>
                        </Link>
                        <Link>
                          <a className="navbar-item">Link 3</a>
                        </Link>
                      </div>
                    </div>

                    <div className="navbar-item has-dropdown is-hoverable without-dropdown">
                      {/* <Link
                        href="/contact-us"
                        className="navbar-link"
                        to="/contact-us"
                      >
                        Contact us
                      </Link> */}

                      <Translate
                        content="link.header_contact"
                        component="a"
                        unsafe={true}
                        to="/contact-us"
                        className="navbar-link"
                      />
                    </div>
                  </div>
                </div>
              </nav>
            </div>
            <div className="right-side">
              <LoggedOutUser />
            </div>
          </div>
        </header>
      </>
    );
  }
}
