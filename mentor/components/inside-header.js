import React, { Component } from "react";
import "./inside-header.scss";
import Link from "next/link";
import MobileMenu from "../components/mobile-menu/mobile-menu";
import "../components/mobile-menu/mobile-menu.scss";
import env from "../constant.json";
import Router, { withRouter } from "next/router";

export default class InsideHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userIsOnline: true,
      userIsOffline: false,
      token: null,
      userId: null,
      role: null,
      active_username: null,
      active_userimage: null,
      active_useremail: null,
      active_usershort_desc: null
      // menuClicked: false
    };
  }

  componentDidMount() {
    window.scrollTo(0, 0);

    const token = localStorage.getItem("token");
    if (token) {
      this.setState({
        token: localStorage.getItem("token")
      });
    }
    const userId = localStorage.getItem("userId");
    if (userId) {
      this.setState({
        userId: localStorage.getItem("userId")
      });
    }
    const role = localStorage.getItem("role");
    if (role) {
      this.setState({
        role: localStorage.getItem("role")
      });
    }

    this.fetchUser();
  }

  logout() {
    localStorage.clear();
    sessionStorage.clear();
    Router.push("/login");
  }

  async fetchUser() {
    const userId = localStorage.getItem("userId");

    let requestBody = {
      query: `
        query userDetails($userId: String!) {
          userDetails(userId: $userId) {
            userId    
            name   
            last_name     
            email
            role
            main_desc
            short_desc
            profile_pic
            resume_link
            linkedin_url
            twitte_url
            medium_url
          }
        }
      `,
      variables: {
        userId: userId
      }
    };

    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(async resData => {
        if (resData.data.userDetails) {
          this.setState({
            active_username: resData.data.userDetails.name
          });
          this.setState({
            active_userimage: resData.data.userDetails.profile_pic
          });
          this.setState({ active_useremail: resData.data.userDetails.email });
          this.setState({
            active_usershort_desc: resData.data.userDetails.short_desc
          });
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  handleMobileMenu = () => {
    let mobileMenuElement = document.getElementById("mobile-menu-wrap");
    mobileMenuElement.classList.toggle("show-menu");
    document.getElementById("dropdown-menu5").classList.remove("is-active");
    document.getElementById("dropdown-menu6").classList.remove("is-active");
  };
  handleToggleMenuClick = () => {
    let mobileMenuElement = document.getElementById("mobile-menu-wrap");
    mobileMenuElement.classList.toggle("show-menu");
  };
  handleToggleMenuClose = () => {
    let mobileMenuElement = document.getElementById("mobile-menu-wrap");
    mobileMenuElement.classList.remove("show-menu");
  };
  toggleStatus = currentState => {
    if (currentState === "userIsOffline")
      this.setState({ userIsOnline: false, userIsOffline: true });
    else this.setState({ userIsOnline: true, userIsOffline: false });
  };
  handleDropdown = () => {
    document.getElementById("dropdown-menu6").classList.toggle("is-active");
    document.getElementById("dropdown-menu5").classList.remove("is-active");
    document.getElementById("mobile-menu-wrap").classList.remove("show-menu");
  };
  handleNotifications = () => {
    document.getElementById("dropdown-menu5").classList.toggle("is-active");
    document.getElementById("dropdown-menu6").classList.remove("is-active");
    document.getElementById("mobile-menu-wrap").classList.remove("show-menu");
  };

  render() {
    return (
      <>
        <header className="Header">
          <div className="header-inner">
            <MobileMenu />
            <div
              role="button"
              className="navbar-burger burger"
              aria-label="menu"
              aria-expanded="false"
              data-target="navbarBasicExample"
              onClick={this.handleMobileMenu}
            >
              <span aria-hidden="true" />
              <span aria-hidden="true" />
              <span aria-hidden="true" />
            </div>
            <div className="left-side">
              <nav
                className="navbar navbar-custom"
                role="navigation"
                aria-label="main navigation"
              >
                <div className="navbar-brand">
                  <div className="logo">
                    {/* <a href="/" className="logo-link"> */}
                    <Link href="/">
                      <a onClick={this.handleToggleMenuClose}>
                        <img
                          src="../static/logo.png"
                          className="logo-icon"
                          alt="logo"
                        />
                      </a>
                    </Link>

                    {/* </a> */}
                  </div>
                </div>

                <div className="navbar-menu navbar-custom">
                  <div className="navbar-start">
                    <div className="navbar-item has-dropdown is-hoverable without-dropdown">
                      <Link href="/about">
                        <a className="navbar-link">About</a>
                      </Link>
                    </div>

                    <div className="navbar-item has-dropdown is-hoverable">
                      <Link href="/">
                        <a className="navbar-link">Search</a>
                      </Link>

                      <div className="navbar-dropdown">
                        <Link href="/">
                          <a className="navbar-item"> Link 1</a>
                        </Link>
                        <Link href="/">
                          <a className="navbar-item"> Link 2</a>
                        </Link>
                        <Link href="/">
                          <a className="navbar-item"> Link 3</a>
                        </Link>
                      </div>
                    </div>

                    <div className="navbar-item has-dropdown is-hoverable without-dropdown">
                      <Link href="/contact-us">
                        <a className="navbar-link">Contact us</a>
                      </Link>
                    </div>
                  </div>
                </div>
              </nav>
            </div>
            <div className="right-side">
              <div className="header-widget">
                <div class="dropdown">
                  <div className="dropdown-trigger">
                    <div className="header-notifications header-notifications-ico">
                      <div className="header-notifications-trigger">
                        <a onClick={this.handleNotifications}>
                          <img src="../static/bell.png" alt="bell-icon" />
                          <span>4</span>
                        </a>
                      </div>
                    </div>
                  </div>
                  <div
                    className="dropdown-menu notifications-menu"
                    id="dropdown-menu5"
                    role="menu"
                  >
                    <div className="dropdown-content">
                      <a className="dropdown-item">
                        <ul className="notifications-list is-inline-block">
                          <li>
                            <div className="header-notifications-trigger">
                              <div className="user-avatar">
                                <img src="../static/t_001.jpg" alt="user" />
                              </div>
                            </div>
                          </li>
                          <li>
                            <p className="title is-6">Tony Luke</p>
                            <p className="notification-msg">Heyy You,</p>
                            <p className="notification-msg">
                              ready to get started.
                            </p>
                            <p className="time">5 hours ago</p>
                          </li>
                        </ul>
                      </a>
                      <a className="dropdown-item">
                        <ul className="notifications-list is-inline-block">
                          <li>
                            <div className="header-notifications-trigger">
                              <div className="user-avatar">
                                <img src="../static/t_003.jpg" alt="user" />
                              </div>
                            </div>
                          </li>
                          <li>
                            <p className="title is-6">Ayesha</p>
                            <p className="notification-msg">Thank you Anton</p>
                            <p className="time">8 hours ago</p>
                          </li>
                        </ul>
                      </a>
                      <a className="dropdown-item">
                        <ul className="notifications-list is-inline-block">
                          <li>
                            <div className="header-notifications-trigger">
                              <div className="user-avatar">
                                <img src="../static/t_002.jpg" alt="user" />
                              </div>
                            </div>
                          </li>
                          <li>
                            <p className="title is-6">Jonathan</p>
                            <p className="notification-msg">
                              Thanks for the feedback.
                            </p>
                            <p className="notification-msg">
                              Happy weekend, Anton
                            </p>

                            <p className="time">9 hours ago</p>
                          </li>
                        </ul>
                      </a>
                      <a className="button is-link view-all-notifications-btn">
                        View all notifications
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div className="header-widget header-widget-end">
                <div className="header-notifications header-notifications-end">
                  <div className="dropdown is-right" id="drop-head">
                    <div className="dropdown-trigger">
                      <div className="header-notifications-trigger">
                        <div
                          className={
                            this.state.userIsOnline
                              ? "user-avatar status-online"
                              : "user-avatar"
                          }
                          onClick={this.handleDropdown}
                        >
                          <img
                            src={
                              "../static/uploads/users/" +
                              this.state.active_userimage
                            }
                            alt="user"
                          />
                        </div>
                      </div>
                    </div>
                    <div
                      className="dropdown-menu user-menu"
                      id="dropdown-menu6"
                      role="menu"
                    >
                      <div className="dropdown-content">
                        <div className="dropdown-item">
                          <ul className="main-listing is-block">
                            <li className="is-inline-block">
                              <ul className="sub-listing">
                                <li>
                                  <div className="header-notifications-trigger is-inline-block">
                                    <div
                                      className={
                                        this.state.userIsOnline
                                          ? "user-avatar status-online"
                                          : "user-avatar"
                                      }
                                    >
                                      <img
                                        src={
                                          "../static/uploads/users/" +
                                          this.state.active_userimage
                                        }
                                        alt="user"
                                      />
                                    </div>
                                  </div>
                                </li>
                                <li>
                                  <p className="title is-5">
                                    {this.state.active_username}
                                  </p>
                                  <p className="subtitle is-6">
                                    {this.state.active_usershort_desc}
                                  </p>
                                </li>
                              </ul>
                            </li>
                            <li className="is-block">
                              <div className="status-switcher is-inline-block">
                                <div className="part is-inline-block">
                                  <label
                                    className={
                                      this.state.userIsOnline
                                        ? "radio online is-active"
                                        : "radio online"
                                    }
                                  >
                                    <input
                                      type="radio"
                                      name="status"
                                      onClick={this.toggleStatus.bind(
                                        this,
                                        "userIsOnline"
                                      )}
                                    />
                                    <div className="checkmark" />
                                    Online
                                  </label>
                                  <label
                                    className={
                                      this.state.userIsOffline
                                        ? "radio offline is-active"
                                        : "radio offline"
                                    }
                                  >
                                    <input
                                      type="radio"
                                      name="status"
                                      onClick={this.toggleStatus.bind(
                                        this,
                                        "userIsOffline"
                                      )}
                                    />
                                    Offline
                                  </label>
                                </div>
                              </div>
                            </li>
                          </ul>
                          <ul className="more-listings">
                            <li>
                              <Link>
                                <a>Dashboard</a>
                              </Link>
                            </li>
                            <li>
                              <Link>
                                <a onClick={this.logout}>Logout</a>
                              </Link>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </header>
      </>
    );
  }
}
