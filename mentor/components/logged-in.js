import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./logged-in.scss";
class LoggedInuser extends Component {
  render() {
    return (
      <>
        <div className="header-widget">
          <div className="header-notifications header-notifications-ico">
            <div className="header-notifications-trigger">
              <a href="/">
                <img src="/assets/img/bell.png" alt="bell-icon" />
                <span>4</span>
              </a>
            </div>
          </div>
        </div>
        <div className="header-widget header-widget-end">
          <div className="header-notifications header-notifications-end">
            <div className="dropdown is-right" id="drop-head">
              <div className="dropdown-trigger">
                <div className="header-notifications-trigger">
                  <div
                    className={
                      this.state.userIsOnline
                        ? "user-avatar status-online"
                        : "user-avatar"
                    }
                    onClick={this.handleDropdown}
                  >
                    <img src="../static/p_002.jpg" alt="user" />
                  </div>
                </div>
              </div>
              <div
                className="dropdown-menu user-menu"
                id="dropdown-menu6"
                role="menu"
              >
                <div className="dropdown-content">
                  <div className="dropdown-item">
                    <ul className="main-listing is-block">
                      <li className="is-inline-block">
                        <ul className="sub-listing">
                          <li>
                            <div className="header-notifications-trigger is-inline-block">
                              <div
                                className={
                                  this.state.userIsOnline
                                    ? "user-avatar status-online"
                                    : "user-avatar"
                                }
                              >
                                <img src="../static/p_002.jpg" alt="user" />
                              </div>
                            </div>
                          </li>
                          <li>
                            <p className="title is-5">Antonio</p>
                            <p className="subtitle is-6">
                              Nuclear physics student
                            </p>
                          </li>
                        </ul>
                      </li>
                      <li className="is-block">
                        <div className="status-switcher is-inline-block">
                          <div className="part is-inline-block">
                            <label
                              className={
                                this.state.userIsOnline
                                  ? "radio online is-active"
                                  : "radio online"
                              }
                            >
                              <input
                                type="radio"
                                name="status"
                                onClick={this.toggleStatus.bind(
                                  this,
                                  "userIsOnline"
                                )}
                              />
                              <div className="checkmark" />
                              Online
                            </label>
                            <label
                              className={
                                this.state.userIsOffline
                                  ? "radio offline is-active"
                                  : "radio offline"
                              }
                            >
                              <input
                                type="radio"
                                name="status"
                                onClick={this.toggleStatus.bind(
                                  this,
                                  "userIsOffline"
                                )}
                              />
                              Offline
                            </label>
                          </div>
                        </div>
                      </li>
                    </ul>
                    <ul className="more-listings">
                      <li>
                        <Link>
                          <a>Dashboard</a>
                        </Link>
                      </li>
                      <li>
                        <Link>
                          <a>Logout</a>
                        </Link>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
export default LoggedInuser;
