import React, { Component } from "react";
import Link from "next/link";
import "./logged-in.scss";
class LoggedInuser extends Component {
  render() {
    return (
      <>
        <div className="header-widget">
          <div className="header-notifications header-notifications-ico">
            <div className="header-notifications-trigger">
              <a href="/">
                <img src="./static/bell.png" alt="bell-icon" />
                <span>4</span>
              </a>
            </div>
          </div>
        </div>
        <div className="header-widget header-widget-end">
          <div className="header-notifications header-notifications-end">
            <div
              className="dropdown is-right"
              onClick={this.handleDropdown}
              id="drop-head"
            >
              <div className="dropdown-trigger">
                <div className="header-notifications-trigger">
                  <div className="user-avatar status-online">
                    <img src="./static/user.png" alt="user" />
                  </div>
                </div>
              </div>
              <div className="dropdown-menu" id="dropdown-menu6" role="menu">
                <div className="dropdown-content">
                  <div className="dropdown-item">
                    <Link href="/">
                      <a>Log Out</a>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
export default LoggedInuser;
