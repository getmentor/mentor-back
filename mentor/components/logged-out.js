import React, { Component } from "react";

class LoggedOutuser extends Component {
  handleLoginModal = () => {
    document
      .getElementById("login-signup-modal-wrap")
      .classList.toggle("modal-open");
    document.body.classList.toggle("modal-open");
  };
  render() {
    return (
      <>
        <div className="login-register-wrap">
          <Link href="/login">
            <a className="login-register-content">
              {" "}
              <img
                src="/assets/img/login-icon.png"
                alt="login-icon"
                className="login-icon"
              />
              <span className="login-register-btn">Log In / Sign up</span>
            </a>
          </Link>
        </div>
      </>
    );
  }
}
export default LoggedOutuser;
