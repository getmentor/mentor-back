import React, { Component } from "react";
import Router, { withRouter } from "next/router";
import Link from "next/link";
// import ForgotPassword from "../login/forgot-password/forgot-pasword";
import "./login-page.scss";
import AuthContext from "../../context/auth-context";
import env from "../../constant.json";

import counterpart from "counterpart";
import Translate from "react-translate-component";

import QiscusSDKCore from "qiscus-sdk-core";
const qiscus = new QiscusSDKCore();

class LoginPage extends Component {
  static contextType = AuthContext;

  constructor(props) {
    super(props);

    this.state = {
      activeTab1: true,
      activeTab2: false,
      activeTabStudent: true,
      activeTabTeacher: false,
      modalOpen: false,
      passwordSentOpen: false,
      emailerr: false,
      passerr: false,
      login_err: false,
      forgot_pass: false,
      name_err: false,
      email_err: false,
      new_pass_err: false,
      confirm_pass_err: false,
      signup_err: ""
    };
    this.emailEl = React.createRef();
    this.passwordEl = React.createRef();

    this.emailRecoveryEl = React.createRef();

    this.sName = React.createRef();
    this.sEmail = React.createRef();
    this.sPass = React.createRef();
    this.confirmPass = React.createRef();
  }

  componentDidMount() {
    const token = localStorage.getItem("token");
    if (token) {
      this.setState({
        token: localStorage.getItem("token"),
        isLoading: false
      });
    }
    const userId = localStorage.getItem("userId");
    if (userId) {
      this.setState({
        userId: localStorage.getItem("userId"),
        isLoading: false
      });
    }
    const role = localStorage.getItem("role");
    if (role) {
      this.setState({
        role: localStorage.getItem("role")
      });
    }
    if (token) {
      Router.push("/about");
    }
    this.Initialization();
  }
  toggleClass(tabSelected) {
    if (tabSelected === "loginTab")
      this.setState({ activeTab1: true, activeTab2: false });
    else this.setState({ activeTab1: false, activeTab2: true });
  }

  toggleModalClass(modalState) {
    if (modalState === "modalOpen") this.setState({ modalOpen: true });
    else this.setState({ modalOpen: false });
  }

  onChangeEmail = event => {
    const email = this.emailEl.current.value;
    let lastAtPos = email.lastIndexOf("@");
    let lastDotPos = email.lastIndexOf(".");

    if (email.trim().length === 0) {
      this.setState({ emailerr: true });
      return;
    } else {
      if (
        !(
          lastAtPos < lastDotPos &&
          lastAtPos > 0 &&
          email.indexOf("@@") == -1 &&
          lastDotPos > 2 &&
          email.length - lastDotPos > 2
        )
      ) {
        this.setState({ emailerr: true });
      } else {
        this.setState({ emailerr: false });
      }

      return;
    }
  };
  onChangePassword = event => {
    const password = this.passwordEl.current.value;

    if (password.trim().length === 0) {
      this.setState({ passerr: true });
      return;
    } else {
      this.setState({ passerr: false });
      return;
    }
  };

  onChangeName = event => {
    const name = this.sName.current.value;
    if (name.trim().length === 0) {
      this.setState({ name_err: true });
      return;
    } else {
      this.setState({ name_err: false });
      return;
    }
  };

  onChangeSEmail = event => {
    const email = this.sEmail.current.value;

    let lastAtPos = email.lastIndexOf("@");
    let lastDotPos = email.lastIndexOf(".");
    if (email.trim().length === 0) {
      this.setState({ email_err: true });
      return;
    } else {
      if (
        !(
          lastAtPos < lastDotPos &&
          lastAtPos > 0 &&
          email.indexOf("@@") == -1 &&
          lastDotPos > 2 &&
          email.length - lastDotPos > 2
        )
      ) {
        this.setState({ email_err: true });
      } else {
        this.setState({ email_err: false });
      }

      return;
    }
  };

  onChangeSPassword = event => {
    const pass = this.sPass.current.value;
    const confirmpass = this.confirmPass.current.value;
    if (pass.trim().length === 0) {
      this.setState({ new_pass_err: true });
      return;
    } else {
      if (confirmpass != pass) {
        this.setState({ confirm_pass_err: true });
      } else {
        this.setState({ confirm_pass_err: false });
      }
      this.setState({ new_pass_err: false });
      return;
    }
  };
  onChangeConfirmPass = event => {
    const pass = this.sPass.current.value;
    const confirmpass = this.confirmPass.current.value;
    if (confirmpass.trim().length === 0) {
      this.setState({ confirm_pass_err: true });
      return;
    } else {
      if (confirmpass != pass) {
        this.setState({ confirm_pass_err: true });
      } else {
        this.setState({ confirm_pass_err: false });
      }
      return;
    }
  };

  onChangeResetEmail = event => {
    const email_new = this.emailRecoveryEl.current.value;

    let lastAtPos = email_new.lastIndexOf("@");
    let lastDotPos = email_new.lastIndexOf(".");

    if (email_new.trim().length === 0) {
      this.setState({ forgot_pass: true });
      return;
    } else {
      if (
        !(
          lastAtPos < lastDotPos &&
          lastAtPos > 0 &&
          email_new.indexOf("@@") == -1 &&
          lastDotPos > 2 &&
          email_new.length - lastDotPos > 2
        )
      ) {
        this.setState({ forgot_pass: true });
      } else {
        this.setState({ forgot_pass: false });
      }

      return;
    }
  };

  forgotPassword = event => {
    event.preventDefault();
    const email_new = this.emailRecoveryEl.current.value;
    let lastAtPos = email_new.lastIndexOf("@");
    let lastDotPos = email_new.lastIndexOf(".");

    if (email_new.trim().length === 0) {
      this.setState({ forgot_pass: true });
      return;
    } else {
      if (
        !(
          lastAtPos < lastDotPos &&
          lastAtPos > 0 &&
          email_new.indexOf("@@") == -1 &&
          lastDotPos > 2 &&
          email_new.length - lastDotPos > 2
        )
      ) {
        this.setState({ forgot_pass: true });
        return;
      }
    }

    fetch(env.sendmail, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        email: email_new
      })
    })
      .then(response => {
        Router.push("/reset-email-sent");
        return response.json();
      })
      .then(json => {
        // doSomeThing(json.someProperty)
        // this.setState({jsonData: json});
      })
      .catch(err => {
        console.log("Error : " + err);
      });
  };

  submitSignup = event => {
    event.preventDefault();

    const name = this.sName.current.value;
    const last_name = "";
    const email = this.sEmail.current.value;
    const pass = this.sPass.current.value;
    const confirmpass = this.confirmPass.current.value;
    const role = 1;

    const skills = "";
    const resume_link = "";
    const profile_pic = "no_user.png";
    const short_desc = "";
    const main_desc = "";
    const language = "";
    const location = "";

    // const expertise = "";
    // const toolkit = "";
    // const experience = "";
    // const education = "";
    // const certificates = "";
    // const social_media_links = "";

    const linkedin_url = "";
    const twitte_url = "";
    const medium_url = "";

    if (name.trim().length === 0) {
      this.setState({ name_err: true });
      return;
    }
    if (email.trim().length === 0) {
      this.setState({ email_err: true });
      return;
    } else {
      let lastAtPos = email.lastIndexOf("@");
      let lastDotPos = email.lastIndexOf(".");

      if (
        !(
          lastAtPos < lastDotPos &&
          lastAtPos > 0 &&
          email.indexOf("@@") == -1 &&
          lastDotPos > 2 &&
          email.length - lastDotPos > 2
        )
      ) {
        this.setState({ email_err: true });
        return;
      }
    }
    if (pass.trim().length === 0) {
      this.setState({ new_pass_err: true });
      return;
    } else {
      if (confirmpass != pass) {
        this.setState({ confirm_pass_err: true });
        return;
      }
    }
    if (confirmpass.trim().length === 0) {
      this.setState({ confirm_pass_err: true });
      return;
    } else {
      if (confirmpass != pass) {
        this.setState({ confirm_pass_err: true });
        return;
      }
    }
    let requestBody = {
      query: `
        mutation CreateUser($name: String!,$last_name:String!,$email: String!, $password: String!, $skills: String!, $resume_link: String!, $role: Int!,$profile_pic:String!,$short_desc:String!,$main_desc:String!,$language:String!,$location:String!,$linkedin_url:String!,$twitte_url:String!,$medium_url:String!) {
          createUser(userInput: {name: $name,last_name:$last_name,email: $email, password: $password, skills:$skills, resume_link:$resume_link, role: $role , profile_pic: $profile_pic, short_desc: $short_desc, main_desc: $main_desc, language: $language, location: $location,linkedin_url: $linkedin_url,twitte_url:$twitte_url,medium_url:$medium_url}) {
            _id
            email
          }
        }
      `,
      variables: {
        name: name,
        last_name: last_name,
        email: email,
        password: pass,
        skills: skills,
        profile_pic: profile_pic,
        short_desc: short_desc,
        main_desc: main_desc,
        language: language,
        location: location,
        // expertise: expertise,
        // toolkit: toolkit,
        // experience: experience,
        // education: education,
        // certificates: certificates,
        // social_media_links: social_media_links,
        linkedin_url: linkedin_url,
        twitte_url: twitte_url,
        medium_url: medium_url,
        resume_link: resume_link,
        role: role
      }
    };

    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        response.json().then(result => {
          console.log(result);

          if (result.data.createUser !== null) {
            qiscus
              .setUser(email, email, name, profile_pic, {})
              .then(response => {
                this.CreateChatRoom();
              })
              .catch(function(error) {});
            this.setState({
              signup_err: ""
            });

            Router.push("/registration-complete");
          } else {
            result.errors.forEach(message => {
              console.log(message.message);
              this.setState({
                signup_err: " Signup Failed!" + message.message
              });
            });
          }
        });
      })
      .catch(err => {
        console.log("Error : " + err);
        this.setState({
          signup_err: " Signup Failed!"
        });
      });

    /*      fetch(env.GRAPHQL, {
        method: "POST",
        body: JSON.stringify(requestBody),
        headers: {
          "Content-Type": "application/json"
        }
      })
        .then(res => {
          if (res.status !== 200 && res.status !== 201) {
            this.setState({ signup_err: true });
  
            throw new Error("Failed!");
          }
          return res.json();
        })
        .then(resData => {
          if (resData.data.createUser._id) {
            console.log(resData);
  
            qiscus
              .setUser(email, email, name, profile_pic, {})
              .then(response => {
                this.CreateChatRoom();
              })
              .catch(function(error) {});
  
            Router.push("/registration-complete");
          } else {
            console.log(resData.data.errors.message);
            this.setState({ signup_err: true });
          }
        })
        .catch(err => {
          console.log(err);
        }); */
  };

  submitHandler = event => {
    event.preventDefault();
    const email = this.emailEl.current.value;
    const password = this.passwordEl.current.value;

    let lastAtPos = email.lastIndexOf("@");
    let lastDotPos = email.lastIndexOf(".");

    if (email.trim().length === 0) {
      this.setState({ emailerr: true });
      return;
    } else {
      if (
        !(
          lastAtPos < lastDotPos &&
          lastAtPos > 0 &&
          email.indexOf("@@") == -1 &&
          lastDotPos > 2 &&
          email.length - lastDotPos > 2
        )
      ) {
        this.setState({ emailerr: true });
        return;
      }
    }
    if (password.trim().length === 0) {
      this.setState({ passerr: true });
      return;
    }

    let requestBody = {
      query: `
        query Login($email: String!, $password: String!) {
          login(email: $email, password: $password) {
            userId
            token
            tokenExpiration
            role
            email
          }
        }
      `,
      variables: {
        email: email,
        password: password
      }
    };

    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          this.setState({ login_err: true });
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(resData => {
        if (resData.data.login.token) {
          console.log(resData);
          if (resData.data.login.token) {
            this.context.login(
              resData.data.login.token,
              resData.data.login.userId,
              resData.data.login.tokenExpiration,
              resData.data.login.role
            );
            if (resData.data.login.role == 1) {
              Router.push("/search");
            } else {
              Router.push("/about");
            }

            this.setState({ isLogin: true });
            this.setState({ login_err: false });
            localStorage.setItem("token", resData.data.login.token);
            localStorage.setItem("userId", resData.data.login.userId);
            localStorage.setItem("role", resData.data.login.role);
          }
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  //  Initialization Qiscus Chat SDK
  Initialization = event => {
    qiscus.init({
      AppId: "mentor-r6ledbypleisco", // "mentorcha-ehkoh6xenpl"
      options: {
        commentDeletedCallback: function(data) {},
        commentDeliveredCallback: function(data) {},
        commentReadCallback: function(data) {},
        presenceCallback: function(data) {},
        typingCallback: function(data) {},
        onReconnectCallback: function(data) {},
        newMessagesCallback: function(messages) {},
        roomClearedCallback: function(data) {}
      }
    });
    console.log(qiscus);
  };

  render() {
    return (
      <div>
        <div className="container layout-container">
          {/* <ForgotPassword modalOpen={false} /> */}
          <div
            className={
              this.state.modalOpen
                ? "modal forgot-password-modal show"
                : "modal forgot-password-modal"
            }
          >
            <div
              className="modal-background"
              onClick={this.toggleModalClass.bind(this, "modalClose")}
            />
            <div className="modal-content">
              <div className="password-recovery-container has-text-centered">
                <h1>Password recovery</h1>
                <div className="inputWrap">
                  <input
                    classsName="input"
                    type="text"
                    placeholder="Enter your email"
                    ref={this.emailRecoveryEl}
                    onKeyUp={this.onChangeResetEmail}
                  />

                  <span
                    className={
                      this.state.forgot_pass
                        ? "forgot_pass show"
                        : "forgot_pass"
                    }
                  >
                    Please enter a valid email address
                  </span>
                </div>

                {/* <Link>
                  <span className="button submit-btn">
                    Submit{" "}
                    <i className="icon-material-outline-arrow-right-alt" />
                  </span>
                </Link> */}

                <button
                  className="button submit-btn"
                  type="submit"
                  onClick={this.forgotPassword}
                >
                  Submit
                </button>
              </div>
            </div>
            <button
              className="modal-close is-large"
              aria-label="close"
              onClick={this.toggleModalClass.bind(this, "modalClose")}
            />
          </div>

          {/* <div
          className={
            this.state.passwordSentOpen
              ? "modal forgot-password-modal show"
              : "modal forgot-password-modal"
          }
        >
          <div
            className="modal-background"
            onClick={this.toggleModalClass.bind(this, "modalClose")}
          />
          <div className="modal-content">
            <div className="password-recovery-container has-text-centered">
              <h1>Reset Email Sent</h1>
            </div>
          </div>
          <button
            className="modal-close is-large"
            aria-label="close"
            onClick={this.toggleModalClass.bind(this, "modalClose")}
          />
        </div> */}
          <div className="login-signup-wrap" id="login-signup-wrap">
            <div className="tabs is-boxed">
              <ul className="mainTab">
                <li
                  className={
                    this.state.activeTab1 ? "tab-link is-active" : "tab-link"
                  }
                  id="logInTrig"
                  onClick={this.toggleClass.bind(this, "loginTab")}
                >
                  {/* <span>Log In</span> */}
                  <Translate content="span.s1" component="span" unsafe={true} />
                </li>
                <li
                  className={
                    this.state.activeTab2 ? "tab-link is-active" : "tab-link"
                  }
                  id="signUpTrig"
                  onClick={this.toggleClass.bind(this, "signupTab")}
                >
                  {/* <span>Sign up</span> */}

                  <Translate content="span.s2" component="span" unsafe={true} />
                </li>
              </ul>
            </div>

            <div className="container">
              <div
                id="loginPane"
                className={
                  this.state.activeTab1
                    ? "tab-content active-content"
                    : "tab-content"
                }
              >
                {/* <p className="welcome-text">We're glad to see you again!</p> */}
                <Translate
                  content="p.p1"
                  component="p"
                  unsafe={true}
                  className="welcome-text"
                />

                {/* <p className="welcome-text-sub">
                  Don't have an account?
                  <span onClick={this.toggleClass.bind(this, "signupTab")}>
                    Sign up
                  </span>
                </p> */}
                <Translate
                  content="p.p2"
                  component="p"
                  unsafe={true}
                  className="welcome-text-sub"
                  onClick={this.toggleClass.bind(this, "signupTab")}
                />

                {/* Login Form : Start */}
                <Translate
                  content="span.login_err"
                  component="span"
                  unsafe={true}
                  className={
                    this.state.login_err ? "login_error show" : "login_error"
                  }
                />
                <form className="auth-form" onSubmit={this.submitHandler}>
                  <div className="inputWrap">
                    <input
                      className="input"
                      type="text"
                      placeholder="Email Address"
                      ref={this.emailEl}
                      onKeyUp={this.onChangeEmail}
                    />
                    <Translate
                      content="span.s3"
                      component="span"
                      unsafe={true}
                      className={this.state.emailerr ? "error show" : "error"}
                    />

                    {/* <Translate
                      component="input"
                      type="text"
                      attributes={{ placeholder: "placeholder" }}
                      ref={this.emailEl}
                      className="input"
                    /> */}

                    {/* <span className="error">
                      Please enter a valid email address
                    </span> */}
                  </div>

                  <div className="inputWrap">
                    <input
                      className="input"
                      type="password"
                      placeholder="Password"
                      ref={this.passwordEl}
                      onKeyUp={this.onChangePassword}
                    />
                    <Translate
                      content="span.s4"
                      component="span"
                      unsafe={true}
                      className={this.state.passerr ? "error show" : "error"}
                    />

                    {/* <Translate
                      component="input"
                      type="password"
                      attributes={{ placeholder: "placeholderpass" }}
                      ref={this.passwordEl}
                      className="input"
                    /> */}
                    {/* <span className="error">Please enter your password</span> */}
                  </div>
                  <div className="forgot-password">
                    <Link>
                      <a
                        onClick={this.toggleModalClass.bind(this, "modalOpen")}
                      >
                        <Translate
                          content="span.s5"
                          component="span"
                          unsafe={true}
                        />
                        {/* Forgot Password? */}
                      </a>
                    </Link>
                  </div>

                  <div>
                    <button
                      className="button login-btn"
                      type="submit"
                      form="auth-form"
                      onClick={this.submitHandler}
                    >
                      <Translate
                        content="span.s6"
                        component="span"
                        unsafe={true}
                      />
                      {/* Log In */}
                    </button>
                  </div>
                </form>
                {/* Login Form : End */}
              </div>
              <div
                id="signupPane"
                className={
                  this.state.activeTab2
                    ? "tab-content active-content"
                    : "tab-content"
                }
              >
                {/* <p className="welcome-text">Let's create a student account!</p> */}
                <Translate
                  content="p.student_account"
                  component="p"
                  unsafe={true}
                  className="welcome-text"
                />
                <div className="teacherSignupLink">
                  {/* <Link href="teacher-registration">Apply as a teacher</Link> */}
                  <Translate
                    content="link.apply_teacher"
                    component="a"
                    unsafe={true}
                    href="teacher-registration"
                  />
                </div>

                <div className="student-teacher-tab-wrap">
                  <div className="container">
                    <span id="signupError" className="signup_error show">
                      {this.state.signup_err}
                    </span>
                    <form className="signup-form" onSubmit={this.submitSignup}>
                      <div className="tab-content active-content">
                        <div className="inputWrap">
                          {/* <Translate
                          component="input"
                          type="text"
                          attributes={{ placeholder: "placeholder_name" }}
                          className="input"
                        /> */}

                          <input
                            className="input"
                            type="text"
                            placeholder="Name"
                            ref={this.sName}
                            onKeyUp={this.onChangeName}
                          />
                          <span
                            className={
                              this.state.name_err ? "error show" : "error"
                            }
                          >
                            Please enter your name
                          </span>
                        </div>

                        <div className="inputWrap">
                          <input
                            className="input"
                            type="text"
                            placeholder="Email Address"
                            ref={this.sEmail}
                            onKeyUp={this.onChangeSEmail}
                          />
                          <span
                            className={
                              this.state.email_err ? "error show" : "error"
                            }
                          >
                            Please enter a valid email address
                          </span>
                        </div>

                        <div className="inputWrap">
                          <input
                            className="input"
                            type="password"
                            placeholder="Password"
                            ref={this.sPass}
                            onKeyUp={this.onChangeSPassword}
                          />
                          <span
                            className={
                              this.state.new_pass_err ? "error show" : "error"
                            }
                          >
                            Please enter your password
                          </span>
                        </div>

                        <div className="inputWrap">
                          <input
                            className="input"
                            type="password"
                            placeholder="Repeat Password"
                            ref={this.confirmPass}
                            onKeyUp={this.onChangeConfirmPass}
                          />
                          <span
                            className={
                              this.state.confirm_pass_err
                                ? "error show"
                                : "error"
                            }
                          >
                            Passwords don't match
                          </span>
                        </div>
                        <div className="">
                          <button
                            className="button login-btn"
                            type="submit"
                            onClick={this.submitSignup}
                          >
                            Sign up
                          </button>
                          {/* <Link>
                            <span>
                              Sign up{" "}
                              <i className="icon-material-outline-arrow-right-alt" />
                            </span>
                          </Link> */}
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default LoginPage;
