import React, { Component } from "react";

export default class RESContent extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <>
        <div className="container layout-container">
          <div className="reset-email-sent-container has-text-centered">
            <h1 className="title">Reset email sent</h1>
            <h3 className="subtitle">
              Your password has been sent to you via email.
              <span>It should be in your mailbox shortly.</span>
            </h3>
          </div>
        </div>
      </>
    );
  }
}
