import React, { Component } from "react";
import Link from "next/link";
import Router from "next/router";
import env from "../../../constant.json";

export default class RPContent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email_err: false,
      new_pass_err: false,
      confirm_pass_err: false,
      signup_err: false
    };

    this.sEmail = React.createRef();
    this.sPass = React.createRef();
    this.confirmPass = React.createRef();
  }

  componentDidMount() {
    const token = localStorage.getItem("token");
    if (token) {
      this.setState({
        token: localStorage.getItem("token"),
        isLoading: false
      });
    }
    const userId = localStorage.getItem("userId");
    if (userId) {
      this.setState({
        userId: localStorage.getItem("userId"),
        isLoading: false
      });
    }
    const role = localStorage.getItem("role");
    if (role) {
      this.setState({
        role: localStorage.getItem("role")
      });
    }
    if (token) {
      Router.push("/about");
    }
  }

  onChangeSEmail = event => {
    const email = this.sEmail.current.value;

    let lastAtPos = email.lastIndexOf("@");
    let lastDotPos = email.lastIndexOf(".");
    if (email.trim().length === 0) {
      this.setState({ email_err: true });
      return;
    } else {
      if (
        !(
          lastAtPos < lastDotPos &&
          lastAtPos > 0 &&
          email.indexOf("@@") == -1 &&
          lastDotPos > 2 &&
          email.length - lastDotPos > 2
        )
      ) {
        this.setState({ email_err: true });
      } else {
        this.setState({ email_err: false });
      }

      return;
    }
  };

  onChangeSPassword = event => {
    const pass = this.sPass.current.value;
    const confirmpass = this.confirmPass.current.value;
    if (pass.trim().length === 0) {
      this.setState({ new_pass_err: true });
      return;
    } else {
      if (confirmpass != pass) {
        this.setState({ confirm_pass_err: true });
      } else {
        this.setState({ confirm_pass_err: false });
      }
      this.setState({ new_pass_err: false });
      return;
    }
  };
  onChangeConfirmPass = event => {
    const pass = this.sPass.current.value;
    const confirmpass = this.confirmPass.current.value;
    if (confirmpass.trim().length === 0) {
      this.setState({ confirm_pass_err: true });
      return;
    } else {
      if (confirmpass != pass) {
        this.setState({ confirm_pass_err: true });
      } else {
        this.setState({ confirm_pass_err: false });
      }
      return;
    }
  };

  submitSignup = event => {
    event.preventDefault();

    const email = this.sEmail.current.value;
    const pass = this.sPass.current.value;
    const confirmpass = this.confirmPass.current.value;

    if (email.trim().length === 0) {
      this.setState({ email_err: true });
      return;
    } else {
      let lastAtPos = email.lastIndexOf("@");
      let lastDotPos = email.lastIndexOf(".");

      if (
        !(
          lastAtPos < lastDotPos &&
          lastAtPos > 0 &&
          email.indexOf("@@") == -1 &&
          lastDotPos > 2 &&
          email.length - lastDotPos > 2
        )
      ) {
        this.setState({ email_err: true });
        return;
      }
    }
    if (pass.trim().length === 0) {
      this.setState({ new_pass_err: true });
      return;
    } else {
      if (confirmpass != pass) {
        this.setState({ confirm_pass_err: true });
        return;
      }
    }
    if (confirmpass.trim().length === 0) {
      this.setState({ confirm_pass_err: true });
      return;
    } else {
      if (confirmpass != pass) {
        this.setState({ confirm_pass_err: true });
        return;
      }
    }
    let requestBody = {
      query: `
        mutation resetPassword($email: String!, $password: String!,$confirmpassword:String!) {
          resetPassword(resetpassword: {email: $email, password: $password,confirmpassword: $confirmpassword}) {
            _id
            email
          }
        }
      `,
      variables: {
        email: email,
        password: pass,
        confirmpassword: confirmpass
      }
    };

    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          this.setState({ signup_err: true });
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(resData => {
        if (resData.data.resetPassword._id) {
          Router.push("/login");
        } else {
          this.setState({ signup_err: true });
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  render() {
    return (
      <div className="container layout-container">
        <div className="container reset-pasword-container has-text-centered">
          <h1 className="title">Reset your pasword</h1>
          <div class="inputWrap">
            <input
              className="input Danger"
              type="text"
              placeholder="Email"
              ref={this.sEmail}
              onKeyUp={this.onChangeSEmail}
            />
            <span className={this.state.email_err ? "error show" : "error"}>
              Please enter a valid email address
            </span>
          </div>
          <div class="inputWrap">
            <input
              className="input Danger"
              type="password"
              placeholder="New password"
              ref={this.sPass}
              onKeyUp={this.onChangeSPassword}
            />
            <span className={this.state.new_pass_err ? "error show" : "error"}>
              Please enter your New Password
            </span>
          </div>
          <div class="inputWrap">
            <input
              className="input Danger"
              type="password"
              placeholder="Confirm new password"
              ref={this.confirmPass}
              onKeyUp={this.onChangeConfirmPass}
            />
            <span
              className={this.state.confirm_pass_err ? "error show" : "error"}
            >
              Passwords don't match
            </span>
          </div>
          <button
            className="button is-link is-medium"
            type="submit"
            onClick={this.submitSignup}
          >
            Reset
          </button>

          {/* <Link href="/">
            <a className="button is-link is-medium">Reset</a>
          </Link> */}
        </div>
      </div>
    );
  }
}
