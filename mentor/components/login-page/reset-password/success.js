import React, { Component } from "react";
import Link from "next/link";
export default class PRSuccess extends Component {
  render() {
    return (
      <div className="container layout-container">
        <div className="container reset-pasword-success-container has-text-centered">
          <h1 className="title">Password succesfully reset</h1>
          <Link href="/login">
            <a className="button is-link is-medium">Login now</a>
          </Link>
        </div>
      </div>
    );
  }
}
