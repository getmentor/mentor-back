import React, { Component } from "react";
import LoggedInuser from "../components/inside-header";
import LoggedOutUser from "../components/header";

export default class MainHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: null,
      userId: null,
      role: null
    };
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    const token = localStorage.getItem("token");
    if (token) {
      this.setState({
        token: localStorage.getItem("token")
      });
    }
    const userId = localStorage.getItem("userId");
    if (userId) {
      this.setState({
        userId: localStorage.getItem("userId")
      });
    }
    const role = localStorage.getItem("role");
    if (role) {
      this.setState({
        role: localStorage.getItem("role")
      });
    }
  }

  render() {
    return (
      <>
        {!this.state.token && <LoggedOutUser />}
        {this.state.token && <LoggedInuser />}
      </>
    );
  }
}
