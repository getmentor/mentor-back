import React, { Component } from "react";
import Link from "next/link";

class MobileMenu extends Component {
  handleToggleMenuClick = () => {
    let mobileMenuElement = document.getElementById("mobile-menu-wrap");
    mobileMenuElement.classList.toggle("show-menu");
    document.getElementById("backdrop").classList.remove("show");
  };
  render() {
    return (
      <>
        <div
          className="mobile-menu-wrap has-text-centered"
          id="mobile-menu-wrap"
        >
          <ul>
            <li>
              <Link href="/about">
                <a onClick={this.handleToggleMenuClick}>About</a>
              </Link>
            </li>
            <li>
              <Link href="/search">
                <a onClick={this.handleToggleMenuClick}>Search</a>
              </Link>
            </li>
            <li>
              <Link href="/contact-us">
                <a onClick={this.handleToggleMenuClick}>Contact us</a>
              </Link>
            </li>
            <li>
              <Link href="/login">
                <a onClick={this.handleToggleMenuClick}>Login / Register</a>
              </Link>
            </li>
          </ul>
        </div>
      </>
    );
  }
}
export default MobileMenu;
