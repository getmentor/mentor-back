import React, { Component } from "react";
import { MultiSelect } from "@progress/kendo-react-dropdowns";
import "@progress/kendo-theme-default/dist/all.css";
import "./multi-select-languages.scss";
const languages = [
  "English",
  "Arabic",
  "Danish",
  "French",
  "German",
  "Japanese",
  "Russian",
  "Chinese"
];
export default class MultiSelectLanguages extends Component {
  state = { value: [] };

  onChange = event => {
    this.setState({
      value: [...event.target.value]
    });
  };
  render() {
    return (
      <div className="multiselect-wrapper">
        <div>
          <MultiSelect
            data={languages}
            onChange={this.onChange}
            value={this.state.value}
          />
        </div>
      </div>
    );
  }
}
