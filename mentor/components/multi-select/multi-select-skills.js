import React, { Component } from "react";
import { MultiSelect } from "@progress/kendo-react-dropdowns";
import "@progress/kendo-theme-default/dist/all.css";
import "./multi-select-skills.scss";
const skills = [
  "Skill 1",
  "Skill 2",
  "Skill 3",
  "Skill 4",
  "Skill 5",
  "Skill 6",
  "sKILL 7",
  "sKILL 8"
];
export default class MultiSelectSkills extends Component {
  state = { value: [] };

  onChange = event => {
    this.setState({
      value: [...event.target.value]
    });
  };
  render() {
    return (
      <div className="multiselect-wrapper">
        <div>
          <MultiSelect
            data={skills}
            onChange={this.onChange}
            value={this.state.value}
          />
        </div>
      </div>
    );
  }
}
