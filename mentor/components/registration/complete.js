import React, { Component } from "react";

export default class RegistrationComp extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <>
        <div className="container layout-container">
          <div className="email-sent-container has-text-centered">
            <h1 className="title">Action required</h1>
            <h3 className="subtitle">
              An email has been sent into your account,{" "}
              <span>Please verify</span>
            </h3>
          </div>
        </div>
      </>
    );
  }
}
