import React, { Component } from "react";
import Link from "next/link";
import "./success.scss";

export default class RegistrationComp extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <>
        <div className="container layout-container">
          <div className="reg-success-container has-text-centered">
            <h1 className="title">
              Your account has been successfully verified,{" "}
              <span classname="is-block">please login.</span>
            </h1>
            <h3 className="subtitle">Thank you.</h3>
            <Link href="/login">
              <button className="button is-success is-outlined">Login</button>
            </Link>
          </div>
        </div>
      </>
    );
  }
}
