import React, { Component } from "react";
import "./approval-success.scss";

export default class UserApproved extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <>
        <div className="container layout-container">
          <div className="apr-success-container has-text-centered">
            <h1 className="title">Teacher account activated successfully</h1>
          </div>
        </div>
      </>
    );
  }
}
