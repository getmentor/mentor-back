import React, { Component } from "react";
import "./rejection-success.scss";

export default class UserRejected extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <>
        <div className="container layout-container">
          <div className="reg-success-container has-text-centered">
            <h1 className="title">Teacher account rejected successfully</h1>
          </div>
        </div>
      </>
    );
  }
}
