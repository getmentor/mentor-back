import React, { Component } from "react";
import Link from "next/link";
import "./teacher-registration.scss";
// import { Router } from "next/router";
import Router, { withRouter } from "next/router";
import env from "../../../constant.json";

import QiscusSDKCore from "qiscus-sdk-core";
const qiscus = new QiscusSDKCore();

export default class TeacherRegistrationContent extends Component {
  constructor(props) {
    super(props);

    this.emailErrors = "Please enter your email address";
    this.state = {
      name_err: false,
      email_err: false,
      new_pass_err: false,
      confirm_pass_err: false,
      skills_err: false,
      resume_link_err: false,
      signup_err: ""
    };

    this.sName = React.createRef();
    this.sEmail = React.createRef();
    this.sPass = React.createRef();
    this.confirmPass = React.createRef();
    this.skills = React.createRef();
    this.resumeLink = React.createRef();
  }
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  onChangeName = event => {
    const name = this.sName.current.value;
    if (name.trim().length === 0) {
      this.setState({ name_err: true });
      return;
    } else {
      this.setState({ name_err: false });
      return;
    }
  };

  onChangeSEmail = event => {
    const email = this.sEmail.current.value;

    let lastAtPos = email.lastIndexOf("@");
    let lastDotPos = email.lastIndexOf(".");

    if (email.trim().length === 0) {
      this.emailErrors = "Please enter your email address";

      this.setState({ email_err: true });
      return;
    } else {
      if (
        !(
          lastAtPos < lastDotPos &&
          lastAtPos > 0 &&
          email.indexOf("@@") == -1 &&
          lastDotPos > 2 &&
          email.length - lastDotPos > 2
        )
      ) {
        this.emailErrors = "Please enter a valid email address";
        this.setState({ email_err: true });
      } else {
        this.setState({ email_err: false });
      }

      return;
    }
  };

  onChangeSPassword = event => {
    const pass = this.sPass.current.value;
    const confirmpass = this.confirmPass.current.value;
    if (pass.trim().length === 0) {
      this.setState({ new_pass_err: true });
      return;
    } else {
      if (confirmpass != pass) {
        this.setState({ confirm_pass_err: true });
      } else {
        this.setState({ confirm_pass_err: false });
      }
      this.setState({ new_pass_err: false });
      return;
    }
  };
  onChangeConfirmPass = event => {
    const confirmpass = this.confirmPass.current.value;
    const pass = this.sPass.current.value;
    if (confirmpass.trim().length === 0) {
      this.setState({ confirm_pass_err: true });
      return;
    } else {
      if (confirmpass != pass) {
        this.setState({ confirm_pass_err: true });
      } else {
        this.setState({ confirm_pass_err: false });
      }
      return;
    }
  };

  onChangeSkills = event => {
    const skills = this.skills.current.value;
    if (skills.trim().length === 0) {
      this.setState({ skills_err: true });
      return;
    } else {
      this.setState({ skills_err: false });
      return;
    }
  };

  onChangeResumeLink = event => {
    const resume_link = this.resumeLink.current.value;
    var res = resume_link.match(
      /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g
    );

    if (resume_link.trim().length === 0) {
      this.setState({ resume_link_err: true });
      return;
    } else {
      if (res == null) {
        this.setState({ resume_link_err: true });
      } else {
        this.setState({ resume_link_err: false });
      }
      return;
    }
  };

  submitSignup = event => {
    event.preventDefault();
    this.Initialization();

    const name = this.sName.current.value;
    const email = this.sEmail.current.value;
    const pass = this.sPass.current.value;
    const confirmpass = this.confirmPass.current.value;
    const role = 2;
    const skills = this.skills.current.value;
    const resume_link = this.resumeLink.current.value;

    const last_name = "";
    const profile_pic = "no_user.png";
    const short_desc = "";
    const main_desc = "";
    const language = "";
    const location = "";
    // const expertise = "";
    // const toolkit = "";
    // const experience = "";
    // const education = "";
    // const certificates = "";
    // const social_media_links = "";

    const linkedin_url = "";
    const twitte_url = "";
    const medium_url = "";

    let errorFlag = 0;

    let lastAtPos = email.lastIndexOf("@");
    let lastDotPos = email.lastIndexOf(".");

    if (name.trim().length === 0) {
      this.setState({ name_err: true });
      errorFlag = 1;
    }
    if (email.trim().length === 0) {
      this.setState({ email_err: true });
      errorFlag = 1;
    } else {
      if (
        !(
          lastAtPos < lastDotPos &&
          lastAtPos > 0 &&
          email.indexOf("@@") == -1 &&
          lastDotPos > 2 &&
          email.length - lastDotPos > 2
        )
      ) {
        this.setState({ emailerr: true });
        errorFlag = 1;
      }
    }
    if (pass.trim().length === 0) {
      this.setState({ new_pass_err: true });
      errorFlag = 1;
    } else {
      if (confirmpass != pass) {
        this.setState({ confirm_pass_err: true });
        errorFlag = 1;
      }
    }
    if (confirmpass.trim().length === 0) {
      this.setState({ confirm_pass_err: true });
      errorFlag = 1;
    } else {
      if (confirmpass != pass) {
        this.setState({ confirm_pass_err: true });
        errorFlag = 1;
      }
    }

    if (skills.trim().length === 0) {
      this.setState({ skills_err: true });
      errorFlag = 1;
    }
    if (resume_link.trim().length === 0) {
      this.setState({ resume_link_err: true });
      errorFlag = 1;
    } else {
      var res = resume_link.match(
        /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g
      );
      if (res == null) {
        this.setState({ resume_link_err: true });
        errorFlag = 1;
      }
    }

    if (errorFlag === 1) {
      return;
    }

    let requestBody = {
      query: `
      mutation CreateUser($name: String!,$last_name:String!,$email: String!, $password: String!, $skills: String!, $resume_link: String!, $role: Int!,$profile_pic:String!,$short_desc:String!,$main_desc:String!,$language:String!,$location:String!,$linkedin_url:String!,$twitte_url:String!,$medium_url:String!) {
          createUser(userInput: {name: $name,last_name:$last_name,email: $email, password: $password, skills:$skills, resume_link:$resume_link, role: $role , profile_pic: $profile_pic, short_desc: $short_desc, main_desc: $main_desc, language: $language, location: $location,linkedin_url: $linkedin_url,twitte_url:$twitte_url,medium_url:$medium_url}) {
            _id
            email
          }
        }
      `,
      variables: {
        name: name,
        last_name: last_name,
        email: email,
        password: pass,
        skills: skills,
        profile_pic: profile_pic,
        short_desc: short_desc,
        main_desc: main_desc,
        language: language,
        location: location,
        // expertise: expertise,
        // toolkit: toolkit,
        // experience: experience,
        // education: education,
        // certificates: certificates,
        // social_media_links: social_media_links,
        linkedin_url: linkedin_url,
        twitte_url: twitte_url,
        medium_url: medium_url,
        resume_link: resume_link,
        role: role
      }
    };
    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    })
      .then(response => {
        response.json().then(result => {
          console.log(result);

          if (result.data.createUser !== null) {
            qiscus
              .setUser(email, email, name, profile_pic, {})
              .then(response => {
                this.CreateChatRoom();
              })
              .catch(function(error) {});
            this.setState({
              signup_err: ""
            });

            Router.push("/registration-complete");
          } else {
            result.errors.forEach(message => {
              console.log(message.message);
              this.setState({
                signup_err: " Signup Failed!" + message.message
              });
            });
          }
        });
      })
      .catch(err => {
        console.log("Error : " + err);
        this.setState({
          signup_err: " Signup Failed!"
        });
      });

    /* fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          this.setState({ signup_err: true });

          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(resData => {
        if (resData.data.createUser._id) {
          qiscus
            .setUser(email, email, name, profile_pic, {})
            .then(response => {
              this.CreateChatRoom();
            })
            .catch(function(error) {});
          Router.push("/registration-complete");
          console.log(resData);
        } else {
          console.log(resData);
          console.log(resData.data.message);
          this.setState({ signup_err: true });
        }
      })
      .catch(err => {
        console.log(err);
      });*/
  };
  //  Initialization Qiscus Chat SDK
  Initialization = event => {
    qiscus.init({
      AppId: "mentor-r6ledbypleisco", // "mentorcha-ehkoh6xenpl"
      options: {
        commentDeletedCallback: function(data) {},
        commentDeliveredCallback: function(data) {},
        commentReadCallback: function(data) {},
        presenceCallback: function(data) {},
        typingCallback: function(data) {},
        onReconnectCallback: function(data) {},
        newMessagesCallback: function(messages) {},
        roomClearedCallback: function(data) {}
      }
    });
    // console.log(qiscus);
  };
  render() {
    return (
      <>
        <div className="container layout-container">
          <div className="teacher-signup-wrap" id="teacher-signup-wrap">
            <div className="tabs is-boxed" />

            <div className="container">
              <form
                className="teacher-signup-form"
                onSubmit={this.submitSignup}
              >
                <div id="signupPane" className="tab-content active-content">
                  <p className="welcome-text">
                    Let's create a teacher account!
                  </p>

                  <div className="student-teacher-tab-wrap">
                    <div className="container">
                      {/* <span
                        id="signupError"
                        className={
                          this.state.signup_err
                            ? "signup_error show"
                            : "signup_error"
                        }
                      >
                        Signup Failed!
                      </span> */}

                      <span id="signupError" className="signup_error show">
                        {this.state.signup_err}
                      </span>

                      <div className="tab-content active-content">
                        <div className="inputWrap">
                          <input
                            className="input"
                            type="text"
                            placeholder="Name"
                            ref={this.sName}
                            onKeyUp={this.onChangeName}
                          />
                          <span
                            className={
                              this.state.name_err ? "error show" : "error"
                            }
                          >
                            Please enter your name
                          </span>
                        </div>
                        <div className="inputWrap">
                          <input
                            className="input"
                            type="text"
                            placeholder="Email Address"
                            ref={this.sEmail}
                            onKeyUp={this.onChangeSEmail}
                          />
                          <span
                            className={
                              this.state.email_err ? "error show" : "error"
                            }
                          >
                            {this.emailErrors}
                            {/* Please enter a valid email address */}
                          </span>
                        </div>
                        <div className="inputWrap">
                          <input
                            className="input"
                            type="password"
                            placeholder="Password"
                            ref={this.sPass}
                            onKeyUp={this.onChangeSPassword}
                          />
                          <span
                            className={
                              this.state.new_pass_err ? "error show" : "error"
                            }
                          >
                            Please enter your password
                          </span>
                        </div>
                        <div className="inputWrap">
                          <input
                            className="input"
                            type="password"
                            placeholder="Repeat Password"
                            ref={this.confirmPass}
                            onKeyUp={this.onChangeConfirmPass}
                          />
                          <span
                            className={
                              this.state.confirm_pass_err
                                ? "error show"
                                : "error"
                            }
                          >
                            Passwords don't match
                          </span>
                        </div>
                        <div className="inputWrap">
                          <textarea
                            className="input"
                            placeholder="Enter your skills separated by commas"
                            ref={this.skills}
                            onKeyUp={this.onChangeSkills}
                          />
                          <span
                            className={
                              this.state.skills_err ? "error show" : "error"
                            }
                          >
                            Please enter your skills
                          </span>
                        </div>
                        <div className="inputWrap">
                          <input
                            className="input"
                            type="url"
                            placeholder="Paste your resume link here"
                            ref={this.resumeLink}
                            onKeyUp={this.onChangeResumeLink}
                          />
                          <span
                            className={
                              this.state.resume_link_err
                                ? "error show"
                                : "error"
                            }
                          >
                            Please enter your resume link
                          </span>
                        </div>
                        <button
                          className="button login-btn"
                          type="submit"
                          onClick={this.submitSignup}
                        >
                          Sign up
                        </button>
                        {/* <Link href="/registration-complete">
                          <a className="button login-btn">Sign up</a>
                        </Link> */}
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </>
    );
  }
}
