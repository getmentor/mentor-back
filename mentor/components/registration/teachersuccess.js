import React, { Component } from "react";
import Link from "next/link";
import "./teachersuccess.scss";

export default class TeacherRegistrationComp extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <>
        <div className="container layout-container">
          <div className="teacher-reg-success-container has-text-centered">
            <h1 className="title">
              Thanks for confirming your email.your profile is under review by
              admin and you will be notified on approval of your registration.
            </h1>
            <h3 className="subtitle">Thank you.</h3>
          </div>
        </div>
      </>
    );
  }
}
