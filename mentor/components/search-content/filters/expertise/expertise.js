import React, { Component } from "react";
import "./expertise.scss";

export default class ExpertiseFilter extends Component {
  render() {
    return (
      <>
        <div className="container">
          <h1 className="title is-6">EXPERTISE</h1>
          <div className="container">
            <label class="checkbox">
              <input type="checkbox" />
              Saas
            </label>
          </div>
          <div className="container">
            <label class="checkbox">
              <input type="checkbox" />
              Stratups
            </label>
          </div>
          <div className="container">
            <label class="checkbox">
              <input type="checkbox" />
              B2B
            </label>
          </div>
          <div className="container">
            <label class="checkbox">
              <input type="checkbox" />
              B2C
            </label>
          </div>
        </div>
      </>
    );
  }
}
