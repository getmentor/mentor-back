import React, { Component } from "react";
import "./language.scss";

export default class LanguageFilter extends Component {
  render() {
    return (
      <>
        <div className="container">
          <h1 className="title is-6">LANGUAGE</h1>
          <div className="container">
            <label class="checkbox">
              <input type="checkbox" />
              English
            </label>
          </div>
          <div className="container">
            <label class="checkbox">
              <input type="checkbox" />
              French
            </label>
          </div>
          <div className="container">
            <label class="checkbox">
              <input type="checkbox" />
              German
            </label>
          </div>
          <div className="container">
            <label class="checkbox">
              <input type="checkbox" />
              Spanish
            </label>
          </div>
        </div>
      </>
    );
  }
}
