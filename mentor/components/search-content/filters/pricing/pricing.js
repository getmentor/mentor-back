import React, { Component } from "react";
import "./pricing.scss";

export default class PricingFilter extends Component {
  render() {
    return (
      <>
        <div className="container">
          <h1 className="title is-6">PRICING</h1>
          <div class="control">
            <label class="radio">
              <input type="radio" name="foobar" />
              Free
            </label>
            <label class="radio">
              <input type="radio" name="foobar" />
              Less than 25$
            </label>
            <label class="radio">
              <input type="radio" name="foobar" />
              25$-50$
            </label>
          </div>
        </div>
      </>
    );
  }
}
