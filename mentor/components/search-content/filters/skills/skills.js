import React, { Component } from "react";
import "./skills.scss";

export default class SkillsFilter extends Component {
  render() {
    return (
      <>
        <div className="container">
          <h1 className="title is-6">SKILLS</h1>
          <div className="container">
            <label class="checkbox">
              <input type="checkbox" />
              Advice on funding
            </label>
          </div>
          <div className="container">
            <label class="checkbox">
              <input type="checkbox" />
              Bootstrapping
            </label>
          </div>
          <div className="container">
            <label class="checkbox">
              <input type="checkbox" />
              Content marketing
            </label>
          </div>
          <div className="container">
            <label class="checkbox">
              <input type="checkbox" />
              Design / UX
            </label>
          </div>
        </div>
      </>
    );
  }
}
