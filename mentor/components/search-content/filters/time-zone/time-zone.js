import React, { Component } from "react";
import "./time-zone.scss";

export default class TimeZoneFilter extends Component {
  render() {
    return (
      <>
        <div className="container">
          <h1 className="title is-6">TIME ZONE</h1>
          <div className="container">
            <label class="checkbox">
              <input type="checkbox" />
              only in your time zone
            </label>
          </div>
          <div className="container">
            <label class="checkbox">
              <input type="checkbox" />
              +/- 3 hrs of your time zone
            </label>
          </div>
          <div className="container">
            <label class="checkbox">
              <input type="checkbox" />
              +/- 7 hrs of your time zone
            </label>
          </div>
          <div className="container">
            <label class="checkbox">
              <input type="checkbox" />
              +/- 12 hrs of your time zone
            </label>
          </div>
        </div>
      </>
    );
  }
}
