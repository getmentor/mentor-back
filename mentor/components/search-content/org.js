import React, { Component } from "react";
import "./search.scss";
import SkillsFilter from "../search/filters/skills/skills";
import PricingFilter from "../search/filters/pricing/pricing";
import ExpertiseFilter from "../search/filters/expertise/expertise";
import TimeZoneFilter from "../search/filters/time-zone/time-zone";
import LanguageFilter from "../search/filters/language/language";

export default class Search extends Component {
  render() {
    return (
      <div className="container search-content">
        <div className="columns">
          <div className="column is-one-quarter is-desktop">
            <h1 className="title is-4">Find a teacher</h1>
          </div>
          <div className="column is-three-quarters search-results is-desktop">
            {" "}
            <div className="columns">
              <div className="column is-one-fifth number-of-results">
                <h3 className="title is-6">100 teachers found</h3>
              </div>
              <div className="column is-three-fifths search-box-wrap">
                <nav class="level">
                  <div class="level-left">
                    <div class="level-item">
                      <div class="field has-addons">
                        <p class="control is-inline-block search-box">
                          <input
                            class="input"
                            type="text"
                            placeholder="Find a teacher"
                          />
                        </p>
                        <p class="control is-inline-block">
                          <button class="button">Search</button>
                        </p>
                      </div>
                    </div>
                  </div>
                </nav>
              </div>
              <div className="column is-one-fifth sort-wrap">
                <div class="select">
                  <select>
                    <option>Relevant</option>
                    <option>Rating</option>
                    <option>Price</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="columns">
          <div className="column is-one-quarter filters">
            <div className="container filter-box">
              <SkillsFilter />
            </div>
            <div className="container filter-box">
              <PricingFilter />
            </div>
            <div className="container filter-box">
              <ExpertiseFilter />
            </div>
            <div className="container filter-box">
              <TimeZoneFilter />
            </div>
            <div className="container filter-box">
              <LanguageFilter />
            </div>
          </div>
          <div className="column is-three-quarters results-desk">
            <div className="results-panel">
              <div className="container teacher-info">
                <div className="columns inner">
                  <div className="column is-one-quarter left">
                    <figure className="image">
                      <img className="is-rounded" src="/assets/img/t_001.jpg" />
                    </figure>
                    <ul className="is-inline-block">
                      <li className="is-inline-block">
                        <span className="spec">Price</span>
                        <span className="value free">Free</span>
                      </li>
                      <li className="is-inline-block">
                        <span className="spec">Reviews</span>
                        <span className="value">99</span>
                      </li>
                    </ul>
                  </div>
                  <div className="column three-quarters right">
                    <div className="column spec-wrap is-block">
                      <div className="columns">
                        <div className="column is-four-fifths left-blk">
                          <h1 className="title is-4 teacher-name">Tony Luke</h1>
                          <h1 className="subtitle is-6 teacher-name">
                            Founder @ Cloud_mega digital
                          </h1>
                          <div className="columns">
                            <div className="column is-two-fifths location-wrap">
                              <p>
                                <img
                                  src="/assets/img/location.png"
                                  alt="location-icon"
                                />
                                <span className="subtitle">
                                  <span className="current-city">
                                    Barcelona
                                  </span>
                                  <span className="current-country">Spain</span>
                                </span>
                              </p>
                              <p>
                                <span className="subtitle">(+01:00 UTC)</span>
                              </p>
                            </div>
                            <div className="column is-one-fifth language-wrap">
                              <p>
                                <span className="subtitle">Spanish</span>
                              </p>
                              <p>
                                <span className="subtitle">English</span>
                              </p>
                            </div>
                            <div className="column is-two-fifths native-wrap">
                              <p>
                                <span className="subtitle">
                                  <span className="current-city">
                                    Christchurch
                                  </span>
                                  <span className="current-country">
                                    Newzealand
                                  </span>
                                </span>
                              </p>
                            </div>
                          </div>
                        </div>
                        <div className="column is-one-fifth right-blk">
                          <p className="is-inline-block">
                            <img src="/assets/img/star.png" alt="star-rating" />
                            <p className="title is-5 is-inline-block rating">
                              4.5
                            </p>
                            <p className="subtitle is-6 is-inline-block">
                              99 sessions
                            </p>
                          </p>
                          <a className="button is-success is-outlined view-profile">
                            View profile
                          </a>
                        </div>
                      </div>
                    </div>
                    <div className="column spec-wrap">
                      <div className="container">
                        <p className="subtitle is-6 des">
                          Lorem Ipsum is simply dummy text of the printing and
                          typesetting industry. Lorem Ipsum has been the
                          industry's standard dummy text ever since the 1500s,
                          when an unknown printer took a galley of type and
                          scrambled it to make a type specimen book. It has
                          survived not only five centuries, but also the leap
                          into electronic typesetting, remaining essentially
                          unchanged.
                        </p>
                        <div class="tags">
                          <span class="tag">React</span>
                          <span class="tag">Typescript</span>
                          <span class="tag">Node</span>
                          <span class="tag">Angular</span>
                          <span class="tag">Javascript</span>
                          <span class="tag">Express</span>
                          <span class="tag">MongoDB</span>
                          <span class="tag">Sass</span>
                          <span class="tag">Vue</span>
                          <span class="tag">Ionic</span>
                          <span class="tag">SQL</span>
                          <span class="tag">PHP</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="container teacher-info">
                <div className="columns inner">
                  <div className="column is-one-quarter left">
                    <figure className="image">
                      <img className="is-rounded" src="/assets/img/t_002.jpg" />
                    </figure>
                    <ul className="is-inline-block">
                      <li className="is-inline-block">
                        <span className="spec">Price</span>
                        <span className="value">$50 / hour</span>
                      </li>
                      <li className="is-inline-block">
                        <span className="spec">Reviews</span>
                        <span className="value">99</span>
                      </li>
                    </ul>
                  </div>
                  <div className="column three-quarters right">
                    <div className="column spec-wrap is-block">
                      <div className="columns">
                        <div className="column is-four-fifths left-blk">
                          <h1 className="title is-4 teacher-name">
                            Jonathan Gregory
                          </h1>
                          <h1 className="subtitle is-6 teacher-name">
                            Founder @ life_hacks consultants
                          </h1>
                          <div className="columns">
                            <div className="column is-two-fifths location-wrap">
                              <p>
                                <img
                                  src="/assets/img/location.png"
                                  alt="location-icon"
                                />
                                <span className="subtitle">
                                  <span className="current-city">Athens</span>
                                  <span className="current-country">
                                    Greece
                                  </span>
                                </span>
                              </p>
                              <p>
                                <span className="subtitle">(+02:00 UTC)</span>
                              </p>
                            </div>
                            <div className="column is-one-fifth language-wrap">
                              <p>
                                <span className="subtitle">English</span>
                              </p>
                              <p>
                                <span className="subtitle">Greek</span>
                              </p>
                            </div>
                            <div className="column is-two-fifths native-wrap">
                              <p>
                                <span className="subtitle">
                                  <span className="current-city">Florida</span>
                                  <span className="current-country">
                                    United States of America
                                  </span>
                                </span>
                              </p>
                            </div>
                          </div>
                        </div>
                        <div className="column is-one-fifth right-blk">
                          <p className="is-inline-block">
                            <img src="/assets/img/star.png" alt="star-rating" />
                            <p className="title is-5 is-inline-block rating">
                              4.4
                            </p>
                            <p className="subtitle is-6 is-inline-block">
                              82 sessions
                            </p>
                          </p>
                          <a className="button is-success is-outlined view-profile">
                            View profile
                          </a>
                        </div>
                      </div>
                    </div>
                    <div className="column spec-wrap">
                      <div className="container">
                        <p className="subtitle is-6 des">
                          Lorem Ipsum is simply dummy text of the printing and
                          typesetting industry. Lorem Ipsum has been the
                          industry's standard dummy text ever since the 1500s,
                          when an unknown printer took a galley of type and
                          scrambled it to make a type specimen book. It has
                          survived not only five centuries, but also the leap
                          into electronic typesetting, remaining essentially
                          unchanged.
                        </p>
                        <div class="tags">
                          <span class="tag">React</span>
                          <span class="tag">Typescript</span>
                          <span class="tag">Node</span>
                          <span class="tag">Javascript</span>
                          <span class="tag">Express</span>
                          <span class="tag">MongoDB</span>
                          <span class="tag">SQL</span>
                          <span class="tag">PHP</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="container teacher-info">
                <div className="columns inner">
                  <div className="column is-one-quarter left">
                    <figure className="image">
                      <img className="is-rounded" src="/assets/img/t_003.jpg" />
                    </figure>
                    <ul className="is-inline-block">
                      <li className="is-inline-block">
                        <span className="spec">Price</span>
                        <span className="value">$42 / hour</span>
                      </li>
                      <li className="is-inline-block">
                        <span className="spec">Reviews</span>
                        <span className="value">75</span>
                      </li>
                    </ul>
                  </div>
                  <div className="column three-quarters right">
                    <div className="column spec-wrap is-block">
                      <div className="columns">
                        <div className="column is-four-fifths left-blk">
                          <h1 className="title is-4 teacher-name">
                            Ayesha Zia
                          </h1>
                          <h1 className="subtitle is-6 teacher-name">
                            Founder @ Embedded_Geeks
                          </h1>
                          <div className="columns">
                            <div className="column is-two-fifths location-wrap">
                              <p>
                                <img
                                  src="/assets/img/location.png"
                                  alt="location-icon"
                                />
                                <span className="subtitle">
                                  <span className="current-city">Toronto</span>
                                  <span className="current-country">
                                    Canada
                                  </span>
                                </span>
                              </p>
                              <p>
                                <span className="subtitle">(-05:00 UTC)</span>
                              </p>
                            </div>
                            <div className="column is-one-fifth language-wrap">
                              <p>
                                <span className="subtitle">English</span>
                              </p>
                            </div>
                            <div className="column is-two-fifths native-wrap">
                              <p>
                                <span className="subtitle">
                                  <span className="current-city">
                                    Tanjung Pinang
                                  </span>
                                  <span className="current-country">
                                    Indonesia
                                  </span>
                                </span>
                              </p>
                            </div>
                          </div>
                        </div>
                        <div className="column is-one-fifth right-blk">
                          <p className="is-inline-block">
                            <img src="/assets/img/star.png" alt="star-rating" />
                            <p className="title is-5 is-inline-block rating">
                              4.1
                            </p>
                            <p className="subtitle is-6 is-inline-block">
                              58 sessions
                            </p>
                          </p>
                          <a className="button is-success is-outlined view-profile">
                            View profile
                          </a>
                        </div>
                      </div>
                    </div>
                    <div className="column spec-wrap">
                      <div className="container">
                        <p className="subtitle is-6 des">
                          Lorem Ipsum is simply dummy text of the printing and
                          typesetting industry. Lorem Ipsum has been the
                          industry's standard dummy text ever since the 1500s,
                          when an unknown printer took a galley of type and
                          scrambled it to make a type specimen book. It has
                          survived not only five centuries, but also the leap
                          into electronic typesetting, remaining essentially
                          unchanged.
                        </p>
                        <div class="tags">
                          <span class="tag">Robotics</span>
                          <span class="tag">Control systems</span>
                          <span class="tag">VLSI</span>
                          <span class="tag">Embedded systems</span>
                          <span class="tag">Micro integrated cicuits</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="container teacher-info">
                <div className="columns inner">
                  <div className="column is-one-quarter left">
                    <figure className="image">
                      <img className="is-rounded" src="/assets/img/t_004.jpg" />
                    </figure>
                    <ul className="is-inline-block">
                      <li className="is-inline-block">
                        <span className="spec">Price</span>
                        <span className="value">$40 / hour</span>
                      </li>
                      <li className="is-inline-block">
                        <span className="spec">Reviews</span>
                        <span className="value">78</span>
                      </li>
                    </ul>
                  </div>
                  <div className="column three-quarters right">
                    <div className="column spec-wrap is-block">
                      <div className="columns">
                        <div className="column is-four-fifths left-blk">
                          <h1 className="title is-4 teacher-name">
                            James Rutherford
                          </h1>
                          <h1 className="subtitle is-6 teacher-name">
                            Author @ Tech_Mantra
                          </h1>
                          <div className="columns">
                            <div className="column is-two-fifths location-wrap">
                              <p>
                                <img
                                  src="/assets/img/location.png"
                                  alt="location-icon"
                                />
                                <span className="subtitle">
                                  <span className="current-city">Toronto</span>
                                  <span className="current-country">
                                    Canada
                                  </span>
                                </span>
                              </p>
                              <p>
                                <span className="subtitle">(-05:00 UTC)</span>
                              </p>
                            </div>
                            <div className="column is-one-fifth language-wrap">
                              <p>
                                <span className="subtitle">English</span>
                              </p>
                            </div>
                            <div className="column is-two-fifths native-wrap">
                              <p>
                                <span className="subtitle">
                                  <span className="current-city">London</span>
                                  <span className="current-country">
                                    England
                                  </span>
                                </span>
                              </p>
                            </div>
                          </div>
                        </div>
                        <div className="column is-one-fifth right-blk">
                          <p className="is-inline-block">
                            <img src="/assets/img/star.png" alt="star-rating" />
                            <p className="title is-5 is-inline-block rating">
                              4
                            </p>
                            <p className="subtitle is-6 is-inline-block">
                              45 sessions
                            </p>
                          </p>
                          <a className="button is-success is-outlined view-profile">
                            View profile
                          </a>
                        </div>
                      </div>
                    </div>
                    <div className="column spec-wrap">
                      <div className="container">
                        <p className="subtitle is-6 des">
                          Lorem Ipsum is simply dummy text of the printing and
                          typesetting industry. Lorem Ipsum has been the
                          industry's standard dummy text ever since the 1500s,
                          when an unknown printer took a galley of type and
                          scrambled it to make a type specimen book. It has
                          survived not only five centuries, but also the leap
                          into electronic typesetting, remaining essentially
                          unchanged.
                        </p>
                        <div class="tags">
                          <span class="tag">Symphony</span>
                          <span class="tag">Laravel</span>
                          <span class="tag">MySQL</span>
                          <span class="tag">PHP</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="container teacher-info">
                <div className="columns inner">
                  <div className="column is-one-quarter left">
                    <figure className="image">
                      <img className="is-rounded" src="/assets/img/t_005.jpg" />
                    </figure>
                    <ul className="is-inline-block">
                      <li className="is-inline-block">
                        <span className="spec">Price</span>
                        <span className="value">$30 / hour</span>
                      </li>
                      <li className="is-inline-block">
                        <span className="spec">Reviews</span>
                        <span className="value">40</span>
                      </li>
                    </ul>
                  </div>
                  <div className="column three-quarters right">
                    <div className="column spec-wrap is-block">
                      <div className="columns">
                        <div className="column is-four-fifths left-blk">
                          <h1 className="title is-4 teacher-name">
                            Kim jiyong
                          </h1>
                          <h1 className="subtitle is-6 teacher-name">
                            Founder @ Code_Campus
                          </h1>
                          <div className="columns">
                            <div className="column is-two-fifths location-wrap">
                              <p>
                                <img
                                  src="/assets/img/location.png"
                                  alt="location-icon"
                                />
                                <span className="subtitle">
                                  <span className="current-city">Toronto</span>
                                  <span className="current-country">
                                    Canada
                                  </span>
                                </span>
                              </p>
                              <p>
                                <span className="subtitle">(-05:00 UTC)</span>
                              </p>
                            </div>
                            <div className="column is-one-fifth language-wrap">
                              <p>
                                <span className="subtitle">English</span>
                              </p>
                            </div>
                            <div className="column is-two-fifths native-wrap">
                              <p>
                                <span className="subtitle">
                                  <span className="current-city">Suwon</span>
                                  <span className="current-country">
                                    South Korea
                                  </span>
                                </span>
                              </p>
                            </div>
                          </div>
                        </div>
                        <div className="column is-one-fifth right-blk">
                          <p className="is-inline-block">
                            <img src="/assets/img/star.png" alt="star-rating" />
                            <p className="title is-5 is-inline-block rating">
                              4.3
                            </p>
                            <p className="subtitle is-6 is-inline-block">
                              68 sessions
                            </p>
                          </p>
                          <a className="button is-success is-outlined view-profile">
                            View profile
                          </a>
                        </div>
                      </div>
                    </div>
                    <div className="column spec-wrap">
                      <div className="container">
                        <p className="subtitle is-6 des">
                          Lorem Ipsum is simply dummy text of the printing and
                          typesetting industry. Lorem Ipsum has been the
                          industry's standard dummy text ever since the 1500s,
                          when an unknown printer took a galley of type and
                          scrambled it to make a type specimen book. It has
                          survived not only five centuries, but also the leap
                          into electronic typesetting, remaining essentially
                          unchanged.
                        </p>
                        <div class="tags">
                          <span class="tag">React</span>
                          <span class="tag">Typescript</span>
                          <span class="tag">Node</span>
                          <span class="tag">Angular</span>
                          <span class="tag">Javascript</span>
                          <span class="tag">Vue</span>
                          <span class="tag">Redux</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
