import React, { Component } from "react";
import Router from "next/router";

import SkillsFilter from "../search-content/filters/skills/skills";
import PricingFilter from "../search-content/filters/pricing/pricing";
import ExpertiseFilter from "../search-content/filters/expertise/expertise";
import TimeZoneFilter from "../search-content/filters/time-zone/time-zone";
import LanguageFilter from "../search-content/filters/language/language";
import Link from "next/link";
import MentorList from "../../components/Mentors/MentorList/MentorList";
import Spinner from "../../components/Spinner/Spinner";
import env from "../../constant.json";

export default class SearchContent extends Component {
  state = {
    token: null,
    userId: null,
    role: null,
    mentors: [],
    isLoading: false,
    postsperpage: 2,
    first: 0,
    last: 2,
    pageNumbers: [],
    totalmentors: 0,
    currentpage: 1
  };

  isActive = true;

  componentDidMount() {
    this.getTotalMentors();
    const currentpage = localStorage.getItem("currentpage");
    if (currentpage) {
      this.setState({
        currentpage: localStorage.getItem("currentpage")
      });
    }

    const token = localStorage.getItem("token");
    if (token) {
      this.setState({
        token: localStorage.getItem("token")
      });
    }
    const userId = localStorage.getItem("userId");
    if (userId) {
      this.setState({
        userId: localStorage.getItem("userId")
      });
    }
    const role = localStorage.getItem("role");
    if (role) {
      this.setState({
        role: localStorage.getItem("role")
      });
    }

    if (!token) {
      Router.push("/login");
    } else {
      if (role == 1) {
        Router.push("/search");
      } else if (role == 2) {
        Router.push("/about");
      }
    }

    this.fetchMentors();
  }

  async paginate(num) {
    localStorage.setItem("currentpage", num);
    const postsperpage = this.state.postsperpage;
    console.log(num);

    const new_last = postsperpage; //  2
    const new_first = num * postsperpage - new_last; //1*2 -2  =0//2*2-2 =2

    await this.setState({ first: new_first, last: new_last });

    this.fetchMentors();
  }
  getTotalMentors() {
    const requestBody = {
      query: `
          query TotalMentors {
            totalmentors {
              _id
            }
          }
        `
    };

    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(resData => {
        const total = resData.data.totalmentors.length;
        this.setState({ totalmentors: total });

        const totalMentors = this.state.totalmentors;
        const postsperpage = this.state.postsperpage;
        const pageNumbers = this.state.pageNumbers;
        for (let i = 1; i <= Math.ceil(totalMentors / postsperpage); i++) {
          pageNumbers.push(i);
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  fetchMentors() {
    const first = this.state.first;
    const last = this.state.last;

    console.log("fetchMentors : " + first);
    console.log("fetchMentors : " + last);

    this.setState({ isLoading: true });
    const requestBody = {
      query: `
          query Mentors($first: Int!, $last: Int!) {
            mentors (first: $first, last: $last) {
              _id 
              name
              last_name
              email
              password
              skills
              resume_link
              profile_pic
              short_desc
              main_desc
              language
              location
              linkedin_url
              twitte_url
              medium_url
              role
              status
              createdAt
              updatedAt
            }
          }
        `,
      variables: {
        first: first,
        last: last
      }
    };

    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(resData => {
        const mentors = resData.data.mentors;

        console.log(mentors);
        if (this.isActive) {
          this.setState({ mentors: mentors, isLoading: false });
        }
        window.scrollTo(0, 0);
      })
      .catch(err => {
        console.log(err);
        if (this.isActive) {
          this.setState({ isLoading: false });
        }
      });
  }
  componentWillUnmount() {
    this.isActive = false;
  }
  toggleFilters = () => {
    document.getElementById("filters").classList.toggle("hide");
  };

  render() {
    return (
      <div className="container layout-container">
        <div className="container search-content">
          <h1 className="title is-4 res-title">Find a teacher</h1>
          <a
            class="button is-success is-outlined res-filters"
            onClick={this.toggleFilters}
          >
            Filters
          </a>
          <div className="columns">
            <div className="column is-one-quarter filters hide" id="filters">
              <h1 className="title is-4 res-hide-title">Find a teacher</h1>
              <div className="container filter-box">
                <SkillsFilter />
              </div>
              <div className="container filter-box">
                <PricingFilter />
              </div>
              <div className="container filter-box">
                <ExpertiseFilter />
              </div>
              <div className="container filter-box">
                <TimeZoneFilter />
              </div>
              <div className="container filter-box">
                <LanguageFilter />
              </div>
            </div>
            <div className="column is-three-quarters results-desk">
              <div className="columns title-bar">
                <div className="column is-one-fifth number-of-results">
                  <h3 className="title is-6">100 teachers found</h3>
                </div>
                <div className="column is-three-fifths search-box-wrap">
                  <nav class="level">
                    <div class="level-left">
                      <div class="level-item">
                        <div class="field has-addons">
                          <p class="control is-inline-block search-box">
                            <input
                              class="input"
                              type="text"
                              placeholder="Find a teacher"
                            />
                          </p>
                          <p class="control is-inline-block">
                            <button class="button">Search</button>
                          </p>
                        </div>
                      </div>
                    </div>
                  </nav>
                </div>
                <div className="column is-one-fifth sort-wrap">
                  <div class="select">
                    <select>
                      <option>Relevant</option>
                      <option>Rating</option>
                      <option>Price</option>
                    </select>
                  </div>
                </div>
              </div>
              <div className="results-panel">
                {/* Listing : Start */}

                {this.state.isLoading ? (
                  <Spinner />
                ) : (
                  <MentorList mentors={this.state.mentors} />
                )}
                {/* Listing : End */}

                {/* Pagination : Start */}
                <nav
                  class="pagination"
                  role="navigation"
                  aria-label="pagination"
                >
                  <a
                    class="pagination-previous"
                    title="This is the first page"
                    disabled
                  >
                    Previous
                  </a>
                  <a class="pagination-next">Next page</a>
                  <ul class="pagination-list">
                    {this.state.pageNumbers.map(number => (
                      <li key={number}>
                        <a
                          onClick={this.paginate.bind(this, number)}
                          class="pagination-link is-current"
                          aria-label="Page 1"
                          aria-current="page"
                        >
                          {number}
                        </a>
                      </li>
                    ))}
                  </ul>
                </nav>
                {/* Pagination : End */}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
