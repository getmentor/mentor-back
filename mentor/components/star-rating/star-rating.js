import React, { Component } from "react";
import "./star-rating.scss";

export default class StarRating extends Component {
  constructor(props) {
    super(props);
    this.state = {
      oneStar: false,
      twoStars: false,
      threeStars: false,
      fourStars: false,
      fiveStars: false,
      ratingActiveOne: false,
      ratingActiveTwo: false,
      ratingActiveThree: false,
      ratingActiveFour: false,
      ratingActiveFive: false
    };
  }
  handleStarRating(selected) {
    if (selected === "oneStar")
      this.setState({
        oneStar: true,
        twoStars: false,
        threeStars: false,
        fourStars: false,
        fiveStars: false,
        ratingActiveOne: true,
        ratingActiveTwo: false,
        ratingActiveThree: false,
        ratingActiveFour: false,
        ratingActiveFive: false
      });
    else if (selected === "twoStars")
      this.setState({
        oneStar: true,
        twoStars: true,
        threeStars: false,
        fourStars: false,
        fiveStars: false,
        ratingActiveOne: true,
        ratingActiveTwo: true,
        ratingActiveThree: false,
        ratingActiveFour: false,
        ratingActiveFive: false
      });
    else if (selected === "threeStars")
      this.setState({
        oneStar: true,
        twoStars: true,
        threeStars: true,
        fourStars: false,
        fiveStars: false,
        ratingActiveOne: true,
        ratingActiveTwo: true,
        ratingActiveThree: true,
        ratingActiveFour: false,
        ratingActiveFive: false
      });
    else if (selected === "fourStars")
      this.setState({
        oneStar: true,
        twoStars: true,
        threeStars: true,
        fourStars: true,
        fiveStars: false,
        ratingActiveOne: true,
        ratingActiveTwo: true,
        ratingActiveThree: true,
        ratingActiveFour: true,
        ratingActiveFive: false
      });
    else if (selected === "fiveStars")
      this.setState({
        oneStar: true,
        twoStars: true,
        threeStars: true,
        fourStars: true,
        fiveStars: true,
        ratingActiveOne: true,
        ratingActiveTwo: true,
        ratingActiveThree: true,
        ratingActiveFour: true,
        ratingActiveFive: true
      });
  }
  render() {
    return (
      <>
        <div className="star-rating-wrap is-inline-block">
          <div className="control">
            <label className="radio">
              <input type="radio" name="rating" checked />
              <span
                className="checkmark"
                onClick={this.handleStarRating.bind(this, "oneStar")}
              >
                <img
                  src="../static/check.png"
                  className={this.state.ratingActiveOne ? "star" : "star show"}
                />
                <img
                  src="../static/checked.png"
                  className={this.state.oneStar ? "star show" : "star"}
                />
              </span>
            </label>
            <label className="radio">
              <input type="radio" name="rating" />
              <span
                className="checkmark"
                onClick={this.handleStarRating.bind(this, "twoStars")}
              >
                {" "}
                <img
                  src="../static/check.png"
                  className={this.state.ratingActiveTwo ? "star" : "star show"}
                />
                <img
                  src="../static/checked.png"
                  className={this.state.twoStars ? "star show" : "star"}
                />
              </span>
            </label>
            <label className="radio">
              <input type="radio" name="rating" />
              <span
                className="checkmark"
                onClick={this.handleStarRating.bind(this, "threeStars")}
              >
                {" "}
                <img
                  src="../static/check.png"
                  className={
                    this.state.ratingActiveThree ? "star" : "star show"
                  }
                />
                <img
                  src="../static/checked.png"
                  className={this.state.threeStars ? "star show" : "star"}
                />
              </span>
            </label>
            <label className="radio">
              <input type="radio" name="rating" />
              <span
                className="checkmark"
                onClick={this.handleStarRating.bind(this, "fourStars")}
              >
                {" "}
                <img
                  src="../static/check.png"
                  className={this.state.ratingActiveFour ? "star" : "star show"}
                />
                <img
                  src="../static/checked.png"
                  className={this.state.fourStars ? "star show" : "star"}
                />
              </span>
            </label>
            <label className="radio">
              <input type="radio" name="rating" />
              <span
                className="checkmark"
                onClick={this.handleStarRating.bind(this, "fiveStars")}
              >
                {" "}
                <img
                  src="../static/check.png"
                  className={this.state.ratingActiveFive ? "star" : "star show"}
                />
                <img
                  src="../static/checked.png"
                  className={this.state.fiveStars ? "star show" : "star"}
                />
              </span>
            </label>
          </div>
        </div>
      </>
    );
  }
}
