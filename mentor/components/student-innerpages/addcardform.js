import React, { Component } from "react";
import { CardElement, injectStripe } from "react-stripe-elements";
import counterpart from "counterpart";
import Translate from "react-translate-component";
var serialize = require("form-serialize");
import Router, { withRouter } from "next/router";
import env from "../../constant.json";

const createOptions = () => {
  return {
    style: {
      base: {
        fontSize: "16px",
        color: "#424770",
        fontFamily: "Open Sans, sans-serif",
        letterSpacing: "0.025em",
        "::placeholder": {
          color: "#aab7c4"
        }
      },
      invalid: {
        color: "#c23d4b"
      }
    }
  };
};

class CardForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errorMessage: "",
      showMessageModal: false,
      addcard_status: "",
      addcard_message: "",
      status_id: null,
      fname: null,
      lname: null,
      email: null
    };
    this.submit = this.submit.bind(this);
  }

  handleChange = ({ error }) => {
    if (error) {
      this.setState({ errorMessage: error.message });
    }
  };

  fetchCustomerData() {
    const userId = localStorage.getItem("userId");
    fetch(striperetrievecustomer, {
      //"http://localhost:5000/stripe/retrievecustomer"
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        userId: userId
      })
    })
      .then(response => {
        response.json().then(result => {
          console.log(response);
        });
      })
      .catch(err => {
        console.log("Error : " + err);
      });
  }

  componentDidMount() {
    this.fetchCustomerData();
    this.fetchUser();
  }
  fetchUser() {
    const userId = localStorage.getItem("userId");

    let requestBody = {
      query: `
        query userDetails($userId: String!) {
          userDetails(userId: $userId) {
            userId    
            name   
            last_name     
            email
            role
            main_desc
            short_desc
            profile_pic
            resume_link
            linkedin_url
            twitte_url
            medium_url
          }
        }
      `,
      variables: {
        userId: userId
      }
    };

    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(resData => {
        if (resData.data.userDetails) {
          this.setState({ fname: resData.data.userDetails.name });
          this.setState({ lname: resData.data.userDetails.last_name });
          this.setState({ email: resData.data.userDetails.email });

          window.scrollTo(0, 0);
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  async handleSubmit(evt) {
    evt.preventDefault();
    this.refs.btn.setAttribute("disabled", "disabled");

    if (this.props.stripe) {
      const userId = localStorage.getItem("userId");
      let { token } = await this.props.stripe.createToken({
        name: this.state.fname
      });

      fetch(env.stripecreatecard, {
        //"http://localhost:5000/stripe/createcard"
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          userId: userId,
          token: token.id
        })
      })
        .then(response => {
          response.json().then(result => {
            if (result.id) {
              this.props.view();
              this.setState({ showMessageModal: false });
              //   this.setState({ status_id: result.id });
              //   this.setState({ addcard_status: "Success" });
              //   this.setState({
              //     addcard_message: "Your card added successfully!"
              //   });
            } else {
              this.setState({ showMessageModal: true });
              this.setState({ status_id: null });
              this.setState({ addcard_status: "Warning" });
              this.setState({
                addcard_message: "Failed,Please provide valid datas!"
              });
            }
          });
        })
        .catch(err => {
          console.log("Error : " + err);
        });
    } else {
      console.log("Stripe.js hasn't loaded yet.");
    }
  }

  async submit(ev) {}

  render() {
    return (
      <>
        <div className="CardDemo">
          {this.state.showMessageModal && (
            <div className="error">
              {/* <span>{this.state.addcard_status}</span> */}
              <span>{this.state.addcard_message}</span>
            </div>
          )}
          <form
            id="CardForm"
            className="CardForm"
            onSubmit={this.handleSubmit.bind(this)}
          >
            <label>
              <h3>Card details</h3>
              <CardElement onChange={this.handleChange} {...createOptions()} />
            </label>
            <div className="error" role="alert">
              {this.state.errorMessage}
            </div>
            <button ref="btn">Add</button>
          </form>
        </div>
      </>
    );
  }
}

export default injectStripe(CardForm);
