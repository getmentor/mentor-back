import React, { Component } from "react";
import Link from "next/link";
import "./chat-list-mobile.scss";

export default class ChatListMobile extends Component {
  render() {
    return (
      <>
        <div className="container">
          <div className="chat-list-container">
            <div className="listing-header">
              <div className="search-wrap">
                <input type="text" placeholder="Search" />
                <img src="../static/search.png" />
              </div>
            </div>
            <div className="individual-list-wrap">
              <Link href="/mobile-chatbox-student">
                <div className="individual-list is-active">
                  <ul className="notifications-list is-inline-block">
                    <li>
                      <div className="header-notifications-trigger">
                        <div className="user-avatar">
                          <img src="../static/t_001.jpg" alt="user" />
                        </div>
                      </div>
                    </li>
                    <li>
                      <p className="title is-6">Tony Luke</p>
                      <p className="notification-msg">
                        Heyy You, Loremipsumlorem ipsum lorem ipsum
                      </p>
                      <p className="time">5 hours ago</p>
                    </li>
                  </ul>
                </div>
              </Link>
              <Link href="/mobile-chatbox-student">
                <div className="individual-list">
                  <ul className="notifications-list is-inline-block">
                    <li>
                      <div className="header-notifications-trigger">
                        <div className="user-avatar">
                          <img src="../static/t_002.jpg" alt="user" />
                        </div>
                      </div>
                    </li>
                    <li>
                      <p className="title is-6">Jonathan Gregory</p>
                      <p className="notification-msg">
                        Heyy You, Loremipsumlorem ipsum lorem ipsum
                      </p>
                      <p className="time">5 hours ago</p>
                    </li>
                  </ul>
                </div>
              </Link>
              <Link href="/mobile-chatbox-student">
                <div className="individual-list">
                  <ul className="notifications-list is-inline-block">
                    <li>
                      <div className="header-notifications-trigger">
                        <div className="user-avatar">
                          <img src="../static/t_003.jpg" alt="user" />
                        </div>
                      </div>
                    </li>
                    <li>
                      <p className="title is-6">Ayesha Ziya</p>
                      <p className="notification-msg">
                        Heyy You, Loremipsumlorem ipsum lorem ipsum
                      </p>
                      <p className="time">5 hours ago</p>
                    </li>
                  </ul>
                </div>
              </Link>
            </div>
          </div>
        </div>
      </>
    );
  }
}
