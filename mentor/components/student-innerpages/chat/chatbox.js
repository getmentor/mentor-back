import React, { Component } from "react";
import "./chatbox.scss";
import ChatHeader from "../../chatbox/chat-header";
import ChatWindow from "../../chatbox/chat-window";
import ChatFooter from "../../chatbox/chat-footer";

export default class ChatBox extends Component {
  render() {
    return (
      <>
        <div className="container">
          <div className="chat-container">
            <ChatHeader />
            <ChatWindow />
            <ChatFooter />
          </div>
        </div>
      </>
    );
  }
}
