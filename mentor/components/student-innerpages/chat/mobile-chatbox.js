import React, { Component } from "react";
import Link from "next/link";
import "./mobile-chatbox.scss";
import ChatBoxMobile from "../chat/chatbox-mobile";

export default class MobileChatBoxStd extends Component {
  render() {
    return (
      <>
        <div className="container layout-container">
          <div className="chat-details-wrap is-inline-block">
            <div className="chat-details is-inline-block">
              <Link href="/student-chatbox">
                <a className="button is-primary back-to-chatlist">
                  <span>Back to chat list</span>
                </a>
              </Link>
              <ChatBoxMobile />
            </div>
          </div>
        </div>
      </>
    );
  }
}
