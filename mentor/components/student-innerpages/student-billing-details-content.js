import React, { Component } from "react";
import "./student-billing-details-content.scss";
import Footer from "../footer/footer";
import counterpart from "counterpart";
import Translate from "react-translate-component";
import { Elements, StripeProvider } from "react-stripe-elements";
import CardForm from "./addcardform";
import Spinner from "../../components/Spinner/Spinner";
import env from "../../constant.json";

export default class StudentBillingDetailsContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showEditCardDetailsModal: false,
      showAddCardDetailsModal: false,
      retrievecard: [],
      cardholdername: null,
      exp_month_edit: null,
      exp_year_edit: null,
      edit_id: null,
      stripe: null,
      isLoading: false
    };

    this.cardholdername = React.createRef();
    this.exp_month_edit = React.createRef();
    this.exp_year_edit = React.createRef();
  }
  isActive = true;
  componentDidMount() {
    this.retrieveCardDetails();

    if (window.Stripe) {
      this.setState({
        stripe: window.Stripe("pk_test_ol8wRXrC0WE66EfzZWmRXDeF00684gGC7P")
      });
    } else {
      document.querySelector("#stripe-js").addEventListener("load", () => {
        // Create Stripe instance once Stripe.js loads
        this.setState({
          stripe: window.Stripe("pk_test_ol8wRXrC0WE66EfzZWmRXDeF00684gGC7P")
        });
      });
    }
  }

  handleEditCardDetailsModal = modalState => {
    if (modalState === "modalOpen")
      this.setState({ showEditCardDetailsModal: true });
    else this.setState({ showEditCardDetailsModal: false });
  };
  handleAddCardDetailsModal = modalState => {
    if (modalState === "modalOpen")
      this.setState({ showAddCardDetailsModal: true });
    else this.setState({ showAddCardDetailsModal: false });
  };

  deletecard = id => {
    const userId = localStorage.getItem("userId");
    const card_id = id;

    fetch(env.stripedeletecard, {
      //"http://localhost:5000/stripe/deletecard"
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        userId: userId,
        id: card_id
      })
    })
      .then(response => {
        this.retrieveCardDetails();
      })
      .catch(err => {
        console.log("Error : " + err);
      });
  };

  editCardDetails = id => {
    const userId = localStorage.getItem("userId");
    const card_id = id;
    const name = this.cardholdername.current.value;
    const exp_month = this.exp_month_edit.current.value;
    const exp_year = this.exp_year_edit.current.value;

    fetch(env.stripeupdatecard, {
      //"http://localhost:5000/stripe/updatecard"
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        userId: userId,
        id: card_id,
        name: name,
        exp_month: exp_month,
        exp_year: exp_year
      })
    })
      .then(response => {
        this.retrieveCardDetails();
        this.setState({ showEditCardDetailsModal: false });
      })
      .catch(err => {
        console.log("Error : " + err);
      });
  };

  editCardDetailsView = id => {
    const userId = localStorage.getItem("userId");
    const card_id = id;
    // return;

    fetch(env.striperetrievesinglecard, {
      //"http://localhost:5000/stripe/retrievesinglecard"
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        userId: userId,
        id: card_id
      })
    })
      .then(response => {
        response.json().then(result => {
          console.log(result.id);
          this.setState({ cardholdername: result.name });
          this.setState({ exp_month_edit: result.exp_month });
          this.setState({ exp_year_edit: result.exp_year });
          this.setState({ edit_id: result.id });
          this.setState({ showEditCardDetailsModal: true });
        });
      })
      .catch(err => {
        console.log("Error : " + err);
      });
  };

  changeView() {
    this.setState({
      showAddCardDetailsModal: false
    });
    this.retrieveCardDetails();
  }

  retrieveCardDetails() {
    this.setState({ isLoading: true });
    const userId = localStorage.getItem("userId");

    fetch(striperetrievecard, {
      //"http://localhost:5000/stripe/retrievecard"
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        userId: userId
      })
    })
      .then(response => {
        response.json().then(result => {
          if (this.isActive) {
            this.setState({ retrievecard: result.data, isLoading: false });
          }
        });
      })
      .catch(err => {
        console.log("Error : " + err);
        if (this.isActive) {
          this.setState({ isLoading: false });
        }
      });
  }

  render() {
    return (
      <>
        <div className="student-billing-details-container">
          <div className="dashboard-inner">
            <div className="content-box is-inline-block">
              {/* Saved card details : Start */}
              <Translate
                content="sbdheadings.hd1"
                component="h1"
                className="title is-4"
                unsafe={true}
              />
              {/* <h1 className="title is-4">Saved card details</h1> */}
              {this.state.isLoading && <Spinner />}
              {this.state.retrievecard.length <= 0 && (
                <div className="empty-data">
                  <a
                    className="edit-card-details"
                    onClick={this.handleAddCardDetailsModal.bind(
                      this,
                      "modalOpen"
                    )}
                  >
                    <span className="label">Add card details</span>
                  </a>
                </div>
              )}
              {this.state.retrievecard.map(
                function(item) {
                  return (
                    <div>
                      <div className="columns">
                        <div className="column">
                          <div className="titl is-block">
                            <div className="titl is-inline-block">
                              <Translate
                                content="sbdspan.s1"
                                component="span"
                                className="label"
                                unsafe={true}
                              />
                              {/* <span className="label">Card holder's name</span> */}
                            </div>
                            <div className="des is-inline-block">
                              <span className="">{item.name}</span>
                            </div>
                          </div>
                        </div>
                        <div className="column">
                          <div className="titl is-block">
                            <div className="titl is-inline-block">
                              <Translate
                                content="sbdspan.s2"
                                component="span"
                                className="label"
                                unsafe={true}
                              />
                              {/* <span className="label">Card number</span> */}
                            </div>
                            <div className="des is-inline-block">
                              <span className="">xxxxxxxxxxxx{item.last4}</span>
                            </div>
                          </div>
                        </div>
                        <div className="column">
                          <div className="titl is-block">
                            <div className="titl is-inline-block">
                              <Translate
                                content="sbdspan.s3"
                                component="span"
                                className="label"
                                unsafe={true}
                              />
                              {/* <span className="label">Expiry date</span> */}
                            </div>
                            <div className="des is-inline-block">
                              <div className="expiry-wrap is-inline-block">
                                <div className="">
                                  <span className="">
                                    {item.exp_month}/{item.exp_year}
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="column column-edit">
                          <a
                            className="edit-card-details"
                            onClick={this.editCardDetailsView.bind(
                              this,
                              item.id
                            )}
                          >
                            <img src="../static/edit.png" alt="edit-icon" />
                          </a>
                          <a
                            className="edit-card-details"
                            onClick={this.deletecard.bind(this, item.id)}
                          >
                            <img src="../static/delete.png" alt="delete-icon" />
                          </a>
                        </div>
                      </div>
                    </div>
                  );
                }.bind(this)
              )}

              {/* Saved card details : End */}
              <Translate
                content="sbdheadings.hd2"
                component="h2"
                className="title is-4 billing-title is-inline-block"
                unsafe={true}
              />
              <div className="billing-history-wrap">
                <table className="table is-bordered is-fullwidth">
                  <thead>
                    <tr>
                      <th>
                        <Translate
                          content="sbdspan.s5"
                          component="span"
                          className="label"
                          unsafe={true}
                        />
                        {/* <span className="label">Session with</span> */}
                      </th>
                      <th>
                        <Translate
                          content="sbdspan.s6"
                          component="span"
                          className="label"
                          unsafe={true}
                        />
                        {/* <span className="label">Pricing</span> */}
                      </th>
                      <th>
                        <Translate
                          content="sbdspan.s7"
                          component="span"
                          className="label"
                          unsafe={true}
                        />
                        {/* <span className="label">Session duration</span> */}
                      </th>
                      <th>
                        <Translate
                          content="sbdspan.s8"
                          component="span"
                          className="label"
                          unsafe={true}
                        />
                        {/* <span className="label">Amount paid</span> */}
                      </th>
                      <th>
                        <Translate
                          content="sbdspan.s9"
                          component="span"
                          className="label"
                          unsafe={true}
                        />
                        {/* <span className="label">Balance amount</span> */}
                      </th>
                    </tr>
                  </thead>

                  <tbody>
                    <tr>
                      <td>
                        <span className="subtitle is-6">
                          <span className="teacher-name">Tony luke</span>
                        </span>
                      </td>
                      <td>
                        <span className="subtitle is-6">
                          <span className="subtitle is-6">
                            <span className="pricing">30$/20min</span>
                          </span>
                        </span>
                      </td>
                      <td>
                        <span className="subtitle is-6">
                          <span className="session-duration">19</span>min
                        </span>
                      </td>
                      <td>
                        <span className="subtitle is-6">
                          <span className="amount-paid">30</span>$
                        </span>
                      </td>
                      <td>
                        <span className="subtitle is-6">
                          <span className="balance-amount">0</span>$
                        </span>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <span className="subtitle is-6">
                          <span className="teacher-name">Jonathan Gregory</span>
                        </span>
                      </td>
                      <td>
                        <span className="subtitle is-6">
                          <span className="subtitle is-6">
                            <span className="pricing">30$/20min</span>
                          </span>
                        </span>
                      </td>
                      <td>
                        <span className="subtitle is-6">
                          <span className="session-duration">20</span>min
                        </span>
                      </td>
                      <td>
                        <span className="subtitle is-6">
                          <span className="amount-paid">30</span>$
                        </span>
                      </td>
                      <td>
                        <span className="subtitle is-6">
                          <span className="balance-amount">30</span>$
                        </span>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <Footer />
          </div>
        </div>

        <div
          className={
            this.state.showAddCardDetailsModal
              ? "modal edit-add-modal edit-card-details-modal is-active"
              : "modal edit-add-modal edit-card-details-modal"
          }
        >
          <div className="modal-background">
            <div className="modal-content">
              <p className="title is-4 card-head">Add card details</p>
              <div className="payment-method-wrap is-inline-block">
                <StripeProvider
                  stripe={this.state.stripe}
                  name={this.state.fname}
                >
                  <Elements>
                    <CardForm
                      handleResult={this.props.handleResult}
                      view={this.changeView.bind(this)}
                    />
                  </Elements>
                </StripeProvider>
              </div>
            </div>

            <button
              className="modal-close is-large"
              aria-label="close"
              onClick={this.handleAddCardDetailsModal.bind(this, "modalClose")}
            />
          </div>
        </div>

        <div
          className={
            this.state.showEditCardDetailsModal
              ? "modal edit-add-modal edit-card-details-modal is-active"
              : "modal edit-add-modal edit-card-details-modal"
          }
        >
          <div className="modal-background">
            <div className="modal-content">
              <p className="title is-4 card-head">Edit card details</p>

              <div className="payment-method-wrap is-inline-block">
                <ul className="card-details-list is-inline-block">
                  <li className="is-inline-block">
                    <label className="label">Card holder's name</label>
                    <input
                      className="input"
                      type="text"
                      placeholder="Card holder's name"
                      defaultValue={this.state.cardholdername}
                      ref={this.cardholdername}
                    />
                  </li>
                  <li className="is-inline-block">
                    <label className="label">Expiry date</label>
                    <div className="exp_input">
                      <input
                        className="input"
                        type="text"
                        placeholder="MM"
                        defaultValue={this.state.exp_month_edit}
                        ref={this.exp_month_edit}
                      />
                    </div>
                    <div className="exp_input">
                      <input
                        className="input"
                        type="text"
                        placeholder="YY"
                        defaultValue={this.state.exp_year_edit}
                        ref={this.exp_year_edit}
                      />
                    </div>
                  </li>
                  <li>
                    <a
                      className="button is-primary add-card is-block"
                      onClick={this.editCardDetails.bind(
                        this,
                        this.state.edit_id
                      )}
                      // onClick={this.handleEditCardDetailsModal.bind(
                      //   this,
                      //   "modalClose"
                      // )}
                    >
                      Save changes
                    </a>
                  </li>
                </ul>
              </div>
            </div>

            <button
              className="modal-close is-large"
              aria-label="close"
              onClick={this.handleEditCardDetailsModal.bind(this, "modalClose")}
            />
          </div>
        </div>
      </>
    );
  }
}
