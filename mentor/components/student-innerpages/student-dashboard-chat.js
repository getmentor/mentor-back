import React, { Component } from "react";
import "./student-dashboard-chat.scss";
import Footer from "../footer/footer";
import ChatBox from "../student-innerpages/chat/chatbox";
import ChatList from "../student-innerpages/chat/chat-list";
import ChatListMobile from "../student-innerpages/chat/chat-list-mobile";
import counterpart from "counterpart";
import Translate from "react-translate-component";

export default class StudentDashboardChat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ShowSuggestedSessionModal: false
    };
  }
  handleSessionSuggestedModal = modalState => {
    if (modalState === "modalOpen")
      this.setState({ ShowSuggestedSessionModal: true });
    else this.setState({ ShowSuggestedSessionModal: false });
  };

  render() {
    return (
      <>
        <div className="dashboard-container">
          <div className="dashboard-inner">
            <div className="content-box is-inline-block">
              <div className="chat-window-wrap is-inline-block">
                <div className="chat-list">
                  <ChatList />
                </div>
                <div className="chat-details">
                  <ChatBox />
                </div>
              </div>
              <div className="Mobile-chat-area is-inline-block">
                <ChatListMobile />
              </div>
            </div>
            <Footer />
          </div>
        </div>

        <div
          className={
            this.state.ShowSuggestedSessionModal
              ? "modal suggested-a-session-modal is-active"
              : "modal suggested-a-session-modal"
          }
        >
          <div className="modal-background">
            <div className="modal-content">
              {" "}
              <p className="title is-4 head">
                Teacher has suggested a session, click here to start
              </p>
              <a
                className="button is-primary"
                onClick={this.handleSessionSuggestedModal.bind(
                  this,
                  "modalClose"
                )}
              >
                Start session
              </a>
            </div>

            <button
              className="modal-close is-large"
              aria-label="close"
              onClick={this.handleSessionSuggestedModal.bind(
                this,
                "modalClose"
              )}
            />
          </div>
        </div>
      </>
    );
  }
}
