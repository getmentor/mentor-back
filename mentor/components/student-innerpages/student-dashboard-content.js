import React, { Component } from "react";
import "./student-dashboard-content.scss";
import Footer from "../footer/footer";
import counterpart from "counterpart";
import Translate from "react-translate-component";
export default class StudentDashboardContent extends Component {
  render() {
    return (
      <>
        <div className="student-dashboard-container">
          <div className="dashboard-inner">
            <div className="content-box is-inline-block">
              <div className="columns">
                <div className="column">
                  <div className="inner-blk-wrap is-inline-block">
                    <div className="sec1 is-inline-block">
                      <Translate
                        content="sdbpara.p1"
                        component="p"
                        className="category"
                        unsafe={true}
                      />
                      {/* <p className="category">Profile views</p> */}

                      <p className="count title">10</p>
                    </div>
                    <div className="sec2">
                      <span>
                        <img src="../static/clipboard-icon.png" alt="icon" />
                      </span>
                    </div>
                  </div>
                </div>
                <div className="column">
                  <div className="inner-blk-wrap is-inline-block">
                    <div className="sec1 is-inline-block">
                      <Translate
                        content="sdbpara.p2"
                        component="p"
                        className="category"
                        unsafe={true}
                      />
                      {/* <p className="category">Requests</p> */}
                      <p className="count title">23</p>
                    </div>
                    <div className="sec2">
                      <span>
                        <img src="../static/clipboard-icon.png" alt="icon" />
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="columns">
                <div className="column">
                  <div className="inner-blk-wrap is-inline-block">
                    <div className="sec1 is-inline-block">
                      <Translate
                        content="sdbpara.p3"
                        component="p"
                        className="category"
                        unsafe={true}
                      />
                      {/* <p className="category">Sessions handled</p> */}
                      <p className="count title">49</p>
                    </div>
                    <div className="sec2">
                      <span>
                        <img src="../static/clipboard-icon.png" alt="icon" />
                      </span>
                    </div>
                  </div>
                </div>
                <div className="column">
                  <div className="inner-blk-wrap is-inline-block">
                    <div className="sec1 is-inline-block">
                      <Translate
                        content="sdbpara.p4"
                        component="p"
                        className="category"
                        unsafe={true}
                      />
                      {/* <p className="category">Reviews</p> */}
                      <p className="count title">98</p>
                    </div>
                    <div className="sec2">
                      <span>
                        <img src="../static/clipboard-icon.png" alt="icon" />
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <Footer />
          </div>
        </div>
      </>
    );
  }
}
