import React, { Component } from "react";
import "./student-profile-edit.scss";
import Footer from "../footer/footer";
import counterpart from "counterpart";
import Translate from "react-translate-component";
export default class StudentProfileEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userIsOnline: true,
      userIsOffline: false,
      teacherAccount: true,
      studentAccount: false,
      showEditExpertiseModal: false,
      showAddExpertiseModal: false,
      showAddEducationModal: false,
      showEditEducationModal: false,
      showAddEducationStudentModal: false,
      showEditEducationStudentModal: false,
      showEditExperienceModal: false,
      showAddExperienceModal: false,
      showAddSkillsModal: false,
      showAddLanguagesModal: false
    };
  }
  handleAddEducationStudentModal = modalState => {
    if (modalState === "modalOpen")
      this.setState({ showAddEducationStudentModal: true });
    else this.setState({ showAddEducationStudentModal: false });
  };
  handleEditEducationStudentModal = modalState => {
    if (modalState === "modalOpen")
      this.setState({ showEditEducationStudentModal: true });
    else this.setState({ showEditEducationStudentModal: false });
  };
  render() {
    return (
      <>
        <div className="student-prof-edit-container">
          <div className="dashboard-inner">
            <div className="content-box is-inline-block">
              <Translate
                content="speheadings.hd1"
                component="h1"
                className="title is-5 pg-heading"
                unsafe={true}
              />
              {/* <h1 className="title is-5 pg-heading">Edit Profile</h1> */}
              <div className="profile-edit-wrap section is-inline-block">
                <div className="left-blk">
                  <div className="avatar-wrap">
                    <div className="file is-boxed avatar-wrap-inner">
                      <label className="file-label">
                        <input
                          className="file-input"
                          type="file"
                          name="resume"
                        />
                        <span className="file-cta">
                          <img src="../static/p_002.jpg" alt="user-picture" />
                        </span>
                      </label>
                    </div>
                    <div className="avatar-wrap-inner" />
                  </div>
                  <Translate
                    content="spepara.p1"
                    component="p"
                    className="change-profile-pic-title"
                    unsafe={true}
                  />
                  {/* <p className="change-profile-pic-title">
                    Change profile picture
                  </p> */}
                </div>
                <div className="right-blk">
                  <div className="field-wrap">
                    <div className="field">
                      <Translate
                        content="spelabel.l1"
                        component="label"
                        className="label"
                        unsafe={true}
                      />
                      {/* <label className="label">First Name</label> */}
                      <div className="control">
                        <input
                          className="input"
                          type="text"
                          placeholder="First Name"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="field-wrap">
                    <div className="field">
                      <Translate
                        content="spelabel.l2"
                        component="label"
                        className="label"
                        unsafe={true}
                      />
                      {/* <label className="label">Last Name</label> */}
                      <div className="control">
                        <input
                          className="input"
                          type="text"
                          placeholder="Last Name"
                        />
                      </div>
                    </div>
                  </div>

                  <div className="field-wrap">
                    <div className="field">
                      <Translate
                        content="spelabel.l3"
                        component="label"
                        className="label"
                        unsafe={true}
                      />
                      {/* <label className="label">Email address</label> */}
                      <div classnName="control">
                        <input
                          className="input"
                          type="text"
                          placeholder="Email address"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="short-description-wrap is-inline-block">
                    <Translate
                      content="spelabel.l4"
                      component="label"
                      className="label"
                      unsafe={true}
                    />
                    {/* <label className="label">Short description</label> */}
                    <div className="control">
                      <textarea
                        className="textarea has-fixed-size"
                        placeholder="Fixed size textarea"
                      >
                        Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry. Lorem Ipsum has been the
                        industry's standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and
                        scrambled it to make a type specimen book. It has
                        survived not only five centuries, but also the leap into
                        electronic typesetting, remaining essentially unchanged.
                      </textarea>
                    </div>
                  </div>
                  <div className="education-wrap is-inline-block">
                    <Translate
                      content="spelabel.l5"
                      component="label"
                      className="label headings edu"
                      unsafe={true}
                    />
                    {/* <label className="label headings edu">Education</label> */}
                    <div className="education-list is-inline-block">
                      <div className="expert-row is-inline-block">
                        <div className="sec1 is-inline-block">
                          <Translate
                            content="spespan.s5"
                            component="label"
                            className="entry label"
                            unsafe={true}
                          />
                          {/* <span className="entry label">School</span> */}
                        </div>
                        <div className="sec2 is-inline-block">
                          <span className="entry">
                            St.Theresa's High school
                          </span>

                          <span className="from-to is-inline-block">
                            ( <span className="from">2005</span> -{" "}
                            <span className="to">2007</span> )
                          </span>
                        </div>
                        <div className="sec3">
                          <div
                            className="edit-delete-wrap is-inline-block"
                            onClick={this.handleEditEducationStudentModal.bind(
                              this,
                              "modalOpen"
                            )}
                          >
                            <img src="../static/edit.png" alt="delete" />
                          </div>

                          <div className="edit-delete-wrap is-inline-block">
                            <img src="../static/delete.png" alt="delete" />
                          </div>
                        </div>
                        <div className="sec1 is-inline-block">
                          <Translate
                            content="spespan.s1"
                            component="span"
                            className="entry label"
                            unsafe={true}
                          />
                          {/* <span className="entry label">Field of study</span> */}
                        </div>
                        <div className="sec2 is-inline-block">
                          <span className="entry" />
                        </div>
                      </div>

                      <div className="expert-row is-inline-block">
                        <div className="sec1 is-inline-block">
                          <Translate
                            content="spespan.s6"
                            component="span"
                            className="entry label"
                            unsafe={true}
                          />
                          {/* <span className="entry label">Degree</span> */}
                        </div>
                        <div className="sec2 is-inline-block">
                          <span className="entry">
                            Albertian College of science and technology
                          </span>

                          <span className="from-to is-inline-block">
                            ( <span className="from">2007</span> -{" "}
                            <span className="to">2011</span> )
                          </span>
                        </div>
                        <div className="sec3">
                          <div
                            className="edit-delete-wrap is-inline-block"
                            onClick={this.handleEditEducationStudentModal.bind(
                              this,
                              "modalOpen"
                            )}
                          >
                            <img src="../static/edit.png" alt="delete" />
                          </div>
                          <div className="edit-delete-wrap is-inline-block">
                            <img src="../static/delete.png" alt="delete" />
                          </div>
                        </div>

                        <div className="sec1 is-inline-block">
                          <Translate
                            content="spespan.s1"
                            component="span"
                            className="entry label"
                            unsafe={true}
                          />
                          {/* <span className="entry label">Field of study</span> */}
                        </div>
                        <div className="sec2 is-inline-block">
                          <span className="entry">Computer science</span>
                        </div>
                      </div>

                      <a
                        className="button is-primary add-new"
                        onClick={this.handleAddEducationStudentModal.bind(
                          this,
                          "modalOpen"
                        )}
                      >
                        <Translate
                          content="spespan.s2"
                          component="span"
                          unsafe={true}
                        />
                        {/* <span>Add Education</span> */}
                      </a>
                    </div>
                  </div>
                  <a class="button is-link is-block btn-submit">
                    <Translate
                      content="spespan.s3"
                      component="span"
                      unsafe={true}
                    />
                    {/* <span>Submit</span> */}
                  </a>
                </div>
              </div>
            </div>
            <Footer />
          </div>
        </div>

        <div
          className={
            this.state.showAddEducationStudentModal
              ? "modal edit-add-modal add-education-student-modal is-active"
              : "modal edit-add-modal add-education-student-modal"
          }
        >
          <div className="modal-background">
            <div className="modal-content">
              <Translate
                content="spepara.p2"
                component="p"
                unsafe={true}
                className="title is-4"
              />
              {/* <p className="title is-4">Add education</p> */}
              <div className="add-edit-wrap is-inline-block">
                {" "}
                <Translate
                  content="spespan.s4"
                  component="label"
                  unsafe={true}
                  className="label"
                />
                {/* <label className="label">Select qualification</label> */}
                <div className="select">
                  <select>
                    <Translate
                      content="speoption.o1"
                      component="option"
                      unsafe={true}
                    />
                    {/* <option>School</option> */}
                    <Translate
                      content="speoption.o2"
                      component="option"
                      unsafe={true}
                    />
                    {/* <option>Degree</option> */}
                    <Translate
                      content="speoption.o3"
                      component="option"
                      unsafe={true}
                    />
                    {/* <option>Post graduation</option> */}
                  </select>
                </div>
                <div className="field-wrap">
                  <div className="field">
                    <label className="label">Field of study</label>
                    <div className="control is-inline-block">
                      <input
                        className="input"
                        type="text"
                        placeholder="Field of study"
                      />
                    </div>
                  </div>
                </div>
                <div className="pick-date-wrap columns">
                  <div className="column">
                    <label class="label">From</label>
                    <div className="field month-selector">
                      <div className="control">
                        <div className="select">
                          <select>
                            <option>Month</option>
                            <option>January</option>
                            <option>February</option>
                            <option>March</option>
                            <option>April</option>
                            <option>May</option>
                            <option>June</option>
                            <option>July</option>
                            <option>August</option>
                            <option>Sepetember</option>
                            <option>October</option>
                            <option>November</option>
                            <option>December</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="field year-selector">
                      <div className="control">
                        <div className="select">
                          <select>
                            <option>Year</option>
                            <option>2019</option>
                            <option>2018</option>
                            <option>2017</option>
                            <option>2016</option>
                            <option>2015</option>
                            <option>2014</option>
                            <option>2013</option>
                            <option>2012</option>
                            <option>2011</option>
                            <option>2010</option>
                            <option>2009</option>
                            <option>2008</option>
                            <option>2007</option>
                            <option>2006</option>
                            <option>2005</option>
                            <option>2004</option>
                            <option>2003</option>
                            <option>2002</option>
                            <option>2001</option>
                            <option>2000</option>
                            <option>1999</option>
                            <option>1998</option>
                            <option>1997</option>
                            <option>1996</option>
                            <option>1995</option>
                            <option>1994</option>
                            <option>1993</option>
                            <option>1992</option>
                            <option>1991</option>
                            <option>1990</option>
                            <option>1989</option>
                            <option>1988</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="column">
                    <label class="label">To</label>
                    <div className="field month-selector">
                      <div className="control">
                        <div className="select">
                          <select>
                            <option>Month</option>
                            <option>January</option>
                            <option>February</option>
                            <option>March</option>
                            <option>April</option>
                            <option>May</option>
                            <option>June</option>
                            <option>July</option>
                            <option>August</option>
                            <option>Sepetember</option>
                            <option>October</option>
                            <option>November</option>
                            <option>December</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="field year-selector">
                      <div className="control">
                        <div className="select">
                          <select>
                            <option>Year</option>
                            <option>2019</option>
                            <option>2018</option>
                            <option>2017</option>
                            <option>2016</option>
                            <option>2015</option>
                            <option>2014</option>
                            <option>2013</option>
                            <option>2012</option>
                            <option>2011</option>
                            <option>2010</option>
                            <option>2009</option>
                            <option>2008</option>
                            <option>2007</option>
                            <option>2006</option>
                            <option>2005</option>
                            <option>2004</option>
                            <option>2003</option>
                            <option>2002</option>
                            <option>2001</option>
                            <option>2000</option>
                            <option>1999</option>
                            <option>1998</option>
                            <option>1997</option>
                            <option>1996</option>
                            <option>1995</option>
                            <option>1994</option>
                            <option>1993</option>
                            <option>1992</option>
                            <option>1991</option>
                            <option>1990</option>
                            <option>1989</option>
                            <option>1988</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <a
                  className="button is-primary"
                  onClick={this.handleAddEducationStudentModal.bind(
                    this,
                    "modalClose"
                  )}
                >
                  Add
                </a>
              </div>
            </div>
            <button
              className="modal-close is-large"
              aria-label="close"
              onClick={this.handleAddEducationStudentModal.bind(
                this,
                "modalClose"
              )}
            />
          </div>
        </div>

        <div
          className={
            this.state.showEditEducationStudentModal
              ? "modal edit-add-modal edit-education-student-modal is-active"
              : "modal edit-add-modal edit-education-student-modal"
          }
        >
          <div className="modal-background">
            <div className="modal-content">
              <p className="title is-4">Edit education</p>
              <div className="add-edit-wrap is-inline-block">
                {" "}
                <label className="label">Select qualification</label>
                <div className="select">
                  <select>
                    <option>School</option>
                    <option>Degree</option>
                    <option>Post graduation</option>
                  </select>
                </div>
                <div className="field-wrap">
                  <div className="field">
                    <label className="label">Field of study</label>
                    <div className="control is-inline-block">
                      <input
                        className="input"
                        type="text"
                        placeholder="Field of study"
                      />
                    </div>
                  </div>
                </div>
                <div className="pick-date-wrap columns">
                  <div className="column">
                    <label class="label">From</label>
                    <div className="field month-selector">
                      <div className="control">
                        <div className="select">
                          <select>
                            <option>Month</option>
                            <option>January</option>
                            <option>February</option>
                            <option>March</option>
                            <option>April</option>
                            <option>May</option>
                            <option>June</option>
                            <option>July</option>
                            <option>August</option>
                            <option>Sepetember</option>
                            <option>October</option>
                            <option>November</option>
                            <option>December</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="field year-selector">
                      <div className="control">
                        <div className="select">
                          <select>
                            <option>Year</option>
                            <option>2019</option>
                            <option>2018</option>
                            <option>2017</option>
                            <option>2016</option>
                            <option>2015</option>
                            <option>2014</option>
                            <option>2013</option>
                            <option>2012</option>
                            <option>2011</option>
                            <option>2010</option>
                            <option>2009</option>
                            <option>2008</option>
                            <option>2007</option>
                            <option>2006</option>
                            <option>2005</option>
                            <option>2004</option>
                            <option>2003</option>
                            <option>2002</option>
                            <option>2001</option>
                            <option>2000</option>
                            <option>1999</option>
                            <option>1998</option>
                            <option>1997</option>
                            <option>1996</option>
                            <option>1995</option>
                            <option>1994</option>
                            <option>1993</option>
                            <option>1992</option>
                            <option>1991</option>
                            <option>1990</option>
                            <option>1989</option>
                            <option>1988</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="column">
                    <label class="label">To</label>
                    <div className="field month-selector">
                      <div className="control">
                        <div className="select">
                          <select>
                            <option>Month</option>
                            <option>January</option>
                            <option>February</option>
                            <option>March</option>
                            <option>April</option>
                            <option>May</option>
                            <option>June</option>
                            <option>July</option>
                            <option>August</option>
                            <option>Sepetember</option>
                            <option>October</option>
                            <option>November</option>
                            <option>December</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="field year-selector">
                      <div className="control">
                        <div className="select">
                          <select>
                            <option>Year</option>
                            <option>2019</option>
                            <option>2018</option>
                            <option>2017</option>
                            <option>2016</option>
                            <option>2015</option>
                            <option>2014</option>
                            <option>2013</option>
                            <option>2012</option>
                            <option>2011</option>
                            <option>2010</option>
                            <option>2009</option>
                            <option>2008</option>
                            <option>2007</option>
                            <option>2006</option>
                            <option>2005</option>
                            <option>2004</option>
                            <option>2003</option>
                            <option>2002</option>
                            <option>2001</option>
                            <option>2000</option>
                            <option>1999</option>
                            <option>1998</option>
                            <option>1997</option>
                            <option>1996</option>
                            <option>1995</option>
                            <option>1994</option>
                            <option>1993</option>
                            <option>1992</option>
                            <option>1991</option>
                            <option>1990</option>
                            <option>1989</option>
                            <option>1988</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <a
                  className="button is-primary"
                  onClick={this.handleEditEducationStudentModal.bind(
                    this,
                    "modalClose"
                  )}
                >
                  Update
                </a>
              </div>
            </div>
            <button
              className="modal-close is-large"
              aria-label="close"
              onClick={this.handleEditEducationStudentModal.bind(
                this,
                "modalClose"
              )}
            />
          </div>
        </div>
      </>
    );
  }
}
