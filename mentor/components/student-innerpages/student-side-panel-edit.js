import React, { Component } from "react";
import Link from "next/link";
import "./student-side-panel-edit.scss";
import counterpart from "counterpart";
import Translate from "react-translate-component";

export default class StudentSidePanelEdit extends Component {
  handleDashboardMenu = () => {
    document
      .getElementById("side-panel-student-edit")
      .classList.toggle("is-active");
  };
  render() {
    return (
      <>
        <div className="student-dashboard-navigation">
          <div
            className="hamburger-wrap is-inline-block"
            onClick={this.handleDashboardMenu}
          >
            <span className="is-inline-block" />
            <span className="is-inline-block" />
            <span className="is-inline-block" />
          </div>
          <span className="dashboard-title">Dashboard Navigation</span>
        </div>
        <div
          className="student-side-panel-edit-wrap"
          id="side-panel-student-edit"
        >
          <ul className="side-panel-menu">
            <li>
              <Link href="/student-dashboard">
                <Translate
                  content="ssplink.a1"
                  component="a"
                  className="label is-inline-block headings"
                  unsafe={true}
                />
                {/* <a>Dashboard</a> */}
              </Link>
            </li>
            <li className="is-active">
              <Link href="/student-profile-edit">
                <Translate
                  content="ssplink.a2"
                  component="a"
                  className="label is-inline-block headings"
                  unsafe={true}
                />
                {/* <a>Profile Edit</a> */}
              </Link>
            </li>
            <li>
              <Link>
                <Translate
                  content="ssplink.a3"
                  component="a"
                  className="label is-inline-block headings"
                  unsafe={true}
                />
                {/* <a>Chat/Messages</a> */}
              </Link>
            </li>
            <li>
              <Link href="/student-billing-details">
                <Translate
                  content="ssplink.a4"
                  component="a"
                  className="label is-inline-block headings"
                  unsafe={true}
                />
                {/* <a>Rates and schedule settings</a> */}
              </Link>
            </li>
            <li>
              <Link href="/">
                <Translate
                  content="ssplink.a5"
                  component="a"
                  className="label is-inline-block headings"
                  unsafe={true}
                />
                {/* <a>Logout</a> */}
              </Link>
            </li>
          </ul>
        </div>
      </>
    );
  }
}
