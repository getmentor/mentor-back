import React, { Component } from "react";
import { Elements, StripeProvider } from "react-stripe-elements";
import CardForm from "./cardform";
import "./add-cards.scss";
import counterpart from "counterpart";
import Translate from "react-translate-component";

class AddCardsComp extends Component {
  constructor() {
    super();
    this.state = { stripe: null };
  }
  componentDidMount() {
    if (window.Stripe) {
      this.setState({
        stripe: window.Stripe("pk_test_ol8wRXrC0WE66EfzZWmRXDeF00684gGC7P")
      });
    } else {
      document.querySelector("#stripe-js").addEventListener("load", () => {
        // Create Stripe instance once Stripe.js loads
        this.setState({
          stripe: window.Stripe("pk_test_ol8wRXrC0WE66EfzZWmRXDeF00684gGC7P")
        });
      });
    }
  }
  render() {
    return (
      <div className="add-cards-wrap">
        <Translate
          content="afheadings.hd4"
          component="h1"
          className="title is-5"
          unsafe={true}
        />
        <StripeProvider stripe={this.state.stripe}>
          <Elements>
            <CardForm handleResult={this.props.handleResult} />
          </Elements>
        </StripeProvider>
      </div>
    );
  }
}

export default AddCardsComp;
