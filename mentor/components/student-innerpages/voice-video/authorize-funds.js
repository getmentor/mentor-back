import React, { Component } from "react";
import "./authorize-funds.scss";
import counterpart from "counterpart";
import Translate from "react-translate-component";
import env from "../../../constant.json";

export default class AuthorizeFundsComp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      first: false,
      second: false,
      third: false,
      fourth: false,
      fifth: false
    };
  }

  componentDidMount() {
    window.scrollTo(0, 0);

    this.paymentAuthentication();
  }
  handleCreditAmount(selected) {
    if (selected === "first")
      this.setState({
        first: true,
        second: false,
        third: false,
        fourth: false,
        fifth: false
      });
    else if (selected === "second")
      this.setState({
        first: false,
        second: true,
        third: false,
        fourth: false,
        fifth: false
      });
    else if (selected === "third")
      this.setState({
        first: false,
        second: false,
        third: true,
        fourth: false,
        fifth: false
      });
    else if (selected === "fourth")
      this.setState({
        first: false,
        second: false,
        third: false,
        fourth: true,
        fifth: false
      });
    else if (selected === "fifth")
      this.setState({
        first: false,
        second: false,
        third: false,
        fourth: false,
        fifth: true
      });
  }

  paymentAuthentication() {
    const userId = localStorage.getItem("userId");

    fetch(env.striperetrievecustomer, {
      //"http://localhost:5000/stripe/retrievecustomer"
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        userId: userId
      })
    })
      .then(response => {})
      .then(json => {})
      .catch(err => {
        console.log("Error : " + err);
      });
  }
  addCards() {
    const userId = localStorage.getItem("userId");

    fetch(env.stripecreatecard, {
      //"http://localhost:5000/stripe/createcard"
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        userId: userId
      })
    })
      .then(response => {})
      .then(json => {})
      .catch(err => {
        console.log("Error : " + err);
      });
  }

  render() {
    return (
      <>
        <div className="authorize-funds-wrap">
          <div className="center-content is-inline-block">
            <Translate
              content="afheadings.hd1"
              component="h1"
              className="title is-4 headings"
              unsafe={true}
            />
            {/* <h1 className="title is-4 headings">Authorize your fund</h1> */}
            <Translate
              content="afheadings.hd2"
              component="h1"
              className="subtitle is-6 headings"
              unsafe={true}
            />
            {/* <h1 className="subtitle is-6 headings">
              Please authenticate your card. Charging will be done only after
              completion of your session.
            </h1> */}
            <div className="funds-wrap is-inline-block">
              <Translate
                content="afheadings.hd3"
                component="h1"
                className="title is-5"
                unsafe={true}
              />
              {/* <h1 className="title is-5">Credits</h1> */}
              <div className="credits-list is-inline-block">
                <div className="control is-inline-block">
                  <label className="radio">
                    <input
                      type="radio"
                      name="rating"
                      onClick={this.handleCreditAmount.bind(this, "first")}
                    />
                    <span
                      className={
                        this.state.first ? "checkmark is-active" : "checkmark"
                      }
                    >
                      $30
                    </span>
                  </label>
                  <label className="radio">
                    <input
                      type="radio"
                      name="rating"
                      onClick={this.handleCreditAmount.bind(this, "second")}
                    />
                    <span
                      className={
                        this.state.second ? "checkmark is-active" : "checkmark"
                      }
                    >
                      $50
                    </span>
                  </label>
                  <label className="radio">
                    <input
                      type="radio"
                      name="rating"
                      onClick={this.handleCreditAmount.bind(this, "third")}
                    />
                    <span
                      className={
                        this.state.third ? "checkmark is-active" : "checkmark"
                      }
                    >
                      $80
                    </span>
                  </label>

                  <label className="radio variable-input">
                    <input
                      type="text"
                      className="variable-input"
                      name="rating"
                      onClick={this.handleCreditAmount.bind(this, "fifth")}
                    />
                    <span className="sign-label">
                      <span className="sign-wrap">$</span>
                    </span>
                  </label>
                </div>
              </div>
            </div>
            <div className="payment-method-wrap is-inline-block">
              <Translate
                content="afheadings.hd4"
                component="h1"
                className="title is-5"
                unsafe={true}
              />
              {/* <h1 className="title is-5">Enter card details</h1> */}

              <form action="/charge" method="post" id="payment-form">
                <div class="form-row">
                  <label for="card-element">Credit or debit card</label>
                  <div id="card-element" />

                  <div id="card-errors" role="alert" />
                </div>

                <button>Submit Payment</button>
              </form>
              <ul className="card-details-list is-inline-block">
                <li className="is-inline-block">
                  <Translate
                    content="aflabel.l1"
                    component="label"
                    className="label"
                    unsafe={true}
                  />
                  {/* <label className="label">Card holder's name</label> */}
                  <input
                    className="input"
                    type="text"
                    placeholder="Card holder's name"
                  />
                </li>
                <li className="is-inline-block">
                  <Translate
                    content="aflabel.l2"
                    component="label"
                    className="label"
                    unsafe={true}
                  />
                  {/* <label className="label">Card number</label> */}
                  <input
                    className="input"
                    type="text"
                    placeholder="Card number"
                  />
                </li>
                <li className="is-inline-block">
                  <Translate
                    content="aflabel.l3"
                    component="label"
                    className="label"
                    unsafe={true}
                  />
                  {/* <label className="label">Expiry date</label> */}
                  <div className="select is-block">
                    <select>
                      <option>01</option>
                      <option>02</option>
                      <option>03</option>
                      <option>04</option>
                      <option>05</option>
                      <option>06</option>
                      <option>07</option>
                      <option>08</option>
                      <option>09</option>
                      <option>10</option>
                      <option>11</option>
                      <option>12</option>
                    </select>
                  </div>
                  <div className="select is-block">
                    <select>
                      <option>2025</option>
                      <option>2024</option>
                      <option>2023</option>
                      <option>2022</option>
                      <option>2021</option>
                      <option>2020</option>
                      <option>2019</option>
                      <option>2019</option>
                      <option>2018</option>
                      <option>2017</option>
                      <option>2016</option>
                      <option>2015</option>
                    </select>
                  </div>
                </li>
                <li>
                  <a className="button is-primary add-card is-block">
                    <Translate
                      content="afspan.s2"
                      component="span"
                      unsafe={true}
                      onClick={this.addCards}
                    />
                    {/* <span>Add card</span> */}
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </>
    );
  }
}
