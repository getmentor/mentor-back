import React, { Component } from "react";
import { CardElement, injectStripe } from "react-stripe-elements";
import counterpart from "counterpart";
import Translate from "react-translate-component";
var serialize = require("form-serialize");
import Router, { withRouter } from "next/router";
import env from "../../../constant.json";

const createOptions = () => {
  return {
    style: {
      base: {
        fontSize: "16px",
        color: "#424770",
        fontFamily: "Open Sans, sans-serif",
        letterSpacing: "0.025em",
        "::placeholder": {
          color: "#aab7c4"
        }
      },
      invalid: {
        color: "#c23d4b"
      }
    }
  };
};

class CardForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errorMessage: "",
      showMessageModal: false,
      addcard_status: "",
      addcard_message: "",
      status_id: null
    };
    this.submit = this.submit.bind(this);
  }
  componentDidMount() {
    this.paymentAuthentication();
  }

  handleChange = ({ error }) => {
    if (error) {
      this.setState({ errorMessage: error.message });
    }
  };

  async handleSubmit(evt) {
    evt.preventDefault();
    if (this.props.stripe) {
      const userId = localStorage.getItem("userId");
      let { token } = await this.props.stripe.createToken({ name: "Name" });

      fetch(env.stripecreatecard, {
        //"http://localhost:5000/stripe/createcard"
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          userId: userId,
          token: token.id
        })
      })
        .then(response => {
          response.json().then(result => {
            if (result.id) {
              this.setState({ showMessageModal: true });
              this.setState({ status_id: result.id });
              this.setState({ addcard_status: "Success" });
              this.setState({
                addcard_message: "Your card added successfully!"
              });
            } else {
              this.setState({ showMessageModal: true });
              this.setState({ status_id: null });
              this.setState({ addcard_status: "Warning" });
              this.setState({
                addcard_message: "Failed,Please provide valid datas!"
              });
            }
          });
        })
        .catch(err => {
          console.log("Error : " + err);
        });
    } else {
      console.log("Stripe.js hasn't loaded yet.");
    }
  }

  paymentAuthentication() {
    const userId = localStorage.getItem("userId");

    fetch(env.striperetrievecustomer, {
      //"http://localhost:5000/stripe/retrievecustomer"
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        userId: userId
      })
    })
      .then(response => {})
      .then(json => {})
      .catch(err => {
        console.log("Error : " + err);
      });
  }

  showMessageModal = modalState => {
    if (this.state.status_id) {
      Router.push("/chat");
    } else {
      this.setState({ showMessageModal: false });
    }

    // if (modalState === "modalOpen")
    //   this.setState({ showMessageModal: true });
    // else this.setState({ showMessageModal: false });
  };

  async submit(ev) {
    /*  let {token} = await this.props.stripe.createToken({name: "Name"});
  let response = await fetch("/charge", {
    method: "POST",
    headers: {"Content-Type": "text/plain"},
    body: token.id
  });

  if (response.ok) console.log("Purchase Complete!")*/
  }

  render() {
    return (
      <>
        <div
          className={
            this.state.showMessageModal
              ? "modal edit-add-modal add-expertise-moda l is-active"
              : "modal edit-add-modal add-expertise-modal"
          }
        >
          <div className="modal-background">
            <div className="modal-content">
              <p className="title is-4">{this.state.addcard_status}</p>

              <div className="add-edit-wrap is-inline-block">
                {" "}
                <div className="control">
                  <p className="title is-6 addcard_msg">
                    {this.state.addcard_message}
                  </p>
                </div>
                <a
                  className="button is-primary"
                  onClick={this.showMessageModal.bind(this, "modalClose")}
                >
                  Close
                </a>
              </div>
            </div>
            <button
              className="modal-close is-large"
              aria-label="close"
              onClick={this.showMessageModal.bind(this, "modalClose")}
            />
          </div>
        </div>

        <div className="CardDemo">
          <form
            id="CardForm"
            className="CardForm"
            onSubmit={this.handleSubmit.bind(this)}
          >
            <label>
              <h3>Card details</h3>
              <CardElement onChange={this.handleChange} {...createOptions()} />
            </label>
            <div className="error" role="alert">
              {this.state.errorMessage}
            </div>
            <button>Add</button>
          </form>
        </div>
      </>
    );
  }
}

export default injectStripe(CardForm);
