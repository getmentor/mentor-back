import React, { Component } from "react";
import "./completed.scss";
import StarRating from "../../star-rating/star-rating";

export default class STDVVCompleted extends Component {
  render() {
    return (
      <div>
        <div className="std-voice-video-completed-wrap">
          <div className="center-content">
            <h1 className="title is-4">
              Thank you for completing this session
            </h1>
            <h2 className="subtitle">
              Your account has ben debited with $
              <span className="amount">30</span>
            </h2>
            <label className="label rate-teacher">Rate this teacher</label>
            <div className="rate-teacher-wrap is-inline-block">
              <StarRating />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
