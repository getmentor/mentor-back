import React, { Component } from "react";
import Link from "next/link";
import "./teacher-info.scss";
import Router, { withRouter } from "next/router";
import env from "../../constant.json";

class TecherInfo extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTab1: true,
      activeTab2: false,
      activeTab3: false,
      fname: null,
      lname: null,
      email: null,
      resume_link: null,
      linkedin_url: null,
      twitte_url: null,
      medium_url: null,
      main_desc: null,
      short_desc: null,
      profile_pic: null,
      userexpertise: [],
      mentor_id: null
    };
  }
  isActive = true;
  componentWillMount() {
    if (this.props.router.query.mentorId) {
      this.setState({ mentor_id: this.props.router.query.mentorId });
      // localStorage.setItem("mentor_id", this.props.router.query.mentorId);
      sessionStorage.setItem("mentor_id", this.props.router.query.mentorId);
    }
  }

  componentDidMount() {
    window.scrollTo(0, 0);

    this.fetchMentors();
    this.fetchUserExpertise();
  }

  fetchMentors() {
    // const userId = localStorage.getItem("userId");
    const userId = sessionStorage.getItem("mentor_id");

    let requestBody = {
      query: `
        query MentorDetails($userId: String!) {
          mentorDetails(userId: $userId) {
            userId    
            name   
            last_name     
            email
            role
            main_desc
            short_desc
            profile_pic
            resume_link
            linkedin_url
            twitte_url
            medium_url
          }
        }
      `,
      variables: {
        userId: userId
      }
    };

    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(resData => {
        if (resData.data.mentorDetails) {
          this.setState({ fname: resData.data.mentorDetails.name });
          this.setState({ lname: resData.data.mentorDetails.last_name });
          this.setState({ email: resData.data.mentorDetails.email });
          this.setState({ main_desc: resData.data.mentorDetails.main_desc });
          this.setState({ short_desc: resData.data.mentorDetails.short_desc });
          this.setState({
            profile_pic: resData.data.mentorDetails.profile_pic
          });
          this.setState({
            resume_link: resData.data.mentorDetails.resume_link
          });
          this.setState({
            linkedin_url: resData.data.mentorDetails.linkedin_url
          });
          this.setState({ twitte_url: resData.data.mentorDetails.twitte_url });
          this.setState({ medium_url: resData.data.mentorDetails.medium_url });

          window.scrollTo(0, 0);
        }
      })
      .catch(err => {
        console.log(err);
      });
  }
  fetchUserExpertise() {
    const userId = localStorage.getItem("userId");

    let requestBody = {
      query: `
        query ListUserExpertise($user: ID!) {
              listUserExpertise(user: $user) {
                _id
                expertise{expertise}
                user{name}
                description
          }
        }
      `,
      variables: {
        user: userId
      }
    };
    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(resData => {
        if (resData.data.listUserExpertise) {
          if (this.isActive) {
            this.setState({ userexpertise: resData.data.listUserExpertise });
            console.log(this.state.userexpertise);
          }
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  authenticateUser() {
    const userId = localStorage.getItem("userId");

    const mentor_name = this.state.fname + " " + this.state.lname;
    const mentor_email = this.state.email;
    const mentor_image = this.state.profile_pic;

    fetch(env.striperetrievecard, {
      //"http://localhost:5000/stripe/retrievecard"
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        userId: userId
      })
    })
      .then(response => {
        response.json().then(result => {
          console.log(result);

          if (result.data.length > 0) {
            console.log("chat");
            sessionStorage.setItem("mentor_name", mentor_name);
            sessionStorage.setItem("mentor_email", mentor_email);
            sessionStorage.setItem("mentor_image", mentor_image);
            Router.push("/chat");

            /*Router.push(
              {
                pathname: "/chat",
                query: {
                  mentor_name: mentor_name,
                  mentor_email: mentor_email,
                  mentor_image: mentor_image
                }
              },
              "/chat"
            );*/
          } else {
            console.log("addcards");
            Router.push("/addcards");
          }
        });
      })
      .catch(err => {
        console.log("Error : " + err);
      });
  }

  toggleClass(tabSelected) {
    if (tabSelected === "profile")
      this.setState({ activeTab1: true, activeTab2: false, activeTab3: false });
    else if (tabSelected === "expertise")
      this.setState({ activeTab1: false, activeTab2: true, activeTab3: false });
    else
      this.setState({ activeTab1: false, activeTab2: false, activeTab3: true });
  }
  render() {
    return (
      <>
        <div className="container layout-container">
          <div ClassName="columns">
            <div ClassName="column">
              <div className="teacher-profile-wrap has-text-centered">
                <div className="avatar is-inline-block">
                  <img
                    src={"../static/uploads/users/" + this.state.profile_pic}
                    alt="avatar"
                  />
                </div>
                <h1 className="title is-3 name">{this.state.fname}</h1>
                <h2 className="subtitle is-6 name">{this.state.short_desc}</h2>
                <ul className="des is-inline-block">
                  <li className="is-inline-block">
                    <img src="../static/location.png" alt="location-icon" />
                    <span className="subtitle">
                      <span className="current-city subtitle">Barcelona</span>
                      <span className="current-country subtitle">Spain</span>
                      <span className="time-zone subtitle">(+01:00 UTC)</span>
                    </span>
                  </li>
                  <li clasName="is-inline-block">
                    <span className="subtitle">Spanish</span>
                    <span className="subtitle">English</span>
                  </li>
                  <li className="native is-inline-block">
                    <span className="subtitle">From</span>
                    <span className="current-city subtitle">Christchurch</span>
                    <span className="current-country subtitle is-6">
                      Newzealand
                    </span>
                  </li>
                </ul>
                <div className="detail-info has-text-centered">
                  <ul className="main is-inline-block">
                    <li className="is-inline-block">
                      <p className="title">Free</p>
                      <ul className="session-durations is-inline-block">
                        <li>
                          <p className="subtitle is-5">15min</p>
                        </li>
                        <li>
                          <p className="subtitle is-5">30min</p>
                        </li>
                        <li>
                          <p className="subtitle is-5">60min</p>
                        </li>
                      </ul>
                    </li>
                    <li is-inline-block>
                      <p className="has-text-centerd">
                        <img
                          src="../static/desk-microphone.png"
                          alt="microphone-icon"
                        />
                        <img
                          src="../static/camera-video.png"
                          alt="microphone-icon"
                        />
                      </p>
                      <p>Available</p>
                    </li>
                    <li is-inline-block>
                      <p className="title">93</p>
                      <p>Sessions</p>
                    </li>
                    <li is-inline-block>
                      <p className="has-text-centerd">
                        <img src="../static/star.png" alt="star-icon" />
                        <span className="title">4.5</span>
                      </p>
                      <p>Rating</p>
                    </li>
                  </ul>
                </div>
                <div className="contacts-wrap">
                  <ul className="contacts-ul is-inline-block">
                    <li className="is-inline-block">
                      <a
                        onClick={this.authenticateUser.bind(this)}
                        className="button is-link"
                      >
                        Message Me
                      </a>
                      {/* <Link href="/authorize-funds">
                        <a className="button is-link">Message Me</a>
                      </Link> */}
                    </li>
                    <li className="social">
                      <a>
                        <img src="../static/fb-logo.png" alt="facebook-icon" />
                      </a>
                      <a>
                        <img
                          src="../static/twitter-logo.png"
                          alt="twitter-icon"
                        />
                      </a>
                      <a>
                        <img
                          src="../static/linked-in-logo.png"
                          alt="twitter-icon"
                        />
                      </a>
                    </li>
                  </ul>
                </div>
                <div className="details-wrap">
                  <div className="details-head is-inline-block">
                    <ul>
                      <li
                        className={
                          this.state.activeTab1
                            ? "tab-link is-inline-block is-active"
                            : "tab-link is-inline-block"
                        }
                        onClick={this.toggleClass.bind(this, "profile")}
                      >
                        Profile
                      </li>
                      <li
                        className={
                          this.state.activeTab2
                            ? "tab-link is-inline-block is-active"
                            : "tab-link is-inline-block"
                        }
                        onClick={this.toggleClass.bind(this, "expertise")}
                      >
                        Expertise
                      </li>
                      <li
                        className={
                          this.state.activeTab3
                            ? "tab-link is-inline-block is-active"
                            : "tab-link is-inline-block"
                        }
                        onClick={this.toggleClass.bind(this, "reviews")}
                      >
                        Reviews(<span className="review-count">3</span>)
                      </li>
                    </ul>
                  </div>
                  <div className="details-content">
                    <div
                      className={
                        this.state.activeTab1
                          ? "tab-pane is-active"
                          : "tab-pane"
                      }
                      id="profile"
                    >
                      {this.state.main_desc}
                    </div>
                    <div
                      className={
                        this.state.activeTab2
                          ? "tab-pane is-active"
                          : "tab-pane"
                      }
                      id="expertise"
                    >
                      <h1 className="title is-5">Expertise</h1>
                      <ul className="experise-list is-inline-block">
                        {this.state.userexpertise.map(
                          function(item) {
                            return (
                              <li>
                                <ul className="sub-ul is-inline-block">
                                  <li className="left-side">
                                    <p className="title is-6">
                                      {item.expertise.expertise}
                                    </p>
                                  </li>
                                  <li className="right-side">
                                    {item.description}
                                  </li>
                                </ul>
                              </li>
                            );
                          }.bind(this)
                        )}
                      </ul>
                    </div>
                    <div
                      className={
                        this.state.activeTab3
                          ? "tab-pane is-active"
                          : "tab-pane"
                      }
                      id="reviews"
                    >
                      <h1 className="title is-5">Reviews</h1>
                      <div className="review-wrap">
                        <ul className="review-list">
                          <li className="is-inline-block">
                            <div className="sub-ul columns">
                              <div className="l-side column is-one-fifth">
                                <div className="avatar">
                                  <img
                                    src="../static/p_001.jpg"
                                    alt="profile-image"
                                  />
                                </div>
                                <h1 className="title guest-name">Fernando</h1>
                                <p className="subtitle">1 session</p>
                                <p className="subtitle">25 Apr 2019</p>
                              </div>
                              <div className="r-side column is-four-fifths">
                                <span className="reviews">
                                  Great person and highly skilled teacher !
                                </span>
                              </div>
                            </div>
                          </li>

                          <li className="is-inline-block">
                            <div className="sub-ul columns">
                              <div className="l-side column is-one-fifth">
                                <div className="avatar">
                                  <img
                                    src="../static/p_003.jpg"
                                    alt="profile-image"
                                  />
                                </div>
                                <h1 className="title guest-name">Alexa</h1>
                                <p className="subtitle">2 sessions</p>
                                <p className="subtitle">15 Mar 2019</p>
                              </div>
                              <div className="r-side column is-four-fifths">
                                <span className="reviews">
                                  Talking to Luke was a great experience. I
                                  really enjoyed it. He provided some very
                                  insightful feedback and inspiring ideas. I am
                                  grateful. !
                                </span>
                              </div>
                            </div>
                          </li>
                          <li className="is-inline-block">
                            <div className="sub-ul columns">
                              <div className="l-side column is-one-fifth">
                                <div className="avatar">
                                  <img
                                    src="../static/p_002.jpg"
                                    alt="profile-image"
                                  />
                                </div>
                                <h1 className="title guest-name">Antonio</h1>
                                <p className="subtitle">1 session</p>
                                <p className="subtitle">26 Feb 2019</p>
                              </div>
                              <div className="r-side column is-four-fifths">
                                <span className="reviews">
                                  Luke provided some great resources, shared his
                                  insight and wisdom, and pointed me in the
                                  right direction. Thanks so much Luke for your
                                  time and willingness to help!
                                </span>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default withRouter(TecherInfo);
