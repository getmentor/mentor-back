import React, { Component } from "react";
import "./chat-list.scss";
import Moment from "react-moment";

export default class ChatList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      curDate: null,
      useremail: null
    };
    this.searchchatuser = React.createRef();
  }

  componentDidMount() {
    this.setState({ users: this.props.users });
    this.setState({ useremail: this.props.useremail });

    const today = Date.now();
    const new_date = new Intl.DateTimeFormat("en-US", {
      year: "numeric",
      month: "2-digit",
      day: "2-digit"
    }).format(today);

    this.setState({
      curDate: new_date
    });
  }
  searchUser = event => {
    const searchchatuser = this.searchchatuser.current.value;
    console.log(searchchatuser);
  };
  selectedchat = email => {
    const useremail = this.state.useremail;

    if (email[0].email != useremail) {
      sessionStorage.setItem("mentor_email", email[0].email);
      sessionStorage.setItem("mentor_name", email[0].username);
    } else if (email[1].email != useremail) {
      sessionStorage.setItem("mentor_email", email[1].email);
      sessionStorage.setItem("mentor_name", email[1].username);
    }

    this.props.changekey();
    // window.location.reload(false);
  };

  render() {
    return (
      <>
        <div className="container">
          <div className="chat-list-container">
            <div className="listing-header">
              <div className="search-wrap">
                <input
                  type="text"
                  placeholder="Search"
                  onClick={this.searchUser}
                  ref={this.searchchatuser}
                />
                <img src="../static/search.png" />
              </div>
            </div>
            <div className="individual-list-wrap">
              {this.state.users.length > 0 &&
                this.state.users.map(
                  function(item) {
                    return (
                      <div className="individual-list is-active">
                        <ul className="notifications-list is-inline-block">
                          <li>
                            <div className="header-notifications-trigger">
                              <div className="user-avatar">
                                <img
                                  src={"../static/uploads/users/" + item.avatar}
                                  alt="user"
                                  onClick={this.selectedchat.bind(
                                    this,
                                    item.participants
                                  )}
                                />
                              </div>
                            </div>
                          </li>
                          <li>
                            <p className="title is-6">{item.name}</p>
                            <p className="notification-msg">
                              {item.last_comment_message}
                            </p>
                            <p className="time">
                              {this.state.curDate !=
                              (
                                <Moment format="MM/DD/YYYY">
                                  {item.last_comment_message_created_at}
                                </Moment>
                              ) ? (
                                <Moment format="DD/MM/YY">
                                  {item.last_comment_message_created_at}
                                </Moment>
                              ) : (
                                <Moment format="HH:SS">
                                  {item.last_comment_message_created_at}
                                </Moment>
                              )}
                            </p>
                          </li>
                        </ul>
                      </div>
                    );
                  }.bind(this)
                )}
            </div>
          </div>
        </div>
      </>
    );
  }
}
