import React, { Component } from "react";
import "./dashboard-chat.scss";
import Footer from "../footer/footer";
import ChatBox from "../teacher-innerpages/chat/chatbox";
import ChatList from "../teacher-innerpages/chat/chat-list";
import ChatListMobile from "../teacher-innerpages/chat/chat-list-mobile";
import counterpart from "counterpart";
import Translate from "react-translate-component";
import QiscusSDKCore from "qiscus-sdk-core";
import { EXITED } from "react-transition-group/Transition";
const qiscus = new QiscusSDKCore();
import Spinner from "../../components/Spinner/Spinner";
import env from "../../constant.json";

export default class DashboardChat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ShowSuggestedSessionModal: false,
      username: "",
      useremail: "",
      userimage: "",
      users: [],
      isLoading: true,
      key: null,
      mentor_email: ""
    };
  }
  isActive = true;
  componentDidMount() {
    this.fetchUser();
  }

  handleSessionSuggestedModal = modalState => {
    if (modalState === "modalOpen")
      this.setState({ ShowSuggestedSessionModal: true });
    else this.setState({ ShowSuggestedSessionModal: false });
  };

  //  Initialization Qiscus Chat SDK
  Initialization() {
    qiscus.init({
      AppId: "mentor-r6ledbypleisco", // "mentorcha-ehkoh6xenpl",
      options: {
        commentDeletedCallback: function(data) {},
        commentDeliveredCallback: function(data) {},
        commentReadCallback: function(data) {},
        presenceCallback: function(data) {},
        typingCallback: function(data) {
          console.log("Typing.........");
        },
        onReconnectCallback: function(data) {},
        newMessagesCallback: messages => {
          messages.forEach(message => {
            console.log(message.message);
          });
        },
        roomClearedCallback: function(data) {}
      }
    });
    // console.log(qiscus);
  }

  //  AuthenticateUser to Qiscus

  AuthenticateUser = event => {
    const user_name = this.state.username;
    const user_email = this.state.useremail;
    const user_image = this.state.userimage;

    console.log("user_email", user_email);

    qiscus
      .setUser(user_email, user_email, user_name, user_image, {})
      .then(response => {
        //  this.participantList();
        this.loadRoomList();
      })
      .catch(function(error) {});

    // console.log(qiscus);
  };
  //  Get Participant List
  /*participantList = event => {
    this.setState({ isLoading: true });
    qiscus
      .getUsers()
      .then(async users => {
        if (this.isActive) {
          await this.setState({ users: users.users, isLoading: false });
        }
        console.log(this.state.users);

        // On success
      })
      .catch(function(error) {
        // On error
      });
  };*/

  loadRoomList = event => {
    qiscus
      .loadRoomList({ show_participants: true })

      .then(async rooms => {
        if (this.isActive) {
          await this.setState({
            users: rooms,
            isLoading: false,
            mentor_email: sessionStorage.getItem("mentor_email")
          });
        }
        console.log("this.state.isLoading", this.state.isLoading);
        console.log("userssss:", this.state.users);

        // On success
      })
      .catch(function(error) {
        // On error
      });
  };

  async fetchUser() {
    const userId = localStorage.getItem("userId");

    let requestBody = {
      query: `
        query userDetails($userId: String!) {
          userDetails(userId: $userId) {
            userId    
            name   
            last_name     
            email
            role
            main_desc
            short_desc
            profile_pic
            resume_link
            linkedin_url
            twitte_url
            medium_url
          }
        }
      `,
      variables: {
        userId: userId
      }
    };

    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(async resData => {
        if (resData.data.userDetails) {
          await this.setState({
            username: resData.data.userDetails.name
          });
          await this.setState({
            userimage: resData.data.userDetails.profile_pic
          });
          await this.setState({ useremail: resData.data.userDetails.email });

          window.scrollTo(0, 0);

          this.Initialization();
          this.AuthenticateUser();
          // this.CreateChatRoom();
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  changekey = event => {
    this.setState({ key: Math.random() });
  };
  render() {
    return (
      <>
        <div className="dashboard-container">
          <div className="dashboard-inner">
            <div className="content-box is-inline-block">
              <div className="chat-window-wrap is-inline-block">
                <div className="chat-list">
                  {this.state.isLoading ? (
                    <Spinner />
                  ) : (
                    <ChatList
                      users={this.state.users}
                      useremail={this.state.useremail}
                      changekey={this.changekey.bind(this)}
                    />
                  )}
                </div>
                <div className="chat-details">
                  <ChatBox key={this.state.key} />
                </div>
              </div>
              <div className="Mobile-chat-area is-inline-block">
                <ChatListMobile />
              </div>
            </div>
            <Footer />
          </div>
        </div>

        <div
          className={
            this.state.ShowSuggestedSessionModal
              ? "modal suggested-a-session-modal is-active"
              : "modal suggested-a-session-modal"
          }
        >
          <div className="modal-background">
            <div className="modal-content">
              {" "}
              <p className="title is-4 head">
                Student has requested for a session, click here to start
              </p>
              <a
                className="button is-primary"
                onClick={this.handleSessionSuggestedModal.bind(
                  this,
                  "modalClose"
                )}
              >
                Start session
              </a>
            </div>

            <button
              className="modal-close is-large"
              aria-label="close"
              onClick={this.handleSessionSuggestedModal.bind(
                this,
                "modalClose"
              )}
            />
          </div>
        </div>
      </>
    );
  }
}
