import React, { Component } from "react";
import "./dashboard.scss";

export default class Dashboard extends Component {
  render() {
    return (
      <>
        <div className="container dashboard-container">
          <div className="side-panel" />
          <div className="content-display" />
        </div>
      </>
    );
  }
}
