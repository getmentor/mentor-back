import React, { Component } from "react";
import "./mobile-chat-box.scss";
import Footer from "../footer/footer";
import ChatBox from "../chatbox/chatbox";
import ChatList from "../chatbox/chat-list";
import counterpart from "counterpart";
import Translate from "react-translate-component";

export default class MobileChatBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showChatWindow: false
    };
  }

  render() {
    return (
      <>
        <div className="mobile-chat-window-wrap is-inline-block">
          <div className="chat-details">
            <a className="button is-primary back-to-chatlist">
              <span>Back to chat list</span>
            </a>
            <ChatBox />
          </div>
        </div>
      </>
    );
  }
}
