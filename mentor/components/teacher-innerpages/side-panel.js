import React, { Component } from "react";
import Link from "next/link";
import "./side-panel.scss";
export default class SidePanel extends Component {
  handleDashboardMenu = () => {
    document.getElementById("side-panel").classList.toggle("is-active");
  };
  render() {
    return (
      <>
        <div className="dashboard-navigation">
          <div
            className="hamburger-wrap is-inline-block"
            onClick={this.handleDashboardMenu}
          >
            <span className="is-inline-block" />
            <span className="is-inline-block" />
            <span className="is-inline-block" />
          </div>
          <span className="dashboard-title">Dashboard Navigation</span>
        </div>
        <div className="side-panel-wrap" id="side-panel">
          <ul className="side-panel-menu">
            <li className="is-active">
              <Link>
                <a>Dashboard</a>
              </Link>
            </li>
            <li>
              <Link>
                <a>Profile Edit</a>
              </Link>
            </li>
            <li>
              <Link>
                <a>Chat/Messages</a>
              </Link>
            </li>
            <li>
              <Link>
                <a>Rates and schedule setting</a>
              </Link>
            </li>
            <li>
              <Link href="/">
                <a>Logout</a>
              </Link>
            </li>
          </ul>
        </div>
      </>
    );
  }
}
