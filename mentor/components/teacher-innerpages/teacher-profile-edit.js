import React, { Component } from "react";
import "./teacher-profile-edit.scss";
import Footer from "../footer/footer";
import Spinner from "../../components/Spinner/Spinner";
import env from "../../constant.json";

export default class StudentProfileEditComp extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);

    this.fetchMentors();
    this.fetchSkills();
    this.fetchLanguages();
    this.fetchExpertise();
    this.fetchUserExpertise();
    this.fetchUserExperiences();
    this.fetchUserEducation();
    this.fetchToolkit();
    this.fetchUserToolkit();
    this.fetchCertificates();
    this.fetchUserCertificates();
  }
  toggleStatus = currentState => {
    if (currentState === "userIsOffline")
      this.setState({ userIsOnline: false, userIsOffline: true });
    else this.setState({ userIsOnline: true, userIsOffline: false });
  };
  constructor(props) {
    super(props);
    this.state = {
      userIsOnline: true,
      userIsOffline: false,
      teacherAccount: true,
      studentAccount: false,
      showEditExpertiseModal: false,
      showAddExpertiseModal: false,
      showAddEducationModal: false,
      showEditEducationModal: false,
      showAddEducationStudentModal: false,
      showEditEducationStudentModal: false,
      showEditExperienceModal: false,
      showAddExperienceModal: false,
      showAddSkillsModal: false,
      showAddLanguagesModal: false,
      showAddToolkitModal: false,
      isLoading: false,
      fname: null,
      lname: null,
      email: null,
      resume_link: null,
      linkedin_url: null,
      twitte_url: null,
      medium_url: null,
      main_desc: null,
      short_desc: null,
      profile_pic: null,
      skills: [],
      language: [],
      expertise: [],
      userexpertise: [],
      userexperiences: [],
      usereducation: [],
      toolkit: [],
      usertoolkit: [],
      certificate: [],
      usercertificate: [],
      selectedFile: null,
      selectedExpertise: null,
      selectedToolkit: null,
      selectedCertificate: null,
      selUserWorkExpMonthFrom: null,
      selUserWorkExpYearFrom: null,
      selUserWorkExpMonthTo: null,
      selUserWorkExpYearTo: null,
      selUserEduQualAdd: null,
      selUserEduMonthFromAdd: null,
      selUserEduYearFromAdd: null,
      selUserEduMonthToAdd: null,
      selUserEduYearToAdd: null,
      file: null,
      year: [
        { id: 2019, year: 2019 },
        { id: 2018, year: 2018 },
        { id: 2017, year: 2017 },
        { id: 2016, year: 2016 },
        { id: 2015, year: 2015 },
        { id: 2014, year: 2014 },
        { id: 2013, year: 2013 },
        { id: 2012, year: 2012 },
        { id: 2011, year: 2011 },
        { id: 2010, year: 2010 },
        { id: 2009, year: 2009 },
        { id: 2008, year: 2008 },
        { id: 2007, year: 2007 },
        { id: 2006, year: 2006 },
        { id: 2005, year: 2005 },
        { id: 2004, year: 2004 },
        { id: 2003, year: 2003 },
        { id: 2002, year: 2002 },
        { id: 2001, year: 2001 },
        { id: 2000, year: 2000 }
      ],
      month: [
        { id: 1, month: "January" },
        { id: 2, month: "February" },
        { id: 3, month: "March" },
        { id: 4, month: "April" },
        { id: 5, month: "May" },
        { id: 6, month: "June" },
        { id: 7, month: "July" },
        { id: 8, month: "August" },
        { id: 9, month: "Sepetember" },
        { id: 10, month: "October" },
        { id: 11, month: "November" },
        { id: 12, month: "December" }
      ]

      // menuClicked: false
    };
    this.expDesc = React.createRef();
    this.expertiseErrorMsg = null;
    this.experienceErrorMsg = null;
    this.toolkitErrorMsg = null;
    this.certificateErrorMsg = null;

    this.companyName = React.createRef();
    this.jobTitle = React.createRef();
    this.location = React.createRef();
    //this.selUserWorkExpMonthFrom = React.createRef();
    // this.selUserWorkExpYearFrom = React.createRef();
    // this.selUserWorkExpMonthTo = React.createRef();
    // this.selUserWorkExpYearTo = React.createRef();
    this.present = React.createRef();
    this.userworkExpdescription = React.createRef();

    this.fieldAdd = React.createRef();

    this.onChangeFile = this.onChangeFile.bind(this);

    this.toolkitDesc = React.createRef();

    this.certificateDesc = React.createRef();
  }

  onChangeFile(e) {
    this.setState({ file: e.target.files[0] });

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        file: file,
        profile_pic: reader.result
        //profile_pic: file.name
      });
    };

    reader.readAsDataURL(file);
  }
  isActive = true;

  handleAccountSwitching = activeAccount => {
    if (activeAccount === "teacherAccount")
      this.setState({ teacherAccount: true, studentAccount: false });
    else this.setState({ teacherAccount: false, studentAccount: true });
  };
  handleEditExpertiseModal = modalState => {
    if (modalState === "modalOpen")
      this.setState({ showEditExpertiseModal: true });
    else this.setState({ showEditExpertiseModal: false });
  };
  handleAddExpertiseModal = modalState => {
    this.state.selectedExpertise = "";
    this.expDesc.current.value = "";
    this.state.expertiseErrorMsg = "";
    if (modalState === "modalOpen")
      this.setState({ showAddExpertiseModal: true });
    else this.setState({ showAddExpertiseModal: false });
  };
  handleAddEducationModal = modalState => {
    if (modalState === "modalOpen")
      this.setState({ showAddEducationModal: true });
    else this.setState({ showAddEducationModal: false });
  };
  handleEditEducationModal = modalState => {
    if (modalState === "modalOpen")
      this.setState({ showEditEducationModal: true });
    else this.setState({ showEditEducationModal: false });
  };
  handleAddEducationStudentModal = modalState => {
    if (modalState === "modalOpen")
      this.setState({ showAddEducationStudentModal: true });
    else this.setState({ showAddEducationStudentModal: false });
  };
  handleEditEducationStudentModal = modalState => {
    if (modalState === "modalOpen")
      this.setState({ showEditEducationStudentModal: true });
    else this.setState({ showEditEducationStudentModal: false });
  };
  handleEditExperienceModal = modalState => {
    if (modalState === "modalOpen")
      this.setState({ showEditExperienceModal: true });
    else this.setState({ showEditExperienceModal: false });
  };
  handleAddExperienceModal = modalState => {
    this.state.experienceErrorMsg = "";
    if (modalState === "modalOpen")
      this.setState({ showAddExperienceModal: true });
    else this.setState({ showAddExperienceModal: false });
  };
  handleAddSkillsModal = modalState => {
    if (modalState === "modalOpen") this.setState({ showAddSkillsModal: true });
    else this.setState({ showAddSkillsModal: false });
  };
  handleAddLanguagesModal = modalState => {
    if (modalState === "modalOpen")
      this.setState({ showAddLanguagesModal: true });
    else this.setState({ showAddLanguagesModal: false });
  };
  handleAddToolkitModal = modalState => {
    this.state.selectedToolkit = "";
    this.toolkitDesc.current.value = "";
    this.state.toolkitErrorMsg = "";
    if (modalState === "modalOpen")
      this.setState({ showAddToolkitModal: true });
    else this.setState({ showAddToolkitModal: false });
  };
  handleAddCertificateModal = modalState => {
    this.state.selectedCertificate = "";
    this.certificateDesc.current.value = "";
    this.state.certificateErrorMsg = "";
    if (modalState === "modalOpen")
      this.setState({ showAddCertificateModal: true });
    else this.setState({ showAddCertificateModal: false });
  };

  componentWillUnmount() {
    this.isActive = false;
  }

  fetchLanguages() {
    let requestBody = {
      query: `
        query Language {
          language{
            _id,
            language,
            status
          }
        }
      `
    };
    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(resData => {
        if (resData.data.language) {
          if (this.isActive) {
            this.setState({ language: resData.data.language });
          }
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  fetchSkills() {
    let requestBody = {
      query: `
        query Skills {
          skills{
            _id,
            skill,
            status
          }
        }
      `
    };
    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(resData => {
        if (resData.data.skills) {
          if (this.isActive) {
            this.setState({ skills: resData.data.skills });
          }
        }
      })
      .catch(err => {
        console.log(err);
      });
  }
  fetchMentors() {
    const userId = localStorage.getItem("userId");

    let requestBody = {
      query: `
        query MentorDetails($userId: String!) {
          mentorDetails(userId: $userId) {
            userId    
            name   
            last_name     
            email
            role
            main_desc
            short_desc
            profile_pic
            resume_link
            linkedin_url
            twitte_url
            medium_url
          }
        }
      `,
      variables: {
        userId: userId
      }
    };

    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(resData => {
        if (resData.data.mentorDetails) {
          // console.log(resData.data.mentorDetails);
          this.setState({ fname: resData.data.mentorDetails.name });
          this.setState({ lname: resData.data.mentorDetails.last_name });
          this.setState({ email: resData.data.mentorDetails.email });
          this.setState({ main_desc: resData.data.mentorDetails.main_desc });
          this.setState({ short_desc: resData.data.mentorDetails.short_desc });
          this.setState({
            profile_pic:resData.data.mentorDetails.profile_pic
          });
          this.setState({
            resume_link: resData.data.mentorDetails.resume_link
          });
          this.setState({
            linkedin_url: resData.data.mentorDetails.linkedin_url
          });
          this.setState({ twitte_url: resData.data.mentorDetails.twitte_url });
          this.setState({ medium_url: resData.data.mentorDetails.medium_url });

          window.scrollTo(0, 0);
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  fetchExpertise() {
    let requestBody = {
      query: `
        query Expertiselist {
          expertiselist{
            _id,
            expertise,
            status
          }
        }
      `
    };
    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(resData => {
        if (resData.data.expertiselist) {
          if (this.isActive) {
            this.setState({ expertise: resData.data.expertiselist });
          }
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  fetchUserExpertise() {
    const userId = localStorage.getItem("userId");

    let requestBody = {
      query: `
        query ListUserExpertise($user: ID!) {
              listUserExpertise(user: $user) {
                _id
                expertise{expertise}
                user{name}
                description
          }
        }
      `,
      variables: {
        user: userId
      }
    };
    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(resData => {
        if (resData.data.listUserExpertise) {
          if (this.isActive) {
            this.setState({ userexpertise: resData.data.listUserExpertise });
          }
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  addExpertise = event => {
    event.preventDefault();

    const expertise = this.state.selectedExpertise;
    const user = localStorage.getItem("userId");
    const description = this.expDesc.current.value;

    let requestBody = {
      query: `
        mutation CreateUserExpertise($expertise: ID!,$user:ID!,$description: String!) {
          createUserExpertise(expertise: $expertise,user:$user,description: $description) {
            _id
          }
        }
      `,
      variables: {
        expertise: expertise,
        user: user,
        description: description
      }
    };

    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          this.setState({
            expertiseErrorMsg: <span className="error show">Failed!</span>
          });
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(resData => {
        console.log(resData);
        if (resData.data.createUserExpertise._id) {
          this.fetchUserExpertise();
        }
        this.setState({ showAddExpertiseModal: false });
      })
      .catch(err => {
        console.log(err);
      });
  };

  fetchUserExperiences() {
    const userId = localStorage.getItem("userId");

    let requestBody = {
      query: `
        query experiences($userId: ID!) {
          experiences(userId: $userId) {
                _id
                company_name
                job_title
                location
                from_year
                from_month
                to_year
                to_month
                present
                description
          }
        }
      `,
      variables: {
        userId: userId
      }
    };
    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(resData => {
        if (resData.data.experiences) {
          if (this.isActive) {
            this.setState({ userexperiences: resData.data.experiences });
          }
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  addUserExperiences = event => {
    event.preventDefault();

    const company_name = this.companyName.current.value;
    const job_title = this.jobTitle.current.value;
    const location = this.location.current.value;
    // const from_month = this.selUserWorkExpMonthFrom.current.value;
    // const from_year = this.selUserWorkExpYearFrom.current.value;
    // const to_month = this.selUserWorkExpMonthTo.current.value;
    // const to_year = this.selUserWorkExpYearTo.current.value;

    const from_month = this.state.selUserWorkExpMonthFrom;
    const from_year = this.state.selUserWorkExpYearFrom;
    const to_month = this.state.selUserWorkExpMonthTo;
    const to_year = this.state.selUserWorkExpYearTo;

    const present = this.present.current.value;
    let currentComp;
    if (present == "on") {
      currentComp = 1;
    } else {
      currentComp = 0;
    }
    const description = this.userworkExpdescription.current.value;
    const user_id = localStorage.getItem("userId");

    // console.log(company_name);
    // console.log(job_title);
    // console.log(location);
    // console.log(from_month);
    // console.log(from_year);
    // console.log(to_month);
    // console.log(to_year);
    console.log(present);
    // console.log(description);
    // console.log(user_id);

    let requestBody = {
      query: `
      mutation CreateWorkExperience($company_name:String!,$job_title:String!,$location: String!,$from_year: String,$from_month:String,$to_year:String,$to_month :String,$present :Int!,$description :String,$user_id:ID!) {
        createWorkExperience(workExp: {company_name: $company_name,job_title:$job_title,location:$location,from_year: $from_year, from_month: $from_month,to_year:$to_year,to_month:$to_month,present:$present,description:$description,user_id:$user_id}) {
         _id
        }
      }
      `,
      variables: {
        company_name: company_name,
        job_title: job_title,
        location: location,
        from_year: from_year,
        from_month: from_month,
        to_year: to_year,
        to_month: to_month,
        present: currentComp,
        description: description,
        user_id: user_id
      }
    };

    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          this.setState({
            experienceErrorMsg: <span className="error show">Failed!</span>
          });
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(resData => {
        console.log(resData);
        if (resData.data.createWorkExperience._id) {
          this.fetchUserExperiences();
        }
        this.setState({ showAddExperienceModal: false });
      })
      .catch(err => {
        console.log(err);
      });
  };

  fetchUserEducation() {
    const userId = localStorage.getItem("userId");

    let requestBody = {
      query: `
        query usereducation($userId: ID!) {
          usereducation(userId: $userId) {
                _id
                qualification
                field
                from_year
                from_month
                to_year
                to_month
          }
        }
      `,
      variables: {
        userId: userId
      }
    };
    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(resData => {
        if (resData.data.usereducation) {
          if (this.isActive) {
            this.setState({ usereducation: resData.data.usereducation });
          }
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  addUserEducation = event => {
    event.preventDefault();

    const qualification = this.state.selUserEduQualAdd;
    const field = this.fieldAdd.current.value;
    const from_month = this.state.selUserEduMonthFromAdd;
    const from_year = this.state.selUserEduYearFromAdd;
    const to_month = this.state.selUserEduMonthToAdd;
    const to_year = this.state.selUserEduYearToAdd;
    const user_id = localStorage.getItem("userId");

    let requestBody = {
      query: `
        mutation CreateUserEducation($qualification: String!,$field:String!,$from_year: String,$from_month:String,$to_year:String,$to_month :String,$user_id: ID!) {
          createUserEducation(education: {qualification: $qualification,field:$field,from_year: $from_year, from_month: $from_month,to_year:$to_year,to_month:$to_month,user_id:$user_id}) {
            _id
          }
        }
      `,
      variables: {
        qualification: qualification,
        field: field,
        from_year: from_year,
        from_month: from_month,
        to_year: to_year,
        to_month: to_month,
        user_id: user_id
      }
    };

    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          this.setState({
            expertiseErrorMsg: <span className="error show">Failed!</span>
          });
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(resData => {
        console.log(resData);
        if (resData.data.createUserEducation._id) {
          this.fetchUserEducation();
        }
        this.setState({ showAddEducationModal: false });
      })
      .catch(err => {
        console.log(err);
      });
  };

  fetchToolkit() {
    let requestBody = {
      query: `
        query Toolkitlist {
          toolkitlist{
            _id,
            toolkit,
            status
          }
        }
      `
    };
    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(resData => {
        if (resData.data.toolkitlist) {
          if (this.isActive) {
            this.setState({ toolkit: resData.data.toolkitlist });
          }
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  fetchUserToolkit() {
    const userId = localStorage.getItem("userId");

    let requestBody = {
      query: `
        query ListUserToolkit($user: ID!) {
              listUserToolkit(user: $user) {
                _id
                toolkit{toolkit}
                user{name}
                description
          }
        }
      `,
      variables: {
        user: userId
      }
    };
    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(resData => {
        if (resData.data.listUserToolkit) {
          if (this.isActive) {
            this.setState({ usertoolkit: resData.data.listUserToolkit });
          }
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  addToolkit = event => {
    event.preventDefault();

    const toolkit = this.state.selectedToolkit;
    const user = localStorage.getItem("userId");
    const description = this.toolkitDesc.current.value;

    let requestBody = {
      query: `
        mutation CreateUserToolkit($toolkit: ID!,$user:ID!,$description: String!) {
          createUserToolkit(toolkit: $toolkit,user:$user,description: $description) {
            _id
          }
        }
      `,
      variables: {
        toolkit: toolkit,
        user: user,
        description: description
      }
    };

    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          this.setState({
            toolkitErrorMsg: <span className="error show">Failed!</span>
          });
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(resData => {
        console.log(resData);
        if (resData.data.createUserToolkit._id) {
          this.fetchUserToolkit();
        }
        this.setState({ showAddToolkitModal: false });
      })
      .catch(err => {
        console.log(err);
      });
  };

  fetchCertificates() {
    let requestBody = {
      query: `
        query certificatelist {
          certificatelist{
            _id,
            certificate,
            status
          }
        }
      `
    };
    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(resData => {
        if (resData.data.certificatelist) {
          if (this.isActive) {
            this.setState({ certificate: resData.data.certificatelist });
          }
        }
      })
      .catch(err => {
        console.log(err);
      });
  }
  fetchUserCertificates() {
    const userId = localStorage.getItem("userId");

    let requestBody = {
      query: `
        query listUserCertificate($user: ID!) {
          listUserCertificate(user: $user) {
                _id
                certificate{certificate}
                user{name}
                description
          }
        }
      `,
      variables: {
        user: userId
      }
    };
    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(resData => {
        if (resData.data.listUserCertificate) {
          if (this.isActive) {
            this.setState({
              usercertificate: resData.data.listUserCertificate
            });
          }
        }
      })
      .catch(err => {
        console.log(err);
      });
  }
  addCertificates = event => {
    event.preventDefault();

    const certificate = this.state.selectedCertificate;
    const user = localStorage.getItem("userId");
    const description = this.certificateDesc.current.value;

    let requestBody = {
      query: `
        mutation createUserCertificate($certificate: ID!,$user:ID!,$description: String!) {
          createUserCertificate(certificate: $certificate,user:$user,description: $description) {
            _id
          }
        }
      `,
      variables: {
        certificate: certificate,
        user: user,
        description: description
      }
    };

    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          this.setState({
            certificateErrorMsg: <span className="error show">Failed!</span>
          });
          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(resData => {
        console.log(resData);
        if (resData.data.createUserCertificate._id) {
          this.fetchUserCertificates();
        }
        this.setState({ showAddCertificateModal: false });
      })
      .catch(err => {
        console.log(err);
      });
  };

  finalSubmit = event => {
    event.preventDefault();

    const name = this.state.fname;
    const last_name = this.state.lname;
    const email = this.state.email;
    const resume_link = this.state.resume_link;
    const profile_pic = this.state.profile_pic;
    const main_desc = this.state.main_desc;
    const short_desc = this.state.short_desc;
    const linkedin_url = this.state.linkedin_url;
    const twitte_url = this.state.twitte_url;
    const medium_url = this.state.medium_url;

    let requestBody = {
      query: `
      mutation editMentorProfile($name: String!,$last_name:String,$email: String!, $resume_link: String!,$profile_pic:String!,$short_desc:String!,$main_desc:String!,$linkedin_url:String!,$twitte_url:String!,$medium_url:String!) {
        editMentorProfile(mentorInput: {name: $name,last_name:$last_name,email: $email,resume_link:$resume_link, profile_pic: $profile_pic, short_desc: $short_desc, main_desc: $main_desc,linkedin_url: $linkedin_url,twitte_url:$twitte_url,medium_url:$medium_url}) {
            _id
          }
        }
      `,
      variables: {
        name: name,
        last_name: last_name,
        email: email,
        profile_pic: profile_pic,
        short_desc: short_desc,
        main_desc: main_desc,
        linkedin_url: linkedin_url,
        twitte_url: twitte_url,
        medium_url: medium_url,
        resume_link: resume_link
      }
    };

    fetch(env.GRAPHQL, {
      method: "POST",
      body: JSON.stringify(requestBody),
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => {
        if (res.status !== 200 && res.status !== 201) {
          this.setState({ signup_err: true });

          throw new Error("Failed!");
        }
        return res.json();
      })
      .then(resData => {
        if (resData.data.editMentorProfile._id) {
          this.fetchMentors();
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  render() {
    return (
      <>
        <div className="teacher-prof-edit-container">
          <div className="dashboard-inner">
            <div
              className={
                this.state.teacherAccount
                  ? "content-box is-inline-block show"
                  : "content-box is-inline-block"
              }
            >
              <h1 className="title is-5 pg-heading">Edit Profile</h1>
              <div className="profile-edit-wrap section is-inline-block">
                <div className="left-blk">
                  <div className="avatar-wrap">
                    <div className="file is-boxed avatar-wrap-inner">
                      <label className="file-label">
                        <input
                          className="file-input"
                          type="file"
                          name="myImage"
                          onChange={this.onChangeFile}
                        />

                        {/* <input
                          className="file-input"
                          type="file"
                          name="resume"
                        /> */}

                        <span className="file-cta">
                          <img
                            src={"../static/uploads/users/"+this.state.profile_pic}
                            alt="user-picture"
                          />
                        </span>
                      </label>
                    </div>
                    <div className="avatar-wrap-inner" />
                  </div>
                  <p className="change-profile-pic-title">
                    Change profile picture
                  </p>
                </div>
                <div class="right-blk">
                  <div className="field-wrap">
                    <div class="field">
                      <label class="label headings">First Name</label>
                      <div class="control">
                        <input
                          class="input"
                          type="text"
                          placeholder="First Name"
                          // value={this.state.fname}
                          onChange={e =>
                            this.setState({ fname: e.target.value })
                          }
                          defaultValue={this.state.fname}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="field-wrap">
                    <div className="field">
                      <label className="label headings">Last Name</label>
                      <div className="control">
                        <input
                          className="input"
                          type="text"
                          placeholder="Last Name"
                          // value={this.state.lname}
                          onChange={e =>
                            this.setState({ lname: e.target.value })
                          }
                          defaultValue= {this.state.lname}
                        />
                      </div>
                    </div>
                  </div>

                  <div className="field-wrap">
                    <div className="field">
                      <label className="label headings">Email address</label>
                      <div classnName="control">
                        <input
                          className="input"
                          type="text"
                          placeholder="Email address"
                          // value={this.state.email}
                          onChange={e =>
                            this.setState({ email: e.target.value })
                          }
                          defaultValue={this.state.email}
                        />
                      </div>
                    </div>
                  </div>

                  <div className="field-wrap">
                    <div className="field">
                      <label className="label headings">Resume Link</label>
                      <div classnName="control">
                        <input
                          className="input"
                          type="text"
                          placeholder="Resume Link"
                          // value={this.state.resume_link}
                          onChange={e =>
                            this.setState({ resume_link: e.target.value })
                          }
                          defaultValue={this.state.resume_link}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="select-skills-wrap is-inline-block">
                    <label className="label headings">Skills</label>
                    <div className="part is-inline-block add-skills">
                      <p>
                        <span>Skill 1</span>
                        <img src="../static/delete.png" alt="delete" />
                      </p>
                      <p>
                        <span>Skill 2</span>
                        <img src="../static/delete.png" alt="delete" />
                      </p>
                      <p>
                        <span>Skill 3</span>
                        <img src="../static/delete.png" alt="delete" />
                      </p>
                      <p>
                        <span>Skill 4</span>
                        <img src="../static/delete.png" alt="delete" />
                      </p>
                    </div>
                    <a
                      className="button is-primary add-new"
                      onClick={this.handleAddSkillsModal.bind(
                        this,
                        "modalOpen"
                      )}
                    >
                      Add Skills
                    </a>
                  </div>
                  <div className="switch-account-wrap is-inline-block">
                    <label className="label headings">Switch account</label>
                    <div className="switch-account-inner">
                      <div className="status-switcher is-inline-block">
                        <div className="part is-inline-block">
                          <label
                            className={
                              this.state.teacherAccount
                                ? "radio online is-active"
                                : "radio online"
                            }
                          >
                            <input
                              type="radio"
                              name="status"
                              onClick={this.handleAccountSwitching.bind(
                                this,
                                "teacherAccount"
                              )}
                            />
                            <div className="checkmark" />
                            Teacher
                          </label>
                          <label
                            className={
                              this.state.studentAccount
                                ? "radio offline is-active"
                                : "radio offline"
                            }
                          >
                            <input
                              type="radio"
                              name="status"
                              onClick={this.handleAccountSwitching.bind(
                                this,
                                "studentAccount"
                              )}
                            />
                            Student
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="description-wrap is-inline-block">
                    <label className="label headings">Main description</label>
                    <div className="control">
                      <textarea
                        className="textarea has-fixed-size"
                        placeholder="Fixed size textarea"
                        // value={this.state.main_desc}
                        onChange={e =>
                          this.setState({ main_desc: e.target.value })
                        }
                        defaultValue={this.state.main_desc}
                      />
                    </div>
                  </div>
                  <div className="description-wrap is-inline-block">
                    <label className="label headings">Short description</label>
                    <div className="control">
                      <textarea
                        className="textarea has-fixed-size"
                        placeholder="Fixed size textarea"
                        // value={this.state.short_desc}
                        onChange={e =>
                          this.setState({ short_desc: e.target.value })
                        }
                        defaultValue={this.state.short_desc}
                      />
                    </div>
                  </div>
                  <div className="edit-languages-wrap is-inline-block">
                    <label className="label headings">Known languages</label>
                    <div className="part is-inline-block known-languages">
                      <p>
                        <span>English</span>
                        <img src="../static/delete.png" alt="delete" />
                      </p>
                      <p>
                        <span>Spanish</span>
                        <img src="../static/delete.png" alt="delete" />
                      </p>
                      <p>
                        <span>French</span>
                        <img src="../static/delete.png" alt="delete" />
                      </p>
                      <p>
                        <span>German</span>
                        <img src="../static/delete.png" alt="delete" />
                      </p>
                    </div>
                    <a
                      className="button is-primary add-new"
                      onClick={this.handleAddLanguagesModal.bind(
                        this,
                        "modalOpen"
                      )}
                    >
                      Add languages
                    </a>
                  </div>

                  {/* ------------------Expertise : Start-------------------- */}
                  <div className="expertise-wrap">
                    <label className="label headings">Expertise</label>
                    <div className="expertise-list is-inline-block">
                      {this.state.userexpertise.length > 0 && (
                        <div className="expert-row is-inline-block titles">
                          <div className="sec1 is-inline-block">
                            <span className="head label">Title</span>
                          </div>
                          <div className="sec2 is-inline-block">
                            <span className="head label">Description</span>
                          </div>
                        </div>
                      )}

                      {this.state.userexpertise.map(
                        function(item) {
                          return (
                            <div className="expert-row is-inline-block">
                              <div className="sec1 is-inline-block">
                                <span className="entry label">
                                  {item.expertise.expertise}
                                </span>
                              </div>
                              <div className="sec2 is-inline-block">
                                <span className="entry">
                                  {item.description}
                                </span>
                              </div>
                              <div className="sec3">
                                <div
                                  className="edit-delete-wrap is-inline-block"
                                  onClick={this.handleEditExpertiseModal.bind(
                                    this,
                                    "modalOpen"
                                  )}
                                >
                                  <img src="../static/edit.png" alt="delete" />
                                </div>
                                <div className="edit-delete-wrap is-inline-block">
                                  <img
                                    src="../static/delete.png"
                                    alt="delete"
                                  />
                                </div>
                              </div>
                            </div>
                          );
                        }.bind(this)
                      )}
                    </div>

                    {this.state.userexpertise.length <= 0 && (
                      <div className="empty-data">
                        <span>No Data Found!</span>
                      </div>
                    )}

                    <a
                      className="button is-primary add-new"
                      onClick={this.handleAddExpertiseModal.bind(
                        this,
                        "modalOpen"
                      )}
                    >
                      Add Expertise
                    </a>
                  </div>

                  {/* ------------------Expertise : End-------------------- */}

                  {/* ------------------Experience : Start-------------------- */}

                  <div className="work-experience-wrap">
                    <label className="label headings">Work Experience</label>
                    <div className="experience-list is-inline-block">
                      {this.state.userexperiences.map(
                        function(item) {
                          return (
                            <div className="expert-row is-inline-block">
                              <div className="sec1 is-inline-block">
                                <span className="entry label">
                                  Company name
                                </span>
                              </div>
                              <div className="sec2 is-inline-block">
                                <span className="entry">
                                  {item.company_name}
                                </span>

                                <span className="from-to is-inline-block">
                                  ({" "}
                                  <span className="from">{item.from_year}</span>{" "}
                                  - <span className="to">{item.to_year}</span> )
                                </span>
                              </div>
                              <div className="sec3">
                                <div
                                  className="edit-delete-wrap is-inline-block"
                                  onClick={this.handleEditExperienceModal.bind(
                                    this,
                                    "modalOpen"
                                  )}
                                >
                                  <img src="../static/edit.png" alt="delete" />
                                </div>
                                <div className="edit-delete-wrap is-inline-block">
                                  <img
                                    src="../static/delete.png"
                                    alt="delete"
                                  />
                                </div>
                              </div>
                              <div className="sec1 is-inline-block">
                                <span className="entry label">Job title</span>
                              </div>
                              <div className="sec2 is-inline-block">
                                <span className="entry">{item.job_title}</span>
                              </div>
                              <div className="sec1 is-inline-block">
                                <span className="entry label">Location</span>
                              </div>
                              <div className="sec2 is-inline-block">
                                <span className="entry">{item.location}</span>
                              </div>
                            </div>
                          );
                        }.bind(this)
                      )}
                      {this.state.userexperiences.length <= 0 && (
                        <div className="empty-data">
                          <span>No Data Found!</span>
                        </div>
                      )}

                      <a
                        className="button is-primary add-new"
                        onClick={this.handleAddExperienceModal.bind(
                          this,
                          "modalOpen"
                        )}
                      >
                        Add Experience
                      </a>
                    </div>
                  </div>
                  {/* ------------------Experience : End-------------------- */}

                  <div className="edit-social-media-links-wrap is-inline-block">
                    <label className="label headings">Social media links</label>
                    <div className="field-wrap">
                      <div class="field">
                        <label class="label">Linkedin URL</label>
                        <div class="control">
                          <input
                            class="input"
                            type="text"
                            placeholder="url"
                            // value={this.state.linkedin_url}
                            onChange={e =>
                              this.setState({ linkedin_url: e.target.value })
                            }
                            defaultValue={this.state.linkedin_url} 
                          />
                        </div>
                      </div>
                    </div>
                    <div className="field-wrap">
                      <div class="field">
                        <label class="label">Twitter URL</label>
                        <div class="control">
                          <input
                            class="input"
                            type="text"
                            placeholder="url"
                            // value={this.state.twitte_url}
                            onChange={e =>
                              this.setState({ twitte_url: e.target.value })
                            }
                            defaultValue ={this.state.twitte_url}
                          />
                        </div>
                      </div>
                    </div>
                    <div className="field-wrap">
                      <div class="field">
                        <label class="label">Medium URL</label>
                        <div class="control">
                          <input
                            class="input"
                            type="text"
                            placeholder="url"
                            // value={this.state.medium_url}
                            onChange={e =>
                              this.setState({ medium_url: e.target.value })
                            }
                            defaultValue= {this.state.medium_url}
                          />
                        </div>
                      </div>
                    </div>
                  </div>

                  {/* ------------------Education : Start-------------------- */}
                  <div className="education-wrap is-inline-block">
                    <label className="label headings edu">Education</label>
                    <div className="education-list is-inline-block">
                      {this.state.usereducation.map(
                        function(item) {
                          return (
                            <div className="expert-row is-inline-block">
                              <div className="sec1 is-inline-block">
                                <span className="entry label">
                                  Qualification
                                </span>
                              </div>
                              <div className="sec2 is-inline-block">
                                <span className="entry">
                                  {item.qualification}
                                </span>

                                <span className="from-to is-inline-block">
                                  ({" "}
                                  <span className="from">{item.from_year}</span>{" "}
                                  - <span className="to">{item.to_year}</span> )
                                </span>
                              </div>
                              <div className="sec3">
                                <div
                                  className="edit-delete-wrap is-inline-block"
                                  onClick={this.handleEditEducationModal.bind(
                                    this,
                                    "modalOpen"
                                  )}
                                >
                                  <img src="../static/edit.png" alt="delete" />
                                </div>

                                <div className="edit-delete-wrap is-inline-block">
                                  <img
                                    src="../static/delete.png"
                                    alt="delete"
                                  />
                                </div>
                              </div>
                              <div className="sec1 is-inline-block">
                                <span className="entry label">
                                  Field of study
                                </span>
                              </div>
                              <div className="sec2 is-inline-block">
                                <span className="entry" />
                                {item.field}
                              </div>
                            </div>
                          );
                        }.bind(this)
                      )}

                      {this.state.usereducation.length <= 0 && (
                        <div className="empty-data">
                          <span>No Data Found!</span>
                        </div>
                      )}
                      <a
                        className="button is-primary add-new"
                        onClick={this.handleAddEducationModal.bind(
                          this,
                          "modalOpen"
                        )}
                      >
                        Add Education
                      </a>
                    </div>
                  </div>
                  {/* ------------------Education : End-------------------- */}

                  {/* ------------------Toolkit : Start-------------------- */}
                  <div className="expertise-wrap">
                    <label className="label headings">Toolkit</label>
                    <div className="expertise-list is-inline-block">
                      {this.state.usertoolkit.map(
                        function(item) {
                          return (
                            <div className="toolkit-row is-inline-block">
                              <div className="sec1 is-inline-block">
                                <span className="entry label">
                                  {item.toolkit.toolkit}
                                </span>
                              </div>
                              <div className="sec2 is-inline-block">
                                <span className="entry">
                                  {item.description}
                                </span>
                              </div>
                              <div className="sec3">
                                <div
                                  className="edit-delete-wrap is-inline-block"
                                  onClick={this.handleEditExpertiseModal.bind(
                                    this,
                                    "modalOpen"
                                  )}
                                >
                                  <img src="../static/edit.png" alt="delete" />
                                </div>
                                <div className="edit-delete-wrap is-inline-block">
                                  <img
                                    src="../static/delete.png"
                                    alt="delete"
                                  />
                                </div>
                              </div>
                            </div>
                          );
                        }.bind(this)
                      )}
                    </div>

                    {this.state.usertoolkit.length <= 0 && (
                      <div className="empty-data">
                        <span>No Data Found!</span>
                      </div>
                    )}

                    <a
                      className="button is-primary add-new"
                      onClick={this.handleAddToolkitModal.bind(
                        this,
                        "modalOpen"
                      )}
                    >
                      Add Toolkit
                    </a>
                  </div>

                  {/* ------------------Toolkit : End-------------------- */}

                  {/* ------------------Certificate : Start-------------------- */}
                  <div className="expertise-wrap">
                    <label className="label headings">Certificates</label>
                    <div className="expertise-list is-inline-block">
                      {this.state.usercertificate.map(
                        function(item) {
                          return (
                            <div className="toolkit-row is-inline-block">
                              <div className="sec1 is-inline-block">
                                <span className="entry label">
                                  {item.certificate.certificate}
                                </span>
                              </div>
                              <div className="sec2 is-inline-block">
                                <span className="entry">
                                  {item.description}
                                </span>
                              </div>
                              <div className="sec3">
                                <div
                                  className="edit-delete-wrap is-inline-block"
                                  onClick={this.handleEditExpertiseModal.bind(
                                    this,
                                    "modalOpen"
                                  )}
                                >
                                  <img src="../static/edit.png" alt="delete" />
                                </div>
                                <div className="edit-delete-wrap is-inline-block">
                                  <img
                                    src="../static/delete.png"
                                    alt="delete"
                                  />
                                </div>
                              </div>
                            </div>
                          );
                        }.bind(this)
                      )}
                    </div>

                    {this.state.usercertificate.length <= 0 && (
                      <div className="empty-data">
                        <span>No Data Found!</span>
                      </div>
                    )}

                    <a
                      className="button is-primary add-new"
                      onClick={this.handleAddCertificateModal.bind(
                        this,
                        "modalOpen"
                      )}
                    >
                      Add Certificates
                    </a>
                  </div>

                  {/* ------------------Certificate : End-------------------- */}

                  <a
                    onClick={this.finalSubmit}
                    class="button is-primary is-block btn-submit"
                  >
                    Submit
                  </a>
                </div>
              </div>
            </div>
            <div
              className={
                this.state.studentAccount
                  ? "content-box is-inline-block student-content show"
                  : "content-box is-inline-block student-content"
              }
            >
              <h1 className="title is-5 pg-heading">Edit Profile</h1>
              <div className="profile-edit-wrap section is-inline-block">
                <div className="left-blk">
                  <div className="avatar-wrap">
                    <div className="file is-boxed avatar-wrap-inner">
                      <label className="file-label">
                        <input
                          className="file-input"
                          type="file"
                          name="resume"
                        />
                        <span className="file-cta">
                          <img src="../static/p_002.jpg" alt="user-picture" />
                        </span>
                      </label>
                    </div>
                    <div className="avatar-wrap-inner" />
                  </div>
                  <p className="change-profile-pic-title">
                    Change profile picture
                  </p>
                </div>
                <div class="right-blk">
                  <div className="field-wrap">
                    <div class="field">
                      <label class="label">First Name</label>
                      <div class="control">
                        <input
                          class="input"
                          type="text"
                          placeholder="First Name"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="field-wrap">
                    <div className="field">
                      <label className="label">Last Name</label>
                      <div className="control">
                        <input
                          className="input"
                          type="text"
                          placeholder="Last Name"
                        />
                      </div>
                    </div>
                  </div>

                  <div className="field-wrap">
                    <div className="field">
                      <label className="label">Email address</label>
                      <div classnName="control">
                        <input
                          className="input"
                          type="text"
                          placeholder="Email address"
                        />
                      </div>
                    </div>
                  </div>
                  <div className="short-description-wrap is-inline-block">
                    <label className="label">Short description</label>
                    <div className="control">
                      <textarea
                        className="textarea has-fixed-size"
                        placeholder="Fixed size textarea"
                      >
                        Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry. Lorem Ipsum has been the
                        industry's standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and
                        scrambled it to make a type specimen book. It has
                        survived not only five centuries, but also the leap into
                        electronic typesetting, remaining essentially unchanged.
                      </textarea>
                    </div>
                  </div>
                  <div className="switch-account-wrap is-inline-block">
                    <label className="label headings">Switch account</label>
                    <div className="switch-account-inner">
                      <div className="status-switcher is-inline-block">
                        <div className="part is-inline-block">
                          <label
                            className={
                              this.state.teacherAccount
                                ? "radio online is-active"
                                : "radio online"
                            }
                          >
                            <input
                              type="radio"
                              name="status"
                              onClick={this.handleAccountSwitching.bind(
                                this,
                                "teacherAccount"
                              )}
                            />
                            <div className="checkmark" />
                            Teacher
                          </label>
                          <label
                            className={
                              this.state.studentAccount
                                ? "radio offline is-active"
                                : "radio offline"
                            }
                          >
                            <input
                              type="radio"
                              name="status"
                              onClick={this.handleAccountSwitching.bind(
                                this,
                                "studentAccount"
                              )}
                            />
                            Student
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="education-wrap is-inline-block">
                    <label className="label headings edu">Education</label>
                    <div className="education-list is-inline-block">
                      <div className="expert-row is-inline-block">
                        <div className="sec1 is-inline-block">
                          <span className="entry label">School</span>
                        </div>
                        <div className="sec2 is-inline-block">
                          <span className="entry">
                            St.Theresa's High school
                          </span>

                          <span className="from-to is-inline-block">
                            ( <span className="from">2005</span> -{" "}
                            <span className="to">2007</span> )
                          </span>
                        </div>
                        <div className="sec3">
                          <div
                            className="edit-delete-wrap is-inline-block"
                            onClick={this.handleEditEducationStudentModal.bind(
                              this,
                              "modalOpen"
                            )}
                          >
                            <img src="../static/edit.png" alt="delete" />
                          </div>

                          <div className="edit-delete-wrap is-inline-block">
                            <img src="../static/delete.png" alt="delete" />
                          </div>
                        </div>
                        <div className="sec1 is-inline-block">
                          <span className="entry label">Field of study</span>
                        </div>
                        <div className="sec2 is-inline-block">
                          <span className="entry" />
                        </div>
                      </div>

                      <div className="expert-row is-inline-block">
                        <div className="sec1 is-inline-block">
                          <span className="entry label">Degree</span>
                        </div>
                        <div className="sec2 is-inline-block">
                          <span className="entry">
                            Albertian College of science and technology
                          </span>

                          <span className="from-to is-inline-block">
                            ( <span className="from">2007</span> -{" "}
                            <span className="to">2011</span> )
                          </span>
                        </div>
                        <div className="sec3">
                          <div
                            className="edit-delete-wrap is-inline-block"
                            onClick={this.handleEditEducationStudentModal.bind(
                              this,
                              "modalOpen"
                            )}
                          >
                            <img src="../static/edit.png" alt="delete" />
                          </div>
                          <div className="edit-delete-wrap is-inline-block">
                            <img src="../static/delete.png" alt="delete" />
                          </div>
                        </div>
                        <div className="sec1 is-inline-block">
                          <span className="entry label">Field of study</span>
                        </div>
                        <div className="sec2 is-inline-block">
                          <span className="entry">Computer science</span>
                        </div>
                      </div>

                      <a
                        className="button is-primary add-new"
                        onClick={this.handleAddEducationStudentModal.bind(
                          this,
                          "modalOpen"
                        )}
                      >
                        Add Education
                      </a>
                    </div>
                  </div>
                  <a class="button is-primary is-block btn-submit">Submit</a>
                </div>
              </div>
            </div>
            <Footer />
          </div>
        </div>
        <div
          className={
            this.state.showEditExpertiseModal
              ? "modal edit-add-modal edit-expertise-modal is-active"
              : "modal edit-add-modal edit-expertise-modal"
          }
        >
          <div className="modal-background">
            <div className="modal-content">
              <p className="title is-4">Edit expertise</p>
              <div className="add-edit-wrap is-inline-block">
                {" "}
                <label className="label">Title</label>
                <div className="control">
                  <input className="input" type="text" placeholder="Title" />
                </div>
                <div className="field-wrap is-inline-block des">
                  <div className="field">
                    <label className="label">Description</label>
                    <div className="control">
                      <textarea className="textarea">
                        Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry. Lorem Ipsum has been the
                        industry's standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and
                        scrambled it to make a type specimen book. It has
                        survived not only five centuries, but also the leap into
                        electronic typesetting, remaining essentially unchanged.
                      </textarea>
                    </div>
                  </div>
                </div>
                <a
                  className="button is-primary"
                  onClick={this.handleEditExpertiseModal.bind(
                    this,
                    "modalClose"
                  )}
                >
                  Update
                </a>
              </div>
            </div>
            <button
              className="modal-close is-large"
              aria-label="close"
              onClick={this.handleEditExpertiseModal.bind(this, "modalClose")}
            />
          </div>
        </div>

        {/* ***************ExpertiseModal : Add : Start ***************** */}
        <div
          className={
            this.state.showAddExpertiseModal
              ? "modal edit-add-modal add-expertise-modal is-active"
              : "modal edit-add-modal add-expertise-modal"
          }
        >
          <div className="modal-background">
            <div className="modal-content">
              <p className="title is-4">Add expertise</p>
              {this.state.expertiseErrorMsg}
              <div className="add-edit-wrap is-inline-block">
                {" "}
                <label className="label">Title</label>
                <div className="control">
                  {/* <input
                    className="input"
                    type="text"
                    placeholder="Title"
                    
                  /> */}

                  <div class="select">
                    <select
                      value={this.state.selectedExpertise}
                      onChange={e =>
                        this.setState({ selectedExpertise: e.target.value })
                      }
                    >
                      <option>Select dropdown</option>
                      {this.state.expertise.map(
                        function(item) {
                          return (
                            <option key={item._id} value={item._id}>
                              {item.expertise}
                            </option>
                          );
                        }.bind(this)
                      )}
                    </select>
                  </div>
                </div>
                <div className="field-wrap is-inline-block des">
                  <div className="field">
                    <label className="label">Description</label>
                    <div className="control">
                      <textarea className="textarea" ref={this.expDesc} />
                    </div>
                  </div>
                </div>
                <a
                  className="button is-primary"
                  // onClick={this.handleAddExpertiseModal.bind(
                  //   this,
                  //   "modalClose"
                  // )}
                  onClick={this.addExpertise}
                >
                  Save
                </a>
              </div>
            </div>
            <button
              className="modal-close is-large"
              aria-label="close"
              onClick={this.handleAddExpertiseModal.bind(this, "modalClose")}
            />
          </div>
        </div>

        {/* ***************ExpertiseModal : Add : End ***************** */}

        {/* ***************EducationStudentModal : Add : Start ***************** */}
        <div
          className={
            this.state.showAddEducationStudentModal
              ? "modal edit-add-modal add-education-student-modal is-active"
              : "modal edit-add-modal add-education-student-modal"
          }
        >
          <div className="modal-background">
            <div className="modal-content">
              <p className="title is-4">Add education</p>

              {this.state.experienceErrorMsg}
              <div className="add-edit-wrap is-inline-block">
                {" "}
                <label className="label">Select qualification</label>
                <div className="select">
                  <select>
                    <option>School</option>
                    <option>Degree</option>
                    <option>Post graduation</option>
                  </select>
                </div>
                <div className="field-wrap">
                  <div className="field">
                    <label className="label">Field of study</label>
                    <div className="control is-inline-block">
                      <input
                        className="input"
                        type="text"
                        placeholder="Field of study"
                      />
                    </div>
                  </div>
                </div>
                <div className="pick-date-wrap columns">
                  <div className="column">
                    <label class="label">From</label>
                    <div className="field month-selector">
                      <div className="control">
                        <div className="select">
                          <select>
                            <option>Month</option>

                            {this.state.month.map(function(item, key) {
                              return (
                                <option key={item.id}>{item.month}</option>
                              );
                            })}
                            {/* {this.state.month.map(function(item) {
                              return <option>{item}</option>;
                            })} */}
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="field year-selector">
                      <div className="control">
                        <div className="select">
                          <select>
                            <option>Year</option>
                            {this.state.year.map(function(item, key) {
                              return <option key={item.id}>{item.year}</option>;
                            })}

                            {/* {this.state.year.map(function(item) {
                              return <option>{item}</option>;
                            })} */}
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="column">
                    <label class="label">To</label>
                    <div className="field month-selector">
                      <div className="control">
                        <div className="select">
                          <select>
                            <option>Month</option>

                            {this.state.month.map(function(item, key) {
                              return (
                                <option key={item.id}>{item.month}</option>
                              );
                            })}
                            {/* {this.state.month.map(function(item) {
                              return <option>{item}</option>;
                            })} */}
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="field year-selector">
                      <div className="control">
                        <div className="select">
                          <select>
                            <option>Year</option>
                            {this.state.year.map(function(item, key) {
                              return <option key={item.id}>{item.year}</option>;
                            })}
                            {/* {this.state.year.map(function(item) {
                              return <option>{item}</option>;
                            })} */}
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <a
                  className="button is-primary"
                  onClick={this.handleAddEducationStudentModal.bind(
                    this,
                    "modalClose"
                  )}
                >
                  Add
                </a>
              </div>
            </div>
            <button
              className="modal-close is-large"
              aria-label="close"
              onClick={this.handleAddEducationStudentModal.bind(
                this,
                "modalClose"
              )}
            />
          </div>
        </div>

        {/* ***************EducationStudentModal : Add : End ***************** */}
        <div
          className={
            this.state.showEditEducationStudentModal
              ? "modal edit-add-modal edit-education-student-modal is-active"
              : "modal edit-add-modal edit-education-student-modal"
          }
        >
          <div className="modal-background">
            <div className="modal-content">
              <p className="title is-4">Edit education</p>
              <div className="add-edit-wrap is-inline-block">
                {" "}
                <label className="label">Select qualification</label>
                <div className="select">
                  <select>
                    <option>School</option>
                    <option>Degree</option>
                    <option>Post graduation</option>
                  </select>
                </div>
                <div className="field-wrap">
                  <div className="field">
                    <label className="label">Field of study</label>
                    <div className="control is-inline-block">
                      <input
                        className="input"
                        type="text"
                        placeholder="Field of study"
                      />
                    </div>
                  </div>
                </div>
                <div className="pick-date-wrap columns">
                  <div className="column">
                    <label class="label">From</label>
                    <div className="field month-selector">
                      <div className="control">
                        <div className="select">
                          <select>
                            <option>Month</option>

                            {this.state.month.map(function(item, key) {
                              return (
                                <option key={item.id}>{item.month}</option>
                              );
                            })}
                            {/* {this.state.month.map(function(item) {
                              return <option>{item}</option>;
                            })} */}
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="field year-selector">
                      <div className="control">
                        <div className="select">
                          <select>
                            <option>Year</option>
                            {this.state.year.map(function(item, key) {
                              return <option key={item.id}>{item.year}</option>;
                            })}
                            {/* {this.state.year.map(function(item) {
                              return <option>{item}</option>;
                            })} */}
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="column">
                    <label class="label">To</label>
                    <div className="field month-selector">
                      <div className="control">
                        <div className="select">
                          <select>
                            <option>Month</option>

                            {this.state.month.map(function(item, key) {
                              return (
                                <option key={item.id}>{item.month}</option>
                              );
                            })}
                            {/* {this.state.month.map(function(item) {
                              return <option>{item}</option>;
                            })} */}
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="field year-selector">
                      <div className="control">
                        <div className="select">
                          <select>
                            <option>Year</option>
                            {this.state.year.map(function(item, key) {
                              return <option key={item.id}>{item.year}</option>;
                            })}
                            {/* {this.state.year.map(function(item) {
                              return <option>{item}</option>;
                            })} */}
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <a
                  className="button is-primary"
                  onClick={this.handleEditEducationStudentModal.bind(
                    this,
                    "modalClose"
                  )}
                >
                  Update
                </a>
              </div>
            </div>
            <button
              className="modal-close is-large"
              aria-label="close"
              onClick={this.handleEditEducationStudentModal.bind(
                this,
                "modalClose"
              )}
            />
          </div>
        </div>
        <div
          className={
            this.state.showAddEducationModal
              ? "modal edit-add-modal add-education-modal is-active"
              : "modal edit-add-modal add-education-modal"
          }
        >
          <div className="modal-background">
            <div className="modal-content">
              <p className="title is-4">Add education</p>
              <div className="add-edit-wrap is-inline-block">
                {" "}
                <label className="label">Select qualification</label>
                <div className="select">
                  <select
                    value={this.state.selUserEduQualAdd}
                    onChange={e =>
                      this.setState({
                        selUserEduQualAdd: e.target.value
                      })
                    }
                  >
                    <option value="">Select</option>
                    <option value="School">School</option>
                    <option value="Degree">Degree</option>
                    <option value="Post graduation">Post graduation</option>
                  </select>
                </div>
                <div className="field-wrap">
                  <div className="field">
                    <label className="label">Field of study</label>
                    <div className="control is-inline-block">
                      <input
                        className="input"
                        type="text"
                        placeholder="Field of study"
                        ref={this.fieldAdd}
                      />
                    </div>
                  </div>
                </div>
                <div className="pick-date-wrap columns">
                  <div className="column">
                    <label class="label">From</label>
                    <div className="field month-selector">
                      <div className="control">
                        <div className="select">
                          <select
                            value={this.state.selUserEduMonthFromAdd}
                            onChange={e =>
                              this.setState({
                                selUserEduMonthFromAdd: e.target.value
                              })
                            }
                          >
                            <option>Month</option>

                            {this.state.month.map(function(item, key) {
                              return (
                                <option key={item.id}>{item.month}</option>
                              );
                            })}
                            {/* {this.state.month.map(function(item) {
                              return <option>{item}</option>;
                            })} */}
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="field year-selector">
                      <div className="control">
                        <div className="select">
                          <select
                            value={this.state.selUserEduYearFromAdd}
                            onChange={e =>
                              this.setState({
                                selUserEduYearFromAdd: e.target.value
                              })
                            }
                          >
                            <option>Year</option>
                            {this.state.year.map(function(item, key) {
                              return <option key={item.id}>{item.year}</option>;
                            })}
                            {/* {this.state.year.map(function(item) {
                              return <option>{item}</option>;
                            })} */}
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="column">
                    <label class="label">To</label>
                    <div className="field month-selector">
                      <div className="control">
                        <div className="select">
                          <select
                            value={this.state.selUserEduMonthToAdd}
                            onChange={e =>
                              this.setState({
                                selUserEduMonthToAdd: e.target.value
                              })
                            }
                          >
                            <option>Month</option>

                            {this.state.month.map(function(item, key) {
                              return (
                                <option key={item.id}>{item.month}</option>
                              );
                            })}
                            {/* {this.state.month.map(function(item) {
                              return <option>{item}</option>;
                            })} */}
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="field year-selector">
                      <div className="control">
                        <div className="select">
                          <select
                            value={this.state.selUserEduYearToAdd}
                            onChange={e =>
                              this.setState({
                                selUserEduYearToAdd: e.target.value
                              })
                            }
                          >
                            <option>Year</option>
                            {this.state.year.map(function(item, key) {
                              return <option key={item.id}>{item.year}</option>;
                            })}
                            {/* {this.state.year.map(function(item) {
                              return <option>{item}</option>;
                            })} */}
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <a
                  className="button is-primary"
                  // onClick={this.handleAddEducationModal.bind(
                  //   this,
                  //   "modalClose"
                  // )}
                  onClick={this.addUserEducation}
                >
                  Add
                </a>
              </div>
            </div>
            <button
              className="modal-close is-large"
              aria-label="close"
              onClick={this.handleAddEducationModal.bind(this, "modalClose")}
            />
          </div>
        </div>

        <div
          className={
            this.state.showEditEducationModal
              ? "modal edit-add-modal edit-education-modal is-active"
              : "modal edit-add-modal edit-education-modal"
          }
        >
          <div className="modal-background">
            <div className="modal-content">
              <p className="title is-4">Edit education</p>
              <div className="add-edit-wrap is-inline-block">
                {" "}
                <label className="label">Select qualification</label>
                <div className="select">
                  <select>
                    <option>School</option>
                    <option>Degree</option>
                    <option>Post graduation</option>
                  </select>
                </div>
                <div className="field-wrap">
                  <div className="field">
                    <label className="label">Field of study</label>
                    <div className="control is-inline-block">
                      <input
                        className="input"
                        type="text"
                        placeholder="Field of study"
                      />
                    </div>
                  </div>
                </div>
                <div className="pick-date-wrap columns">
                  <div className="column">
                    <label class="label">From</label>
                    <div className="field month-selector">
                      <div className="control">
                        <div className="select">
                          <select>
                            <option>Month</option>

                            {this.state.month.map(function(item, key) {
                              return (
                                <option key={item.id}>{item.month}</option>
                              );
                            })}
                            {/* {this.state.month.map(function(item) {
                              return <option>{item}</option>;
                            })} */}
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="field year-selector">
                      <div className="control">
                        <div className="select">
                          <select>
                            <option>Year</option>
                            {this.state.year.map(function(item, key) {
                              return <option key={item.id}>{item.year}</option>;
                            })}
                            {/* {this.state.year.map(function(item) {
                              return <option>{item}</option>;
                            })} */}
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="column">
                    <label class="label">To</label>
                    <div className="field month-selector">
                      <div className="control">
                        <div className="select">
                          <select>
                            <option>Month</option>
                            {this.state.month.map(function(item, key) {
                              return (
                                <option key={item.id}>{item.month}</option>
                              );
                            })}
                            {/* {this.state.month.map(function(item) {
                              return <option>{item}</option>;
                            })} */}
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="field year-selector">
                      <div className="control">
                        <div className="select">
                          <select>
                            <option>Year</option>
                            {this.state.year.map(function(item, key) {
                              return <option key={item.id}>{item.year}</option>;
                            })}
                            {/* {this.state.year.map(function(item) {
                              return <option>{item}</option>;
                            })} */}
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <a
                  className="button is-primary"
                  onClick={this.handleEditEducationModal.bind(
                    this,
                    "modalClose"
                  )}
                >
                  Update
                </a>
              </div>
            </div>
            <button
              className="modal-close is-large"
              aria-label="close"
              onClick={this.handleEditEducationModal.bind(this, "modalClose")}
            />
          </div>
        </div>

        <div
          className={
            this.state.showEditExperienceModal
              ? "modal edit-add-modal edit-experience-modal is-active"
              : "modal edit-add-modal edit-experience-modal"
          }
        >
          <div className="modal-background">
            <div className="modal-content">
              <p className="title is-4">Edit experince</p>
              <div className="add-edit-wrap is-inline-block">
                <div className="field-wrap is-inline-block">
                  <div className="field">
                    <label className="label">Company name</label>
                    <div className="control">
                      <input
                        className="input"
                        type="text"
                        placeholder="company name"
                      />
                    </div>
                  </div>
                </div>
                <div className="field-wrap">
                  <div className="field">
                    <label className="label">Job title</label>
                    <div className="control">
                      <input
                        class="input"
                        type="text"
                        placeholder="Job title"
                      />
                    </div>
                  </div>
                </div>
                <div className="field-wrap is-inline-block">
                  <div className="field">
                    <label className="label">Location</label>
                    <div className="control">
                      <input
                        className="input"
                        type="text"
                        placeholder="Location"
                      />
                    </div>
                  </div>
                </div>
                <div className="pick-date-wrap columns">
                  <div className="column">
                    <label class="label">From</label>
                    <div className="field month-selector">
                      <div className="control">
                        <div className="select">
                          <select>
                            <option>Month</option>
                            {this.state.month.map(function(item, key) {
                              return (
                                <option key={item.id}>{item.month}</option>
                              );
                            })}
                            {/* {this.state.month.map(function(item) {
                              return <option>{item}</option>;
                            })} */}
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="field year-selector">
                      <div className="control">
                        <div className="select">
                          <select>
                            <option>Year</option>
                            {this.state.year.map(function(item, key) {
                              return <option key={item.id}>{item.year}</option>;
                            })}
                            {/* {this.state.year.map(function(item) {
                              return <option>{item}</option>;
                            })} */}
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="column">
                    <label class="label">To</label>
                    <div className="field month-selector">
                      <div className="control">
                        <div className="select">
                          <select>
                            <option>Month</option>
                            {this.state.month.map(function(item, key) {
                              return (
                                <option key={item.id}>{item.month}</option>
                              );
                            })}
                            {/* {this.state.month.map(function(item) {
                              return <option>{item}</option>;
                            })} */}
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="field year-selector">
                      <div className="control">
                        <div className="select">
                          <select>
                            <option>Year</option>
                            {this.state.year.map(function(item, key) {
                              return <option key={item.id}>{item.year}</option>;
                            })}
                            {/* {this.state.year.map(function(item) {
                              return <option>{item}</option>;
                            })} */}
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <label className="checkbox current">
                  <input type="checkbox" />
                  Currently work here
                </label>
                <div className="field-wrap is-inline-block des">
                  <div className="field">
                    <label className="label">Description</label>
                    <div className="control">
                      <textarea className="textarea">
                        Lorem Ipsum is simply dummy text of the printing and
                        typesetting industry. Lorem Ipsum has been the
                        industry's standard dummy text ever since the 1500s,
                        when an unknown printer took a galley of type and
                        scrambled it to make a type specimen book. It has
                        survived not only five centuries, but also the leap into
                        electronic typesetting, remaining essentially unchanged.
                      </textarea>
                    </div>
                  </div>
                </div>

                <a
                  className="button is-primary"
                  onClick={this.handleEditExperienceModal.bind(
                    this,
                    "modalClose"
                  )}
                >
                  Update
                </a>
              </div>
            </div>
            <button
              className="modal-close is-large"
              aria-label="close"
              onClick={this.handleEditExperienceModal.bind(this, "modalClose")}
            />
          </div>
        </div>

        <div
          className={
            this.state.showAddExperienceModal
              ? "modal edit-add-modal add-experience-modal is-active"
              : "modal edit-add-modal add-experience-modal"
          }
        >
          <div className="modal-background">
            <div className="modal-content">
              <p className="title is-4">Add experience</p>
              <div className="add-edit-wrap is-inline-block">
                <div className="field-wrap is-inline-block">
                  <div className="field">
                    <label className="label">Company name</label>
                    <div className="control">
                      <input
                        className="input"
                        type="text"
                        placeholder="company name"
                        ref={this.companyName}
                      />
                    </div>
                  </div>
                </div>
                <div className="field-wrap">
                  <div className="field">
                    <label className="label">Job title</label>
                    <div className="control">
                      <input
                        class="input"
                        type="text"
                        placeholder="Job title"
                        ref={this.jobTitle}
                      />
                    </div>
                  </div>
                </div>
                <div className="field-wrap is-inline-block">
                  <div className="field">
                    <label className="label">Location</label>
                    <div className="control">
                      <input
                        className="input"
                        type="text"
                        placeholder="Location"
                        ref={this.location}
                      />
                    </div>
                  </div>
                </div>
                <div className="pick-date-wrap columns">
                  <div className="column">
                    <label class="label">From</label>
                    <div className="field month-selector">
                      <div className="control">
                        <div className="select">
                          <select
                            value={this.state.selUserWorkExpMonthFrom}
                            onChange={e =>
                              this.setState({
                                selUserWorkExpMonthFrom: e.target.value
                              })
                            }
                          >
                            <option>Month</option>
                            {this.state.month.map(
                              function(item, key) {
                                return (
                                  <option key={item.id} value={item.id}>
                                    {item.month}
                                  </option>
                                );
                              }.bind(this)
                            )}
                            {/* {this.state.month.map(function(item) {
                              return <option>{item}</option>;
                            })} */}
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="field year-selector">
                      <div className="control">
                        <div className="select">
                          <select
                            value={this.state.selUserWorkExpYearFrom}
                            onChange={e =>
                              this.setState({
                                selUserWorkExpYearFrom: e.target.value
                              })
                            }
                          >
                            <option>Year</option>
                            {this.state.year.map(function(item, key) {
                              return (
                                <option key={item.id} value={item.id}>
                                  {item.year}
                                </option>
                              );
                            })}
                            {/* {this.state.year.map(function(item) {
                              return <option>{item}</option>;
                            })} */}
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="column">
                    <label class="label">To</label>
                    <div className="field month-selector">
                      <div className="control">
                        <div className="select">
                          <select
                            value={this.state.selUserWorkExpMonthTo}
                            onChange={e =>
                              this.setState({
                                selUserWorkExpMonthTo: e.target.value
                              })
                            }
                          >
                            <option>Month</option>
                            {this.state.month.map(function(item, key) {
                              return (
                                <option key={item.id}>{item.month}</option>
                              );
                            })}
                            {/* {this.state.month.map(function(item) {
                              return <option>{item}</option>;
                            })} */}
                          </select>
                        </div>
                      </div>
                    </div>
                    <div className="field year-selector">
                      <div className="control">
                        <div className="select">
                          <select
                            value={this.state.selUserWorkExpYearTo}
                            onChange={e =>
                              this.setState({
                                selUserWorkExpYearTo: e.target.value
                              })
                            }
                          >
                            <option>Year</option>
                            {this.state.year.map(function(item, key) {
                              return <option key={item.id}>{item.year}</option>;
                            })}
                            {/* {this.state.year.map(function(item) {
                              return <option>{item}</option>;
                            })} */}
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <label className="checkbox current">
                  <input type="checkbox" ref={this.present} />
                  Currently work here
                </label>
                <div className="field-wrap is-inline-block des">
                  <div className="field">
                    <label className="label">Description</label>
                    <div className="control">
                      <textarea
                        className="textarea"
                        ref={this.userworkExpdescription}
                      />
                    </div>
                  </div>
                </div>

                <a
                  className="button is-primary"
                  // onClick={this.handleAddExperienceModal.bind(
                  //   this,
                  //   "modalClose"
                  // )}
                  onClick={this.addUserExperiences}
                >
                  Save
                </a>
              </div>
            </div>
            <button
              className="modal-close is-large"
              aria-label="close"
              onClick={this.handleAddExperienceModal.bind(this, "modalClose")}
            />
          </div>
        </div>

        <div
          className={
            this.state.showAddSkillsModal
              ? "modal edit-add-modal add-skills-modal is-active"
              : "modal edit-add-modal add-skills-modal"
          }
        >
          <div className="modal-background">
            <div className="modal-content">
              <p className="title is-4 lan-head">Add skills</p>
              <div>
                {this.state.skills.map(function(item) {
                  return (
                    <div className="part is-inline-block">
                      <label class="checkbox">
                        <input type="checkbox" value={item._id} />
                        {item.skill}
                      </label>
                    </div>
                  );
                })}
              </div>
              <a
                className="button is-primary"
                onClick={this.handleAddSkillsModal.bind(this, "modalClose")}
              >
                Save
              </a>
            </div>

            <button
              className="modal-close is-large"
              aria-label="close"
              onClick={this.handleAddSkillsModal.bind(this, "modalClose")}
            />
          </div>
        </div>

        <div
          className={
            this.state.showAddLanguagesModal
              ? "modal edit-add-modal add-languages-modal is-active"
              : "modal edit-add-modal add-languages-modal"
          }
        >
          <div className="modal-background">
            <div className="modal-content">
              {" "}
              <p className="title is-4 lan-head">Add languages</p>
              <div>
                {this.state.language.map(function(item) {
                  return (
                    <div className="part is-inline-block">
                      <label class="checkbox">
                        <input type="checkbox" value={item._id} />
                        {item.language}
                      </label>
                    </div>
                  );
                })}
              </div>
              <a
                className="button is-primary"
                onClick={this.handleAddSkillsModal.bind(this, "modalClose")}
              >
                Save
              </a>
            </div>

            <button
              className="modal-close is-large"
              aria-label="close"
              onClick={this.handleAddLanguagesModal.bind(this, "modalClose")}
            />
          </div>
        </div>

        {/* ***************ToolkitModal : Add : Start ***************** */}
        <div
          className={
            this.state.showAddToolkitModal
              ? "modal edit-add-modal add-toolkit-modal is-active"
              : "modal edit-add-modal add-toolkit-modal"
          }
        >
          <div className="modal-background">
            <div className="modal-content">
              <p className="title is-4">Add Toolkit</p>
              {this.state.toolkitErrorMsg}
              <div className="add-edit-wrap is-inline-block">
                {" "}
                <label className="label">Toolkit</label>
                <div className="control">
                  <div class="select">
                    <select
                      value={this.state.selectedToolkit}
                      onChange={e =>
                        this.setState({ selectedToolkit: e.target.value })
                      }
                    >
                      <option>Select dropdown</option>
                      {this.state.toolkit.map(
                        function(item) {
                          return (
                            <option key={item._id} value={item._id}>
                              {item.toolkit}
                            </option>
                          );
                        }.bind(this)
                      )}
                    </select>
                  </div>
                </div>
                <div className="field-wrap is-inline-block des">
                  <div className="field">
                    <label className="label">Description</label>
                    <div className="control">
                      <textarea className="textarea" ref={this.toolkitDesc} />
                    </div>
                  </div>
                </div>
                <a
                  className="button is-primary"
                  // onClick={this.handleAddExpertiseModal.bind(
                  //   this,
                  //   "modalClose"
                  // )}
                  onClick={this.addToolkit}
                >
                  Save
                </a>
              </div>
            </div>
            <button
              className="modal-close is-large"
              aria-label="close"
              onClick={this.handleAddToolkitModal.bind(this, "modalClose")}
            />
          </div>
        </div>

        {/* ***************ToolkitModal : Add : End ***************** */}

        {/* ***************CertificateModal : Add : Start ***************** */}
        <div
          className={
            this.state.showAddCertificateModal
              ? "modal edit-add-modal add-certificate-modal is-active"
              : "modal edit-add-modal add-certificate-modal"
          }
        >
          <div className="modal-background">
            <div className="modal-content">
              <p className="title is-4">Add Certificate</p>
              {this.state.certificateErrorMsg}
              <div className="add-edit-wrap is-inline-block">
                {" "}
                <label className="label">Certificate</label>
                <div className="control">
                  <div class="select">
                    <select
                      value={this.state.selectedCertificate}
                      onChange={e =>
                        this.setState({ selectedCertificate: e.target.value })
                      }
                    >
                      <option>Select dropdown</option>
                      {this.state.certificate.map(
                        function(item) {
                          return (
                            <option key={item._id} value={item._id}>
                              {item.certificate}
                            </option>
                          );
                        }.bind(this)
                      )}
                    </select>
                  </div>
                </div>
                <div className="field-wrap is-inline-block des">
                  <div className="field">
                    <label className="label">Description</label>
                    <div className="control">
                      <textarea
                        className="textarea"
                        ref={this.certificateDesc}
                      />
                    </div>
                  </div>
                </div>
                <a
                  className="button is-primary"
                  // onClick={this.handleAddExpertiseModal.bind(
                  //   this,
                  //   "modalClose"
                  // )}
                  onClick={this.addCertificates}
                >
                  Save
                </a>
              </div>
            </div>
            <button
              className="modal-close is-large"
              aria-label="close"
              onClick={this.handleAddCertificateModal.bind(this, "modalClose")}
            />
          </div>
        </div>

        {/* ***************CertificateModal : Add : End ***************** */}
      </>
    );
  }
}
