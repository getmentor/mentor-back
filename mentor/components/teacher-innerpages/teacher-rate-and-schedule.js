import React, { Component } from "react";
import "./teacher-rate-and-schedule.scss";
import Footer from "../footer/footer";
import counterpart from "counterpart";
import Translate from "react-translate-component";

export default class TeacherRateScheduleComp extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  toggleStatus = currentState => {
    if (currentState === "userIsOffline")
      this.setState({ userIsOnline: false, userIsOffline: true });
    else this.setState({ userIsOnline: true, userIsOffline: false });
  };
  constructor(props) {
    super(props);
    this.state = {
      userIsOnline: true,
      userIsOffline: false,
      teacherAccount: true,
      studentAccount: false
      // menuClicked: false
    };
  }
  handleAccountSwitching = activeAccount => {
    if (activeAccount === "teacherAccount")
      this.setState({ teacherAccount: true, studentAccount: false });
    else this.setState({ teacherAccount: false, studentAccount: true });
  };
  render() {
    return (
      <>
        <div className="teacher-rate-schedule-container">
          <div className="dashboard-inner">
            <div className="content-box is-inline-block">
              <div className="set-values time-zone-wrap item-wrap">
                <Translate
                  content="trslabel.l1"
                  component="label"
                  className="label is-inline-block headings"
                  unsafe={true}
                />
                {/* <label className="label is-inline-block headings">
                  Select time zone
                </label> */}

                <div className="select time-zone-dropdown">
                  <select
                    name="timezone_offset"
                    id="timezone"
                    class="time-zone"
                  >
                    <option value="-12:00">
                      (GMT -12:00) Eniwetok, Kwajalein
                    </option>
                    <option value="-11:00">
                      (GMT -11:00) Midway Island, Samoa
                    </option>
                    <option value="-10:00">(GMT -10:00) Hawaii</option>
                    <option value="-09:50">(GMT -9:30) Taiohae</option>
                    <option value="-09:00">(GMT -9:00) Alaska</option>
                    <option value="-08:00">
                      (GMT -8:00) Pacific Time (US &amp; Canada)
                    </option>
                    <option value="-07:00">
                      (GMT -7:00) Mountain Time (US &amp; Canada)
                    </option>
                    <option value="-06:00">
                      (GMT -6:00) Central Time (US &amp; Canada), Mexico City
                    </option>
                    <option value="-05:00">
                      (GMT -5:00) Eastern Time (US &amp; Canada), Bogota, Lima
                    </option>
                    <option value="-04:50">(GMT -4:30) Caracas</option>
                    <option value="-04:00">
                      (GMT -4:00) Atlantic Time (Canada), Caracas, La Paz
                    </option>
                    <option value="-03:50">(GMT -3:30) Newfoundland</option>
                    <option value="-03:00">
                      (GMT -3:00) Brazil, Buenos Aires, Georgetown
                    </option>
                    <option value="-02:00">(GMT -2:00) Mid-Atlantic</option>
                    <option value="-01:00">
                      (GMT -1:00) Azores, Cape Verde Islands
                    </option>
                    <option value="+00:00" selected="selected">
                      (GMT) Western Europe Time, London, Lisbon, Casablanca
                    </option>
                    <option value="+01:00">
                      (GMT +1:00) Brussels, Copenhagen, Madrid, Paris
                    </option>
                    <option value="+02:00">
                      (GMT +2:00) Kaliningrad, South Africa
                    </option>
                    <option value="+03:00">
                      (GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg
                    </option>
                    <option value="+03:50">(GMT +3:30) Tehran</option>
                    <option value="+04:00">
                      (GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi
                    </option>
                    <option value="+04:50">(GMT +4:30) Kabul</option>
                    <option value="+05:00">
                      (GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent
                    </option>
                    <option value="+05:50">
                      (GMT +5:30) Bombay, Calcutta, Madras, New Delhi
                    </option>
                    <option value="+05:75">
                      (GMT +5:45) Kathmandu, Pokhara
                    </option>
                    <option value="+06:00">
                      (GMT +6:00) Almaty, Dhaka, Colombo
                    </option>
                    <option value="+06:50">(GMT +6:30) Yangon, Mandalay</option>
                    <option value="+07:00">
                      (GMT +7:00) Bangkok, Hanoi, Jakarta
                    </option>
                    <option value="+08:00">
                      (GMT +8:00) Beijing, Perth, Singapore, Hong Kong
                    </option>
                    <option value="+08:75">(GMT +8:45) Eucla</option>
                    <option value="+09:00">
                      (GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk
                    </option>
                    <option value="+09:50">(GMT +9:30) Adelaide, Darwin</option>
                    <option value="+10:00">
                      (GMT +10:00) Eastern Australia, Guam, Vladivostok
                    </option>
                    <option value="+10:50">
                      (GMT +10:30) Lord Howe Island
                    </option>
                    <option value="+11:00">
                      (GMT +11:00) Magadan, Solomon Islands, New Caledonia
                    </option>
                    <option value="+11:50">(GMT +11:30) Norfolk Island</option>
                    <option value="+12:00">
                      (GMT +12:00) Auckland, Wellington, Fiji, Kamchatka
                    </option>
                    <option value="+12:75">(GMT +12:45) Chatham Islands</option>
                    <option value="+13:00">(GMT +13:00) Apia, Nukualofa</option>
                    <option value="+14:00">
                      (GMT +14:00) Line Islands, Tokelau
                    </option>
                  </select>
                </div>
              </div>
              <div className="set-values set-rate-wrap item-wrap is-inline-block">
                <Translate
                  content="trslabel.l2"
                  component="label"
                  className="label is-inline-block headings"
                  unsafe={true}
                />
                {/* <label className="label is-inline-block headings">
                  Set rate
                </label> */}

                <Translate
                  content="trslabel.l3"
                  component="label"
                  className="rate-controls label"
                  unsafe={true}
                />
                {/* <label className="rate-controls label">Minimum rate</label> */}

                <div class="field rate-input">
                  <div class="control">
                    <input
                      class="input"
                      type="text"
                      placeholder="Rate"
                      value="30 $"
                    />
                  </div>
                </div>
                <span className="title is-6 minute is-inline-block"> $ / </span>
                <div class="field rate-input">
                  <div class="control">
                    <input
                      class="input"
                      type="text"
                      placeholder="Duration"
                      value="20"
                    />
                  </div>
                </div>
                <Translate
                  content="trsspan.s1"
                  component="span"
                  className="title is-6 minute is-inline-block"
                  unsafe={true}
                />
                {/* <span className="title is-6 minute is-inline-block">
                  Minutes
                </span> */}
              </div>
              <div className="availability-wrapper is-block item-wrap">
                <div className="set-values availability-wrap">
                  <Translate
                    content="trslabel.l4"
                    component="label"
                    className="label is-inline-block headings"
                    unsafe={true}
                  />
                  {/* <label className="label is-inline-block headings">
                    Availability
                  </label> */}
                  <Translate
                    content="trslabel.l5"
                    component="label"
                    className="label is-inline-block "
                    unsafe={true}
                  />
                  {/* <label className="label is-inline-block ">
                    Add a new shedule
                  </label> */}
                  <div className="select time-zone-dropdown">
                    <select>
                      <Translate
                        content="trsoption.o1"
                        component="option"
                        unsafe={true}
                      />
                      {/* <option>Start Day</option> */}
                      <Translate
                        content="trsoption.o2"
                        component="option"
                        unsafe={true}
                      />
                      {/* <option>Sunday</option> */}
                      <Translate
                        content="trsoption.o3"
                        component="option"
                        unsafe={true}
                      />
                      {/* <option>Monday</option> */}
                      <Translate
                        content="trsoption.o4"
                        component="option"
                        unsafe={true}
                      />
                      {/* <option>Tuesday</option> */}
                      <Translate
                        content="trsoption.o5"
                        component="option"
                        unsafe={true}
                      />
                      {/* <option>Wednesday</option> */}
                      <Translate
                        content="trsoption.o6"
                        component="option"
                        unsafe={true}
                      />
                      {/* <option>Thursday</option> */}
                      <Translate
                        content="trsoption.o7"
                        component="option"
                        unsafe={true}
                      />
                      {/* <option>Friday</option> */}
                      <Translate
                        content="trsoption.o8"
                        component="option"
                        unsafe={true}
                      />
                      {/* <option>Saturday</option> */}
                    </select>
                  </div>
                  <div className="select time-zone-dropdown">
                    <select>
                      <Translate
                        content="trsoption.o9"
                        component="option"
                        unsafe={true}
                      />
                      {/* <option>Start Time</option> */}
                      <option>12.00 AM</option>
                      <option>01.00 AM</option>
                      <option>02.00 AM</option>
                      <option>03.00 AM</option>
                      <option>04.00 AM</option>
                      <option>05.00 AM</option>
                      <option>06.00 AM</option>
                      <option>07.00 AM</option>
                      <option>08.00 AM</option>
                      <option>09.00 AM</option>
                      <option>10.00 AM</option>
                      <option>11.00 AM</option>
                      <option>12.00 PM</option>
                      <option>01.00 PM</option>
                      <option>02.00 PM</option>
                      <option>03.00 PM</option>
                      <option>04.00 PM</option>
                      <option>05.00 PM</option>
                      <option>06.00 PM</option>
                      <option>07.00 PM</option>
                      <option>08.00 PM</option>
                      <option>09.00 PM</option>
                      <option>10.00 PM</option>
                      <option>11.00 PM</option>
                    </select>
                  </div>
                  <div className="select time-zone-dropdown">
                    <select>
                      <Translate
                        content="trsoption.o10"
                        component="option"
                        unsafe={true}
                      />
                      {/* <option>Stop Time</option> */}
                      <option>12.00 AM</option>
                      <option>01.00 AM</option>
                      <option>02.00 AM</option>
                      <option>03.00 AM</option>
                      <option>04.00 AM</option>
                      <option>05.00 AM</option>
                      <option>06.00 AM</option>
                      <option>07.00 AM</option>
                      <option>08.00 AM</option>
                      <option>09.00 AM</option>
                      <option>10.00 AM</option>
                      <option>11.00 AM</option>
                      <option>12.00 PM</option>
                      <option>01.00 PM</option>
                      <option>02.00 PM</option>
                      <option>03.00 PM</option>
                      <option>04.00 PM</option>
                      <option>05.00 PM</option>
                      <option>06.00 PM</option>
                      <option>07.00 PM</option>
                      <option>08.00 PM</option>
                      <option>09.00 PM</option>
                      <option>10.00 PM</option>
                      <option>11.00 PM</option>
                    </select>
                  </div>
                  <Translate
                    content="trslink.a1"
                    component="a"
                    className="button is-primary"
                    unsafe={true}
                  />
                  {/* <a className="button is-primary">ADD</a> */}
                </div>
                <div className="set-values current-schedules is-inline-block">
                  <Translate
                    content="trslabel.l6"
                    component="label"
                    className="label is-inline-block"
                    unsafe={true}
                  />
                  {/* <label className="label is-inline-block ">
                    Current shedules
                  </label> */}
                  <div className="show-current-schedules">
                    <div className="select time-zone-dropdown">
                      <select>
                        <Translate
                          content="trsoption.o1"
                          component="option"
                          unsafe={true}
                        />
                        {/* <option>Start Day</option> */}
                        <Translate
                          content="trsoption.o2"
                          component="option"
                          unsafe={true}
                        />
                        {/* <option>Sunday</option> */}
                        <Translate
                          content="trsoption.o3"
                          component="option"
                          unsafe={true}
                        />
                        {/* <option>Monday</option> */}
                        <Translate
                          content="trsoption.o4"
                          component="option"
                          unsafe={true}
                        />
                        {/* <option>Tuesday</option> */}
                        <Translate
                          content="trsoption.o5"
                          component="option"
                          unsafe={true}
                          attribute="selected"
                        />
                        {/* <option>Wednesday</option> */}
                        <Translate
                          content="trsoption.o6"
                          component="option"
                          unsafe={true}
                        />
                        {/* <option>Thursday</option> */}
                        <Translate
                          content="trsoption.o7"
                          component="option"
                          unsafe={true}
                        />
                        {/* <option>Friday</option> */}
                        <Translate
                          content="trsoption.o8"
                          component="option"
                          unsafe={true}
                        />
                        {/* <option>Saturday</option> */}
                      </select>
                    </div>
                    <div className="select time-zone-dropdown">
                      <select>
                        <option>Start Time</option>
                        <option>12.00 AM</option>
                        <option>01.00 AM</option>
                        <option>02.00 AM</option>
                        <option>03.00 AM</option>
                        <option>04.00 AM</option>
                        <option>05.00 AM</option>
                        <option>06.00 AM</option>
                        <option>07.00 AM</option>
                        <option>08.00 AM</option>
                        <option selected>09.00 AM</option>
                        <option>10.00 AM</option>
                        <option>11.00 AM</option>
                        <option>12.00 PM</option>
                        <option>01.00 PM</option>
                        <option>02.00 PM</option>
                        <option>03.00 PM</option>
                        <option>04.00 PM</option>
                        <option>05.00 PM</option>
                        <option>06.00 PM</option>
                        <option>07.00 PM</option>
                        <option>08.00 PM</option>
                        <option>09.00 PM</option>
                        <option>10.00 PM</option>
                        <option>11.00 PM</option>
                      </select>
                    </div>
                    <div className="select time-zone-dropdown">
                      <select>
                        <option>Stop Time</option>
                        <option>12.00 AM</option>
                        <option>01.00 AM</option>
                        <option>02.00 AM</option>
                        <option>03.00 AM</option>
                        <option>04.00 AM</option>
                        <option>05.00 AM</option>
                        <option>06.00 AM</option>
                        <option>07.00 AM</option>
                        <option>08.00 AM</option>
                        <option>09.00 AM</option>
                        <option>10.00 AM</option>
                        <option>11.00 AM</option>
                        <option>12.00 PM</option>
                        <option>01.00 PM</option>
                        <option>02.00 PM</option>
                        <option>03.00 PM</option>
                        <option selected>04.00 PM</option>
                        <option>05.00 PM</option>
                        <option>06.00 PM</option>
                        <option>07.00 PM</option>
                        <option>08.00 PM</option>
                        <option>09.00 PM</option>
                        <option>10.00 PM</option>
                        <option>11.00 PM</option>
                      </select>
                    </div>
                    <Translate
                      content="trslink.a2"
                      component="a"
                      className="button is-primary update-btn"
                      unsafe={true}
                    />
                    {/* <a className="button is-primary update-btn">UPDATE</a> */}
                    <Translate
                      content="trslink.a3"
                      component="a"
                      className="button is-primary"
                      unsafe={true}
                    />
                    {/* <a className="button is-primary">DELETE</a> */}
                  </div>
                  <div className="show-current-schedules">
                    <div className="select time-zone-dropdown">
                      <select>
                        <Translate
                          content="trsoption.o1"
                          component="option"
                          unsafe={true}
                        />
                        {/* <option>Start Day</option> */}
                        <Translate
                          content="trsoption.o2"
                          component="option"
                          unsafe={true}
                        />
                        {/* <option>Sunday</option> */}
                        <Translate
                          content="trsoption.o3"
                          component="option"
                          unsafe={true}
                        />
                        {/* <option>Monday</option> */}
                        <Translate
                          content="trsoption.o4"
                          component="option"
                          unsafe={true}
                        />
                        {/* <option>Tuesday</option> */}
                        <Translate
                          content="trsoption.o5"
                          component="option"
                          unsafe={true}
                        />
                        {/* <option>Wednesday</option> */}
                        <Translate
                          content="trsoption.o6"
                          component="option"
                          unsafe={true}
                        />
                        {/* <option>Thursday</option> */}
                        <Translate
                          content="trsoption.o7"
                          component="option"
                          unsafe={true}
                        />
                        {/* <option>Friday</option> */}
                        <Translate
                          content="trsoption.o8"
                          component="option"
                          unsafe={true}
                        />
                        {/* <option>Saturday</option> */}
                      </select>
                    </div>
                    <div className="select time-zone-dropdown">
                      <select>
                        <option>Start Time</option>
                        <option>12.00 AM</option>
                        <option>01.00 AM</option>
                        <option>02.00 AM</option>
                        <option>03.00 AM</option>
                        <option>04.00 AM</option>
                        <option>05.00 AM</option>
                        <option>06.00 AM</option>
                        <option>07.00 AM</option>
                        <option>08.00 AM</option>
                        <option selected>09.00 AM</option>
                        <option>10.00 AM</option>
                        <option>11.00 AM</option>
                        <option>12.00 PM</option>
                        <option>01.00 PM</option>
                        <option>02.00 PM</option>
                        <option>03.00 PM</option>
                        <option>04.00 PM</option>
                        <option>05.00 PM</option>
                        <option>06.00 PM</option>
                        <option>07.00 PM</option>
                        <option>08.00 PM</option>
                        <option>09.00 PM</option>
                        <option>10.00 PM</option>
                        <option>11.00 PM</option>
                      </select>
                    </div>
                    <div className="select time-zone-dropdown">
                      <select>
                        <option>Stop Time</option>
                        <option>12.00 AM</option>
                        <option>01.00 AM</option>
                        <option>02.00 AM</option>
                        <option>03.00 AM</option>
                        <option>04.00 AM</option>
                        <option>05.00 AM</option>
                        <option>06.00 AM</option>
                        <option>07.00 AM</option>
                        <option>08.00 AM</option>
                        <option>09.00 AM</option>
                        <option>10.00 AM</option>
                        <option>11.00 AM</option>
                        <option>12.00 PM</option>
                        <option>01.00 PM</option>
                        <option>02.00 PM</option>
                        <option>03.00 PM</option>
                        <option selected>04.00 PM</option>
                        <option>05.00 PM</option>
                        <option>06.00 PM</option>
                        <option>07.00 PM</option>
                        <option>08.00 PM</option>
                        <option>09.00 PM</option>
                        <option>10.00 PM</option>
                        <option>11.00 PM</option>
                      </select>
                    </div>
                    <Translate
                      content="trslink.a2"
                      component="a"
                      className="button is-primary update-btn"
                      unsafe={true}
                    />
                    {/* <a className="button is-primary update-btn">UPDATE</a> */}
                    <Translate
                      content="trslink.a3"
                      component="a"
                      className="button is-primary"
                      unsafe={true}
                    />
                    {/* <a className="button is-primary">DELETE</a> */}
                  </div>
                  <div className="show-current-schedules">
                    <div className="select time-zone-dropdown">
                      <select>
                        <Translate
                          content="trsoption.o1"
                          component="option"
                          unsafe={true}
                        />
                        {/* <option>Start Day</option> */}
                        <Translate
                          content="trsoption.o2"
                          component="option"
                          unsafe={true}
                        />
                        {/* <option>Sunday</option> */}
                        <Translate
                          content="trsoption.o3"
                          component="option"
                          unsafe={true}
                        />
                        {/* <option>Monday</option> */}
                        <Translate
                          content="trsoption.o4"
                          component="option"
                          unsafe={true}
                        />
                        {/* <option>Tuesday</option> */}
                        <Translate
                          content="trsoption.o5"
                          component="option"
                          unsafe={true}
                          attribute="selected"
                        />
                        {/* <option>Wednesday</option> */}
                        <Translate
                          content="trsoption.o6"
                          component="option"
                          unsafe={true}
                        />
                        {/* <option>Thursday</option> */}
                        <Translate
                          content="trsoption.o7"
                          component="option"
                          unsafe={true}
                        />
                        {/* <option>Friday</option> */}
                        <Translate
                          content="trsoption.o8"
                          component="option"
                          unsafe={true}
                        />
                        {/* <option>Saturday</option> */}
                      </select>
                    </div>
                    <div className="select time-zone-dropdown">
                      <select>
                        <option>Start Time</option>
                        <option>12.00 AM</option>
                        <option>01.00 AM</option>
                        <option>02.00 AM</option>
                        <option>03.00 AM</option>
                        <option>04.00 AM</option>
                        <option>05.00 AM</option>
                        <option>06.00 AM</option>
                        <option>07.00 AM</option>
                        <option>08.00 AM</option>
                        <option selected>09.00 AM</option>
                        <option>10.00 AM</option>
                        <option>11.00 AM</option>
                        <option>12.00 PM</option>
                        <option>01.00 PM</option>
                        <option>02.00 PM</option>
                        <option>03.00 PM</option>
                        <option>04.00 PM</option>
                        <option>05.00 PM</option>
                        <option>06.00 PM</option>
                        <option>07.00 PM</option>
                        <option>08.00 PM</option>
                        <option>09.00 PM</option>
                        <option>10.00 PM</option>
                        <option>11.00 PM</option>
                      </select>
                    </div>
                    <div className="select time-zone-dropdown">
                      <select>
                        <option>Stop Time</option>
                        <option>12.00 AM</option>
                        <option>01.00 AM</option>
                        <option>02.00 AM</option>
                        <option>03.00 AM</option>
                        <option>04.00 AM</option>
                        <option>05.00 AM</option>
                        <option>06.00 AM</option>
                        <option>07.00 AM</option>
                        <option>08.00 AM</option>
                        <option>09.00 AM</option>
                        <option>10.00 AM</option>
                        <option>11.00 AM</option>
                        <option>12.00 PM</option>
                        <option>01.00 PM</option>
                        <option>02.00 PM</option>
                        <option>03.00 PM</option>
                        <option selected>04.00 PM</option>
                        <option>05.00 PM</option>
                        <option>06.00 PM</option>
                        <option>07.00 PM</option>
                        <option>08.00 PM</option>
                        <option>09.00 PM</option>
                        <option>10.00 PM</option>
                        <option>11.00 PM</option>
                      </select>
                    </div>
                    <Translate
                      content="trslink.a2"
                      component="a"
                      className="button is-primary update-btn"
                      unsafe={true}
                    />
                    {/* <a className="button is-primary update-btn">UPDATE</a> */}
                    <Translate
                      content="trslink.a3"
                      component="a"
                      className="button is-primary"
                      unsafe={true}
                    />
                    {/* <a className="button is-primary">DELETE</a> */}
                  </div>
                </div>
              </div>
              <div className="set-values">
                <Translate
                  content="trslabel.l7"
                  component="label"
                  className="label is-inline-block headings"
                  unsafe={true}
                />
                {/* <label className="label is-inline-block headings">
                  Voice / Video availability
                </label> */}
                <div className="voice-video-wrap is-inline-block">
                  {" "}
                  <Translate
                    content="trslabel.l8"
                    component="label"
                    className="label is-inline-block availability-controls"
                    unsafe={true}
                  />
                  {/* <label className="label is-inline-block availability-controls">
                    Voice
                  </label> */}
                  <div className="control is-inline-block voice-video-control">
                    <div className="input-wrap is-inline-block">
                      <Translate
                        content="trslabel.l10"
                        component="label"
                        className="radio"
                        unsafe={true}
                      />
                      {/* <label className="radio">Yes</label> */}
                      <input type="radio" name="voice" checked />
                    </div>

                    <div className="input-wrap is-inline-block">
                      <Translate
                        content="trslabel.l11"
                        component="label"
                        className="radio"
                        unsafe={true}
                      />
                      {/* <label className="radio">No</label> */}
                      <input type="radio" name="voice" checked />
                    </div>
                  </div>
                </div>
                <div className="voice-video-wrap is-inline-block">
                  {" "}
                  <Translate
                    content="trslabel.l9"
                    component="label"
                    className="label is-inline-block availability-controls"
                    unsafe={true}
                  />
                  {/* <label className="label is-inline-block availability-controls">
                    video
                  </label> */}
                  <div className="control is-inline-block voice-video-control">
                    <div className="input-wrap is-inline-block">
                      <Translate
                        content="trslabel.l10"
                        component="label"
                        className="radio"
                        unsafe={true}
                      />
                      {/* <label className="radio">Yes</label> */}
                      <input type="radio" name="voice" checked />
                    </div>

                    <div className="input-wrap is-inline-block">
                      <Translate
                        content="trslabel.l11"
                        component="label"
                        className="radio"
                        unsafe={true}
                      />
                      {/* <label className="radio">No</label> */}
                      <input type="radio" name="voice" checked />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <Footer />
          </div>
        </div>
      </>
    );
  }
}
