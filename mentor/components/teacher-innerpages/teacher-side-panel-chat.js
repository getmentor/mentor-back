import React, { Component } from "react";
import Link from "next/link";
import "./teacher-side-panel-chat.scss";
import counterpart from "counterpart";
import Translate from "react-translate-component";
export default class TeacherSidePanelChat extends Component {
  handleDashboardMenu = () => {
    document
      .getElementById("side-panel-teacher-rate")
      .classList.toggle("is-active");
  };
  render() {
    return (
      <>
        <div className="teacher-dashboard-navigation">
          <div
            className="hamburger-wrap is-inline-block"
            onClick={this.handleDashboardMenu}
          >
            <span className="is-inline-block" />
            <span className="is-inline-block" />
            <span className="is-inline-block" />
          </div>
          <span className="dashboard-title">Dashboard Navigation</span>
        </div>
        <div
          className="teacher-side-panel-chat-wrap"
          id="side-panel-teacher-rate"
        >
          <ul className="side-panel-menu">
            <li>
              <Link href="/teacher-dashboard">
                <Translate
                  content="tsplink.a1"
                  component="a"
                  className="label is-inline-block headings"
                  unsafe={true}
                />
                {/* <a>Dashboard</a> */}
              </Link>
            </li>
            <li>
              <Link href="/teacher-profile-edit">
                <Translate
                  content="tsplink.a2"
                  component="a"
                  className="label is-inline-block headings"
                  unsafe={true}
                />
                {/* <a>Profile Edit</a> */}
              </Link>
            </li>
            <li className="is-active">
              <Link href="/student-chatbox">
                <Translate
                  content="tsplink.a3"
                  component="a"
                  className="label is-inline-block headings"
                  unsafe={true}
                />
                {/* <a>Chat/Messages</a> */}
              </Link>
            </li>
            <li>
              <Link href="/teacher-rate-and-schedule">
                <Translate
                  content="tsplink.a4"
                  component="a"
                  className="label is-inline-block headings"
                  unsafe={true}
                />
                {/* <a>Rates and schedule settings</a> */}
              </Link>
            </li>
            <li>
              <Link href="/">
                <Translate
                  content="tsplink.a5"
                  component="a"
                  className="label is-inline-block headings"
                  unsafe={true}
                />
                {/* <a>Logout</a> */}
              </Link>
            </li>
          </ul>
        </div>
      </>
    );
  }
}
