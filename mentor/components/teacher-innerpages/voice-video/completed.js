import React, { Component } from "react";
import "./completed.scss";

export default class TCRVVCompleted extends Component {
  render() {
    return (
      <div>
        <div className="tcr-voice-video-completed-wrap">
          <div className="center-content">
            <h1 className="title is-4 is-inline-block">
              Thank you for completing this session
            </h1>
            <h2 className="subtitle">
              Your account has beed credited with $
              <span className="amount">30</span>
            </h2>
          </div>
        </div>
      </div>
    );
  }
}
