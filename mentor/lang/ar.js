export default {
  title: "Titel",

  p: {
    p1: "نحن سعداء لرؤيتك مرة أخرى!",
    p2: "ليس لديك حساب؟ <span>سجل</span>",
    student_account: "لنقم بإنشاء حساب الطالب!"
  },
  span: {
    s1: "تسجيل الدخول",
    s2: "سجل",
    s3: "من فضلك أدخل بريد أليكترونى صحيح",
    s4: "من فضلك أدخل رقمك السري",
    s5: "هل نسيت كلمة المرور؟",
    s6: "تسجيل الدخول",
    s7: "ليس لديك حساب؟"
  },

  placeholder: "عنوان بريد الكتروني",
  placeholderpass: "كلمه السر",
  link: {
    apply_teacher: "تطبيق كمدرس",
    about: "حول",
    header_search: "بحث",
    header_contact: "اتصل بنا"
  },

  // Footer starts

  footerpara: {
    p1: "حقوق الطبع والنشر 2019 مينتور"
  },
  footerspan: {
    s1: "سياسة خاصة",
    s2: "شروط الخدمة"
  },

  // Footer ends

  // Mobile menu starts

  mobilemenuspan: {
    s1: "حول",
    s2: "بحث",
    s3: "اتصل بنا",
    s4: "دخولتسجيل"
  },

  // Mobile menu ends

  // Inside header starts

  ihlink: {
    a1: "حول",
    a2: "بحث",
    a3: "اتصل بنا",
    a4: "لوحة القيادة",
    a5: "الخروج",
    a6: "عرض جميع الإخطارات"
  },
  ihspan: {
    s1: "عبر الانترنت",
    s2: "غير متصل على الانترنت"
  },

  // Inside header ends

  // teacher rate and schedule page starts

  trslabel: {
    l1: "اختر المجال الزمني",
    l2: "تحديد معدل",
    l3: "معدل الحد الأدنى",
    l4: "توفر",
    l5: "إضافة shedule جديد",
    l6: "الملاعب الحالية",
    l7: "توفر الصوت / الفيديو",
    l8: "صوت",
    l9: "فيديو",
    l10: "نعم فعلا",
    l11: "لا"
  },
  trsspan: {
    s1: "الدقائق"
  },
  trslink: {
    a1: "أضف",
    a2: "تحديث",
    a3: "حذف"
  },
  trsoption: {
    o1: "تبدأ اليوم",
    o2: "الأحد",
    o3: "الإثنين",
    o4: "الثلاثاء",
    o5: "الأربعاء",
    o6: "الخميس",
    o7: "يوم الجمعة",
    o8: "يوم السبت",
    o9: "وقت البدء",
    o10: "توقف الوقت"
  },

  // teacher rate and schedule page ends

  // teacher sidepanel starts

  tsplink: {
    a1: "لوحة القيادة",
    a2: "تعديل الملف الشخصي",
    a3: "دردشة / رسائل",
    a3: "دردشة / رسائل",
    a4: "معدلات وإعدادات الجدول الزمني",
    a5: "الخروج"
  },
  // teacher side panel ends

  // teacher dashboard-content starts

  tdbpara: {
    p1: "مشاهدات الملف",
    p2: "طلبات",
    p3: "جلسات التعامل معها",
    p4: "التعليقات"
  },

  // teacher dashboard-content ends

  // student sidepanel starts

  ssplink: {
    a1: "لوحة القيادة",
    a2: "تعديل الملف الشخصي",
    a3: "دردشة / رسائل",
    a3: "دردشة / رسائل",
    a4: "تفاصيل الفاتورة",
    a5: "الخروج"
  },

  // student side panel ends

  // student dashboard-content starts

  sdbpara: {
    p1: "مشاهدات الملف",
    p2: "طلبات",
    p3: "جلسات التعامل معها",
    p4: "التعليقات"
  },

  // student dashboard-content ends

  // student billing details starts

  sbdheadings: {
    hd1: "تفاصيل البطاقة  المحفوظة",
    hd2: "تاريخ الفواتير"
  },
  sbdspan: {
    s1: "اسم حامل البطاقة",
    s2: "رقم البطاقة",
    s3: "تاريخ الانتهاء",
    s4: "تحرير تفاصيل البطاقة",
    s5: "جلسة مع",
    s6: "التسعير",
    s7: "مدة الجلسة",
    s8: "المبلغ المدفوع",
    s9: "مقدار وسطي"
  },

  // student billing details ends

  // student profile edit starts

  speheadings: {
    hd1: "تعديل الملف الشخصي"
  },
  spepara: {
    p1: "تغيير صورة الملف الشخصي",
    p2: "أضف التعليم"
  },
  spespan: {
    s1: "مجال الدراسة",
    s2: "أضف التعليم",
    s3: "خضع",
    s4: "اختر المؤهل",
    s5: "مدرسة",
    s6: "الدرجة العلمية"
  },
  spelabel: {
    l1: "الاسم الاول",
    l2: "الكنية",
    l3: "عنوان بريد الكتروني",
    l4: "وصف قصير",
    l5: "التعليم"
  },
  speoption: {
    o1: "مدرسة",
    o2: "الدرجة العلمية",
    o3: "بعد التخرج"
  },

  // student profile edit ends

  // Authorize funds starts

  afheadings: {
    hd1: "تخويل أموالك",
    hd2: "يرجى مصادقة بطاقتك. لن يتم الشحن إلا بعد الانتهاء من الجلسة.",
    hd3: "قروض",
    hd4: "أدخل تفاصيل البطاقة"
  },
  aflabel: {
    l1: "اسم حامل البطاقة",
    l2: "رقم البطاقة",
    l3: "تاريخ الانتهاء"
  },

  afspan: {
    s1: "مجال الدراسة",
    s2: "إضافة بطاقة"
  },

  // Authorize funds ends

  // Student voice/video completed starts

  svvcheadings: {
    hd1: "شكرا لاستكمال هذه",
    hd2: " لقد تم الخصم من حسابك بـ $"
  },
  svvclabel: {
    l1: "قيم هذا المعلم",
    l2: "أكتب مراجعة"
  }

  // Student voice/video completed ends
};
