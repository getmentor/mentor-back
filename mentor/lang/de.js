export default {
  title: "Titel",

  p: {
    p1: "Vi er glade for at se dig igen!",
    p2: "Har du ikke en konto?? <span>Tilmelde</span>",
    student_account: "Lad os oprette en studiekonto!"
  },
  span: {
    s1: "Log på",
    s2: "Tilmelde",
    s3: "Indtast en gyldig e-mail-adresse",
    s4: "Indtast venligst dit kodeord",
    s5: "Glemt kodeord?",
    s6: "Log på",
    s7: "Har du ikke en konto?"
  },

  placeholder: "Email adresse",
  placeholderpass: "Adgangskode",
  link: {
    apply_teacher: "Ansøg som lærer",
    about: "Om",
    header_search: "Søg",
    header_contact: "Kontakt os"
  },

  // Footer starts

  footerpara: {
    p1: "Copyright 2019 Mentor"
  },
  footerspan: {
    s1: "Fortrolighedspolitik",
    s2: "Servicevilkår"
  },

  // Footer ends

  // Mobile menu starts

  mobilemenuspan: {
    s1: "om",
    s2: "Søg",
    s3: "Kontakt os",
    s4: "Login / Register"
  },

  // Mobile menu ends

  // inside header starts

  ihlink: {
    a1: "Om",
    a2: "Søg",
    a3: "Kontakt os",
    a4: "instrumentbræt",
    a5: "Log ud",
    a6: "Se alle underretninger"
  },
  ihspan: {
    s1: "Online",
    s2: "Offline"
  },

  // Inside header ends

  // teacher rate and schedule page starts

  trslabel: {
    l1: "vælg tidszone",
    l2: "sæt sats",
    l3: "Minimumssats",
    l4: "tilgængelighed",
    l5: "Tilføj en ny tidsplan",
    l6: "Aktuelle planer",
    l7: "Stemme / video tilgængelighed",
    l8: "stemme",
    l9: "Video",
    l10: "Ja",
    l11: "Ingen"
  },
  trsspan: {
    s1: "minutter"
  },
  trslink: {
    a1: "TILFØJE",
    a2: "UPDATE",
    a3: "SLET"
  },
  trsoption: {
    o1: "Startdag",
    o2: "Søndag",
    o3: "Mandag",
    o4: "tirsdag",
    o5: "onsdag",
    o6: "torsdag",
    o7: "Fredag",
    o8: "lørdag",
    o9: "Fredag",
    o10: "lørdag"
  },

  // teacher rate and schedule page ends

  // teacher sidepanel starts

  tsplink: {
    a1: "instrumentbræ",
    a2: "Profilredigering",
    a3: "Chat / meddelelser",
    a4: "Priser og tidsplanindstillinger",
    a5: "Log ud"
  },

  // teacher side panel ends

  // teacher dashboard-content starts

  tdbpara: {
    p1: "Profil visninger",
    p2: "Anmodning",
    p3: "Sessioner håndteret",
    p4: "Anmeldelser"
  },

  // teacher dashboard-content ends

  // student sidepanel starts

  ssplink: {
    a1: "instrumentbræ",
    a2: "Profilredigering",
    a3: "Chat / meddelelser",
    a4: "Faktureringsdetaljer",
    a5: "Log ud"
  },

  // student side panel ends

  // student dashboard-content starts

  sdbpara: {
    p1: "Profil visninger",
    p2: "Anmodning",
    p3: "Sessioner håndteret",
    p4: "Anmeldelser"
  },

  // student dashboard-content ends

  // student billing details starts

  sbdheadings: {
    hd1: "Gemte kortoplysninger",
    hd2: "Faktureringshistorik"
  },
  sbdspan: {
    s1: "Kortindehaverens navn",
    s2: "Kortnummer",
    s3: "Udløbsdato",
    s4: "Rediger kortoplysninger",
    s5: "Samling med",
    s6: "Prisfastsættelse",
    s7: "Session varighed",
    s8: "Betalt beløb",
    s9: "Balancebeløb"
  },

  // student billing details ends

  // student profile edit starts

  speheadings: {
    hd1: "Edit Profile"
  },
  spepara: {
    p1: "Change profile picture",
    p2: "Add education"
  },
  spespan: {
    s1: " Field of study",
    s2: "Add Education",
    s3: "Submit",
    s4: "Select qualification",
    s5: "School",
    s6: "Degree"
  },
  spelabel: {
    l1: "First Name",
    l2: "Last Name",
    l3: "Email address",
    l4: "Short description",
    l5: "Education"
  },
  speoption: {
    o1: "School",
    o2: "Degree",
    o3: "Post graduation"
  },

  // student profile edit ends

  // Authorize funds starts

  afheadings: {
    hd1: "Autoriser din fond",
    hd2:
      "Godkend dit kort. Opladning udføres først efter afslutningen af ​​din session.",
    hd3: "credits",
    hd4: "Indtast kortoplysninger"
  },
  aflabel: {
    l1: "Kortindehaverens navn",
    l2: "Kortnummer",
    l3: "Udløbsdato"
  },

  afspan: {
    s1: "Studieområde",
    s2: "Tilføj kort"
  },

  // Authorize funds ends

  // Student voice/video completed starts

  svvcheadings: {
    hd1: "Tak for at have afsluttet denne session",
    hd2: "Din konto er debiteret med $"
  },
  svvclabel: {
    l1: "Bedøm denne lærer",
    l2: "Skrive en anmeldelse"
  }

  // Student voice/video completed ends
};
