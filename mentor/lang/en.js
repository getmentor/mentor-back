export default {
  title: "Title",

  p: {
    p1: "We're glad to see you again!",
    p2: "Don't have an account?<span>Sign up</span>",
    student_account: "Let's create a student account!"
  },
  span: {
    s1: "Log In",
    s2: "Sign up",
    s3: "Please enter a valid email address",
    s4: "Please enter your password",
    s5: "Forgot Password?",
    s6: "Log In",
    s7: "Don't have an account?",
    login_err: "Invalid Credentials!"
  },
  placeholder: "Email Address",
  placeholderpass: "Password",
  placeholder_name: "Name",
  link: {
    apply_teacher: "Apply as a teacher",
    about: "About",
    header_search: "Search",
    header_contact: "Contact us"
  },

  // Inside header starts

  ihlink: {
    a1: "About",
    a2: "Search",
    a3: "Contact us",
    a4: "Dashboard",
    a5: "Logout",
    a6: "View all notifications"
  },
  ihspan: {
    s1: "Online",
    s2: "Offline"
  },

  // Inside header ends

  // Mobile menu starts

  mobilemenuspan: {
    s1: "about",
    s2: "Search",
    s3: "Contact us",
    s4: "Login / Register"
  },

  // Mobile menu ends

  // Footer starts

  footerpara: {
    p1: "Copyright 2019 Mentor"
  },
  footerspan: {
    s1: "Privacy policy",
    s2: "Terms of service"
  },

  // Footer ends

  // teacher rate and schedule page starts

  trslabel: {
    l1: "Select time zone",
    l2: "Set rate",
    l3: "Minimum rate",
    l4: "Availability",
    l5: "Add a new shedule",
    l6: "Current shedules",
    l7: "Voice / Video availability",
    l8: "Voice",
    l9: "Video",
    l10: "Yes",
    l11: "No"
  },
  trsspan: {
    s1: "Minutes"
  },
  trslink: {
    a1: "ADD",
    a2: "UPDATE",
    a3: "DELETE"
  },
  trsoption: {
    o1: "Start day",
    o2: "Sunday",
    o3: "Monday",
    o4: "Tuesday",
    o5: "Wednesday",
    o6: "Thursday",
    o7: "Friday",
    o8: "Saturday",
    o9: "Start time",
    o10: "Stop time"
  },

  // teacher rate and schedule page ends

  // teacher sidepanel starts

  tsplink: {
    a1: "Dashboard",
    a2: "Profile Edit",
    a3: "Chat/Messages",
    a4: "Rates and schedule settings",
    a5: "Logout"
  },

  // teacher side panel ends

  // teacher dashboard-content starts

  tdbpara: {
    p1: "Profile views",
    p2: "Requests",
    p3: "Sessions handled",
    p4: "Reviews"
  },

  // teacher dashboard-content ends

  // student sidepanel starts

  ssplink: {
    a1: "Dashboard",
    a2: "Profile Edit",
    a3: "Chat/Messages",
    a4: "Billing details",
    a5: "Logout"
  },

  // student side panel ends

  // student dashboard-content starts

  sdbpara: {
    p1: "Profile views",
    p2: "Requests",
    p3: "Sessions handled",
    p4: "Reviews"
  },

  // student dashboard-content ends

  // student billing details starts

  sbdheadings: {
    hd1: "Saved card details",
    hd2: "Billing history"
  },
  sbdspan: {
    s1: "Card holder's name",
    s2: "Card number",
    s3: "Expiry date",
    s4: "Edit card details",
    s5: "Session with",
    s6: "Pricing",
    s7: "Session duration",
    s8: "Amount paid",
    s9: "Balance amount"
  },

  // student billing details ends

  // student profile edit starts

  speheadings: {
    hd1: "Edit Profile"
  },
  spepara: {
    p1: "Change profile picture",
    p2: "Add education"
  },
  spespan: {
    s1: " Field of study",
    s2: "Add Education",
    s3: "Submit",
    s4: "Select qualification",
    s5: "School",
    s6: "Degree"
  },
  spelabel: {
    l1: "First Name",
    l2: "Last Name",
    l3: "Email address",
    l4: "Short description",
    l5: "Education"
  },
  speoption: {
    o1: "School",
    o2: "Degree",
    o3: "Post graduation"
  },

  // student profile edit ends

  // Authorize funds starts

  afheadings: {
    hd1: "Authorize your fund",
    hd2:
      "Please authenticate your card. Charging will be done only after completion of your session.",
    hd3: "Credits",
    hd4: "Enter card details"
  },
  aflabel: {
    l1: "Card holder's name",
    l2: "Card number",
    l3: "Expiry date"
  },

  afspan: {
    s1: " Field of study",
    s2: "Add card"
  },

  // Authorize funds ends

  // Student voice/video completed starts

  svvcheadings: {
    hd1: "Thank you for completing this session",
    hd2: " Your account has ben debited with $"
  },
  svvclabel: {
    l1: "Rate this teacher",
    l2: "Write a review"
  },

  // Student voice/video completed ends

  // Teacher profile edit starts

  tpeheadings: {
    hd1: "Edit Profile"
  },
  tpepara: {
    p1: "Change profile picture"
  },
  tpelabel: {
    l1: "First Name",
    l2: "Last Name",
    l3: "Email address",
    l4: "Skills",
    l5: "Switch account",
    l6: "Main description",
    l7: "Short description",
    l8: "Known languages",
    l9: "Expertise",
    l10: "Work experience"
  },
  tpespan: {
    s1: "Add Skills",
    s2: "Teacher",
    s3: "Student",
    s4: "Add languages",
    s5: "Title",
    s6: "Description",
    s7: "Add expertise",
    s8: "Company name",
    s9: "Job title",
    s10: "Location"
  }

  // Teacher profile edit ends
};
