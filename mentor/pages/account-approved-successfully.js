import React, { Component } from "react";
import UserApproved from "../components/registration/teacher/approval-success";

export default class AccountApprovalSuccess extends Component {
  render() {
    return (
      <div>
        <UserApproved />
      </div>
    );
  }
}
