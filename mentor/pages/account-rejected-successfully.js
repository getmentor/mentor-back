import React, { Component } from "react";
import UserRejected from "../components/registration/teacher/rejection-success";

export default class AccountRejectionSuccess extends Component {
  render() {
    return (
      <div>
        <UserRejected />
      </div>
    );
  }
}
