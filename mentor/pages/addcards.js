import React, { Component } from "react";
import Head from "next/head";
import "./App.sass";
// import InsideHeader from "../components/inside-header";
import MainHeader from "../components/main-header";
import Footer from "../components/footer/footer";
import AddCardsComp from "../components/student-innerpages/voice-video/add-cards";

export default class AddCards extends Component {
  render() {
    return (
      <>
        <Head>
          <title>Mentor</title>
          <link rel="icon" type="image/x-icon" href="../static/favicon.ico" />
          <script id="stripe-js" src="https://js.stripe.com/v3/" async></script>
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
        </Head>
        <MainHeader />
        <AddCardsComp />
        <Footer />
      </>
    );
  }
}
