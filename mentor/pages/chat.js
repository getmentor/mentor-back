import React, { Component } from "react";
import Head from "next/head";
import "./App.sass";
// import Header from "../components/header";
import MainHeader from "../components/main-header";
import Footer from "../components/footer/footer";
import ChatBox from "../components/chatbox/chatbox";

export default class Chat extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <>
        <Head>
          <title>Mentor</title>
          <link rel="icon" type="image/x-icon" href="../static/favicon.ico" />
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
          <script src="https://unpkg.com/qiscus-sdk-core" />
        </Head>
        <MainHeader />
        <div className="container layout-container">
          <ChatBox />
        </div>
        <Footer />
      </>
    );
  }
}
