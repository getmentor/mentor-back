import React, { Component } from "react";
import Head from "next/head";
import "./App.sass";
// import Header from "../components/header";
import MainHeader from "../components/main-header";
import Footer from "../components/footer/footer";
import ContactUsContent from "../components/middle-content/contact-us";
import "../components/middle-content/contact-us.scss";

export default class Contact extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <>
        <Head>
          <title>Mentor</title>
          <link rel="icon" type="image/x-icon" href="../static/favicon.ico" />
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
        </Head>
        <MainHeader />
        <ContactUsContent />
        <Footer />
      </>
    );
  }
}
