import React, { Component } from "react";
import Head from "next/head";
import "./App.sass";
import STDVVCompleted from "../components/student-innerpages/voice-video/completed";

export default class STDVoiceVideoCompleted extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <>
        <Head>
          <title>Mentor</title>
          <link rel="icon" type="image/x-icon" href="../static/favicon.ico" />
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
        </Head>
        <STDVVCompleted />
      </>
    );
  }
}
