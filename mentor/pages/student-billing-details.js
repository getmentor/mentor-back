import React, { Component } from "react";
import Head from "next/head";
import "./App.sass";
// import InsideHeader from "../components/inside-header";
import MainHeader from "../components/main-header";
import StudentSidePanelBillingDetails from "../components/student-innerpages/student-side-panel-billing-details";
import StudentBillingDetailsContent from "../components/student-innerpages/student-billing-details-content";

export default class StudentBillingDetails extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <>
        <Head>
          <title>Mentor</title>
          <link rel="icon" type="image/x-icon" href="../static/favicon.ico" />
          <script id="stripe-js" src="https://js.stripe.com/v3/" async />
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
        </Head>
        <MainHeader />
        <StudentSidePanelBillingDetails />
        <StudentBillingDetailsContent />
      </>
    );
  }
}
