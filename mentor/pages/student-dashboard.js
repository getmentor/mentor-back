import React, { Component } from "react";
import Head from "next/head";
import "./App.sass";
// import InsideHeader from "../components/inside-header";
import MainHeader from "../components/main-header";
import StudentSidePanel from "../components/student-innerpages/student-side-panel";
import StudentDashboardContent from "../components/student-innerpages/student-dashboard-content";

export default class StudentDashboard extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <>
        <Head>
          <title>Mentor</title>
          <link rel="icon" type="image/x-icon" href="../static/favicon.ico" />
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
        </Head>
        <MainHeader />
        <StudentSidePanel />
        <StudentDashboardContent />
      </>
    );
  }
}
