import React, { Component } from "react";
import Head from "next/head";
import "./App.sass";
// import InsideHeader from "../components/inside-header";
import MainHeader from "../components/main-header";
import StudentSidePanelEdit from "../components/student-innerpages/student-side-panel-edit";
import StudentProfileEdit from "../components/student-innerpages/student-profile-edit";

export default class StudentDashboard extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <>
        <Head>
          <title>Mentor</title>
          <link rel="icon" type="image/x-icon" href="../static/favicon.ico" />
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
        </Head>
        <MainHeader />
        <StudentSidePanelEdit />
        <StudentProfileEdit />
      </>
    );
  }
}
