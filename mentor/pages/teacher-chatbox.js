import React, { Component } from "react";
import Head from "next/head";
import "./App.sass";
// import InsideHeader from "../components/inside-header";
import MainHeader from "../components/main-header";
import TeacherSidePanelChat from "../components/teacher-innerpages/teacher-side-panel-chat";
import DashboardChat from "../components/teacher-innerpages/dashboard-chat";
import ChatBox from "../components/chatbox/chatbox";
export default class TeacherChatBox extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <>
        <Head>
          <title>Mentor</title>
          <link rel="icon" type="image/x-icon" href="../static/favicon.ico" />
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
        </Head>
        <MainHeader />
        <TeacherSidePanelChat />
        <DashboardChat />
      </>
    );
  }
}
