import React, { Component } from "react";
// import Header from "../components/header";
import MainHeader from "../components/main-header";
import Footer from "../components/footer/footer";
import TeacherInfo from "../components/teacher-info/teacher-info";

export default class TeacherDetails extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <>
        <MainHeader />
        <TeacherInfo />
        <Footer />
      </>
    );
  }
}
