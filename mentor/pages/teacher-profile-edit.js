import React, { Component } from "react";
import Head from "next/head";
import "./App.sass";
// import InsideHeader from "../components/inside-header";
import MainHeader from "../components/main-header";
import TeacherSidePanelEdit from "../components/teacher-innerpages/teacher-side-panel-edit";
import TeacherProfileEditComp from "../components/teacher-innerpages/teacher-profile-edit";

export default class TeacherProfileEdit extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }
  render() {
    return (
      <>
        <Head>
          <title>Mentor</title>
          <link rel="icon" type="image/x-icon" href="../static/favicon.ico" />
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
          />
        </Head>
        <MainHeader />
        <TeacherSidePanelEdit />
        <TeacherProfileEditComp />
      </>
    );
  }
}
